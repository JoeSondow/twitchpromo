package twitter4j

class FakeMaker {

    UploadedMedia makeFakeUploadedMedia(long mediaId, long size) {
        new UploadedMedia(new JSONObject('{"media_id":' + mediaId + ',"size":' + size + '}'))
    }
}
