package sondow.cv

import sondow.common.FileClerk
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

class ArtCriticSpec extends Specification {

    @Shared
    ArtCritic artCritic = new ArtCritic()

    def "spot check determine whether an image is interesting or resembles boring images"() {
        expect:
        expected == artCritic.isImageInteresting(new FileClerk().getFile(image))

        where:
        expected | image
        false    | 'brb1.jpg'
        true     | 'joesondow-coding-1.jpg'
    }

    @Ignore('this takes a long time so only run it when needed')
    def "should determine whether an image is interesting based on similarity to boring images"() {

        expect:
        expected == artCritic.isImageInteresting(new FileClerk().getFile(image))

        where:
        expected | image
        false    | 'start1.jpg'
        false    | 'start2.jpg'
        false    | 'start3.jpg'
        false    | 'start4.jpg'
        false    | 'start5.jpg'
        false    | 'start6.jpg'
        false    | 'start7.jpg'
        false    | 'start8.jpg'
        false    | 'start9.jpg'
        false    | 'brb1.jpg'
        false    | 'brb2.jpg'
        false    | 'brb3.jpg'
        false    | 'brb4.jpg'
        false    | 'brb5.jpg'
        true     | 'joesondow-beginnersguide-1.jpg'
        true     | 'joesondow-beginnersguide-2.jpg'
        true     | 'joesondow-beginnersguide-3.jpg'
        true     | 'joesondow-beginnersguide-1.jpg'
        true     | 'joesondow-coding-1.jpg'
        true     | 'joesondow-coding-2.jpg'
        true     | 'joesondow-coding-3.jpg'
        true     | 'joesondow-coding-4.jpg'
        true     | 'joesondow-coding-5.jpg'
        true     | 'joesondow-coding-6.jpg'
        true     | 'joesondow-coding-7.jpg'
        true     | 'joesondow-coding-8.jpg'
        true     | 'joesondow-coloring.jpg'
        true     | 'joesondow-figuredrawing-1.jpg'
        true     | 'joesondow-jackbox-1.jpg'
        true     | 'joesondow-jackbox-2.jpg'
        true     | 'joesondow-jackbox-3.jpg'
        true     | 'joesondow-jackbox-4.jpg'
        true     | 'joesondow-jackbox-5.jpg'
        true     | 'joesondow-jackbox-6.jpg'
        true     | 'joesondow-jackbox-7.jpg'
        true     | 'joesondow-thimbleweed-1.jpg'
        true     | 'joesondow-thimbleweed-2.jpg'
        true     | 'joesondow-thimbleweed-3.jpg'
        true     | 'joesondow-thimbleweed-4.jpg'
        true     | 'joesondow-undertale-1.jpg'
        true     | 'joesondow-undertale-2.jpg'
        true     | 'joesondow-undertale-3.jpg'
        true     | 'joesondow-undertale-4.jpg'
        true     | 'joesondow-undertale-5.jpg'
    }
}
