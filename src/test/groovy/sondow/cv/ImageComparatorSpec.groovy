package sondow.cv

import sondow.common.FileClerk
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification

class ImageComparatorSpec extends Specification {

    // Shared in order to take advantage of caching histograms, to speed up this test
    @Shared
    ImageComparator imageComparator = new ImageComparator()

    def "spot check determine whether two images are similar"() {
        setup:
        FileClerk fileClerk = new FileClerk()
        File startFile = fileClerk.getFile(image1)
        File codingFile = fileClerk.getFile(image2)

        expect:
        imageComparator.areSimilar(startFile, codingFile) == expected

        where:
        expected | image1       | image2
        true     | 'start1.jpg' | 'brb2.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-4.jpg'
    }

    @Ignore('this takes a long time so only run it when needed')
    def "should determine whether two images are similar"() {
        setup:
        FileClerk fileClerk = new FileClerk()
        File startFile = fileClerk.getFile(image1)
        File codingFile = fileClerk.getFile(image2)

        expect:
        imageComparator.areSimilar(startFile, codingFile) == expected

        where:
        expected | image1       | image2
        true     | 'start1.jpg' | 'brb1.jpg'
        true     | 'start1.jpg' | 'brb2.jpg'
        true     | 'start1.jpg' | 'brb3.jpg'
        true     | 'start1.jpg' | 'start1.jpg'
        true     | 'start1.jpg' | 'start2.jpg'
        true     | 'start1.jpg' | 'start3.jpg'
        true     | 'start1.jpg' | 'start5.jpg'
        true     | 'start1.jpg' | 'start7.jpg'
        true     | 'start1.jpg' | 'start8.jpg'
        true     | 'start1.jpg' | 'start9.jpg'
        true     | 'brb4.jpg'   | 'brb5.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-4.jpg'
        false    | 'start1.jpg' | 'joesondow-beginnersguide-1.jpg'
        false    | 'start1.jpg' | 'joesondow-beginnersguide-2.jpg'
        false    | 'start1.jpg' | 'joesondow-beginnersguide-3.jpg'
        false    | 'start1.jpg' | 'joesondow-beginnersguide-1.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-1.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-2.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-3.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-4.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-5.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-6.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-7.jpg'
        false    | 'start1.jpg' | 'joesondow-coding-8.jpg'
        false    | 'start1.jpg' | 'joesondow-coloring.jpg'
        false    | 'start1.jpg' | 'joesondow-figuredrawing-1.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-1.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-2.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-3.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-4.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-5.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-6.jpg'
        false    | 'start1.jpg' | 'joesondow-jackbox-7.jpg'
        false    | 'start1.jpg' | 'joesondow-thimbleweed-1.jpg'
        false    | 'start1.jpg' | 'joesondow-thimbleweed-2.jpg'
        false    | 'start1.jpg' | 'joesondow-thimbleweed-3.jpg'
        false    | 'start1.jpg' | 'joesondow-thimbleweed-4.jpg'
        false    | 'start1.jpg' | 'joesondow-undertale-1.jpg'
        false    | 'start1.jpg' | 'joesondow-undertale-2.jpg'
        false    | 'start1.jpg' | 'joesondow-undertale-3.jpg'
        false    | 'start1.jpg' | 'joesondow-undertale-4.jpg'
        false    | 'start1.jpg' | 'joesondow-undertale-5.jpg'
        true     | 'start2.jpg' | 'start1.jpg'
        true     | 'start2.jpg' | 'start2.jpg'
        true     | 'start2.jpg' | 'brb1.jpg'
        true     | 'start2.jpg' | 'brb2.jpg'
        true     | 'start2.jpg' | 'brb3.jpg'
        false    | 'start2.jpg' | 'joesondow-beginnersguide-1.jpg'
        false    | 'start2.jpg' | 'joesondow-beginnersguide-2.jpg'
        false    | 'start2.jpg' | 'joesondow-beginnersguide-3.jpg'
        false    | 'start2.jpg' | 'joesondow-beginnersguide-1.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-1.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-2.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-3.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-4.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-5.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-6.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-7.jpg'
        false    | 'start2.jpg' | 'joesondow-coding-8.jpg'
        false    | 'start2.jpg' | 'joesondow-coloring.jpg'
        false    | 'start2.jpg' | 'joesondow-figuredrawing-1.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-1.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-2.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-3.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-4.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-5.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-6.jpg'
        false    | 'start2.jpg' | 'joesondow-jackbox-7.jpg'
        false    | 'start2.jpg' | 'joesondow-thimbleweed-1.jpg'
        false    | 'start2.jpg' | 'joesondow-thimbleweed-2.jpg'
        false    | 'start2.jpg' | 'joesondow-thimbleweed-3.jpg'
        false    | 'start2.jpg' | 'joesondow-thimbleweed-4.jpg'
        false    | 'start2.jpg' | 'joesondow-undertale-1.jpg'
        false    | 'start2.jpg' | 'joesondow-undertale-2.jpg'
        false    | 'start2.jpg' | 'joesondow-undertale-3.jpg'
        false    | 'start2.jpg' | 'joesondow-undertale-4.jpg'
        false    | 'start2.jpg' | 'joesondow-undertale-5.jpg'
        true     | 'brb1.jpg'   | 'start1.jpg'
        true     | 'brb1.jpg'   | 'start2.jpg'
        true     | 'brb1.jpg'   | 'brb1.jpg'
        true     | 'brb1.jpg'   | 'brb2.jpg'
        true     | 'brb1.jpg'   | 'brb3.jpg'
        false    | 'brb1.jpg'   | 'joesondow-beginnersguide-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-beginnersguide-2.jpg'
        false    | 'brb1.jpg'   | 'joesondow-beginnersguide-3.jpg'
        false    | 'brb1.jpg'   | 'joesondow-beginnersguide-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-2.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-3.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-4.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-5.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-6.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-7.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coding-8.jpg'
        false    | 'brb1.jpg'   | 'joesondow-coloring.jpg'
        false    | 'brb1.jpg'   | 'joesondow-figuredrawing-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-2.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-3.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-4.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-5.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-6.jpg'
        false    | 'brb1.jpg'   | 'joesondow-jackbox-7.jpg'
        false    | 'brb1.jpg'   | 'joesondow-thimbleweed-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-thimbleweed-2.jpg'
        false    | 'brb1.jpg'   | 'joesondow-thimbleweed-3.jpg'
        false    | 'brb1.jpg'   | 'joesondow-thimbleweed-4.jpg'
        false    | 'brb1.jpg'   | 'joesondow-undertale-1.jpg'
        false    | 'brb1.jpg'   | 'joesondow-undertale-2.jpg'
        false    | 'brb1.jpg'   | 'joesondow-undertale-3.jpg'
        false    | 'brb1.jpg'   | 'joesondow-undertale-4.jpg'
        false    | 'brb1.jpg'   | 'joesondow-undertale-5.jpg'
        false    | 'brb4.jpg'   | 'joesondow-beginnersguide-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-beginnersguide-2.jpg'
        false    | 'brb4.jpg'   | 'joesondow-beginnersguide-3.jpg'
        false    | 'brb4.jpg'   | 'joesondow-beginnersguide-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-2.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-3.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-4.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-5.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-6.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-7.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coding-8.jpg'
        false    | 'brb4.jpg'   | 'joesondow-coloring.jpg'
        false    | 'brb4.jpg'   | 'joesondow-figuredrawing-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-2.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-3.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-4.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-5.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-6.jpg'
        false    | 'brb4.jpg'   | 'joesondow-jackbox-7.jpg'
        false    | 'brb4.jpg'   | 'joesondow-thimbleweed-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-thimbleweed-2.jpg'
        false    | 'brb4.jpg'   | 'joesondow-thimbleweed-3.jpg'
        false    | 'brb4.jpg'   | 'joesondow-thimbleweed-4.jpg'
        false    | 'brb4.jpg'   | 'joesondow-undertale-1.jpg'
        false    | 'brb4.jpg'   | 'joesondow-undertale-2.jpg'
        false    | 'brb4.jpg'   | 'joesondow-undertale-3.jpg'
        false    | 'brb4.jpg'   | 'joesondow-undertale-4.jpg'
        false    | 'brb4.jpg'   | 'joesondow-undertale-5.jpg'
    }
}
