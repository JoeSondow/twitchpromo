package sondow.twitchpromo

import java.time.ZonedDateTime

class TimeUtil {

    static ZonedDateTime zonedDateTime(String hourMinute) {
        zonedDateTimeWithSeconds(hourMinute + ":00")
    }

    static ZonedDateTime zonedDateTimeWithSeconds(String hourMinuteSecond) {
        ZonedDateTime.parse('2018-01-03T' + hourMinuteSecond + 'Z')
    }

    static Date date(ZonedDateTime zonedDateTime) {
        Date.from(zonedDateTime.toInstant())
    }

    static Date date(String hourMinute) {
        date(zonedDateTime(hourMinute))
    }

    static stream(String hourMinute) {
        Stream stream = new Stream(null, null, 0, date(hourMinute))
        stream
    }
}
