package sondow.twitchpromo

import twitter4j.URLEntity

class FakeUrlEntity implements URLEntity {

    String expandedURL

    @Override
    String getText() {
        return null
    }

    @Override
    String getURL() {
        return null
    }

    @Override
    String getDisplayURL() {
        return null
    }

    @Override
    int getStart() {
        return 0
    }

    @Override
    int getEnd() {
        return 0
    }
}
