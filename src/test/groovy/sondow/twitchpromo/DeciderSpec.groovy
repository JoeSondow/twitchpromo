package sondow.twitchpromo

import java.time.ZonedDateTime
import sondow.cv.ArtCritic
import spock.lang.Specification
import spock.lang.Unroll
import twitter4j.ResponseList
import twitter4j.Status
import twitter4j.Twitter
import static sondow.twitchpromo.TimeUtil.date
import static sondow.twitchpromo.TimeUtil.stream
import static sondow.twitchpromo.TimeUtil.zonedDateTime

class DeciderSpec extends Specification {

    def "should decide not to tweet if stream start time is too recent"() {
        setup:
        File imageFile = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, Mock(Twitter), 22, Mock(ArtCritic))
        ZonedDateTime fakeNow = zonedDateTime('16:13')
        StoredTweet tweet = null
        Stream stream = stream('16:13')

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [tweet], stream, imageFile)

        then:
        !decision.isTweetingTime()
        decision.inReplyToStatusId == null
    }

    def "should decide to tweet if no recent tweet found"() {
        setup:
        ArtCritic artCritic = Mock()
        Twitter twitter = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, 11, artCritic)
        ZonedDateTime fakeNow = zonedDateTime('16:15')
        StoredTweet storedTweet = null
        Stream stream = stream('16:10')
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<>([])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [storedTweet], stream, imageFile)

        then:
        1 * artCritic.isImageInteresting(imageFile) >> true
        decision.isTweetingTime()
        decision.inReplyToStatusId == null
        1 * twitter.getUserTimeline() >> responseList
        0 * _._
    }

    def "should decide to delete single bad tweet and restart thread"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, 13, artCritic)
        ZonedDateTime fakeNow = zonedDateTime('16:49')
        Date then = date('15:29')
        StoredTweet storedTweet = new StoredTweet('97', '', then, 'Moony', '345')
        Status status = new Tweet(id: 97L, createdAt: then, retweetCount: 0, favoriteCount: 0,
                mediaEntities: [new FakeMediaEntity()],
                URLEntities: [new FakeUrlEntity(expandedURL: 'https://twitch.tv/Joe')])
        Stream stream = stream('15:20')
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([status])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [storedTweet], stream, imageFile)

        then:
        2 * config.getMinIntervalMinutes() >> 39
        2 * config.getMedIntervalMinutes() >> 59
        2 * config.getMaxIntervalMinutes() >> 79
        decision.isTweetingTime()
        decision.inReplyToStatusId == null
        decision.tweetIdsToDelete == [97L]
        1 * artCritic.isImageInteresting(imageFile) >> true
        1 * twitter.showStatus(97L) >> status
        1 * twitter.getUserTimeline() >> responseList
        0 * _._
    }

    private static StoredTweet storedTweet(String id, Date timestamp) {
        new StoredTweet(id, null, timestamp, null, null)
    }

    private static Status status(Long id, Date createdAt, int retweets, int favorites) {
        new Tweet(id: id, createdAt: createdAt, retweetCount: retweets, favoriteCount: favorites,
                mediaEntities: [new FakeMediaEntity()],
                URLEntities: [new FakeUrlEntity(expandedURL: 'https://twitch.tv/Joe')]
        )
    }

    private
    static Status status(Long id, Date createdAt, int retweets, int favorites, long inReply) {
        new Tweet(id: id, createdAt: createdAt, retweetCount: retweets, favoriteCount: favorites,
                mediaEntities: [new FakeMediaEntity()],
                URLEntities: [new FakeUrlEntity(expandedURL: 'https://twitch.tv/Joe')],
                inReplyToStatusId: inReply)
    }

    def "should delete first and second bad tweets and restart thread"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, 12, artCritic)
        Date first = date('15:21')
        Date second = date('16:10')
        ZonedDateTime fakeNow = zonedDateTime('17:33')
        storedTweet('97', first)
        StoredTweet storedTweet1 = storedTweet('97', first)
        StoredTweet storedTweet2 = storedTweet('208', second)
        Status tweet1 = status(97L, first, 0, 0)
        Status tweet2 = status(208L, second, 0, 0)
        Stream stream = stream('15:17')
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([tweet1, tweet2])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [storedTweet1, storedTweet2], stream,
                imageFile)

        then:
        decision.isTweetingTime()
        decision.inReplyToStatusId == null
        decision.tweetIdsToDelete == [208L, 97L]
        2 * config.getMinIntervalMinutes() >> 39
        2 * config.getMedIntervalMinutes() >> 59
        2 * config.getMaxIntervalMinutes() >> 79
        1 * artCritic.isImageInteresting(imageFile) >> true
        1 * twitter.showStatus(208L) >> tweet2
        1 * twitter.showStatus(97L) >> tweet1
        1 * twitter.getUserTimeline() >> responseList
        0 * _._
    }

    def "should delete later bad tweets instead of early bad tweets in thread with good middle"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, 12, artCritic)
        Stream stream = stream('10:00')
        ZonedDateTime fakeNow = zonedDateTime('18:00')
        Status tweet1Bad = status(1L, date('11:00'), 0, 0)
        Status tweet2Bad = status(2L, date('12:00'), 0, 0, 1L)
        Status tweet3Good = status(3L, date('13:00'), 0, 2, 2L)
        Status tweet4Good = status(4L, date('14:00'), 0, 2, 3L)
        Status tweet5Bad = status(5L, date('15:00'), 0, 0, 4L)
        Status tweet6Bad = status(6L, date('16:00'), 0, 0, 5L)
        StoredTweet tweet1 = storedTweet('1', date('11:00'))
        StoredTweet tweet2 = storedTweet('2', date('12:00'))
        StoredTweet tweet3 = storedTweet('3', date('13:00'))
        StoredTweet tweet4 = storedTweet('4', date('14:00'))
        StoredTweet tweet5 = storedTweet('5', date('15:00'))
        StoredTweet tweet6 = storedTweet('6', date('16:00'))
        List<StoredTweet> tweets = [tweet6, tweet3, tweet5, tweet2, tweet1, tweet4]
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([tweet1Bad, tweet2Bad,
                                                                          tweet3Good, tweet4Good,
                                                                          tweet5Bad, tweet6Bad])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, tweets, stream, imageFile)

        then:
        decision.isTweetingTime()
        decision.inReplyToStatusId == null
        decision.tweetIdsToDelete == [6L, 5L]
        2 * config.getMinIntervalMinutes() >> 39
        2 * config.getMedIntervalMinutes() >> 59
        2 * config.getMaxIntervalMinutes() >> 79
        1 * artCritic.isImageInteresting(imageFile) >> true
        1 * twitter.showStatus(6L) >> tweet6Bad
        1 * twitter.showStatus(5L) >> tweet5Bad
        1 * twitter.showStatus(4L) >> tweet4Good
        1 * twitter.getUserTimeline() >> responseList
        0 * _._
    }

    def "should decide to tweet if last tweet was 80 minutes ago, replying only to good tweet"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, percent, artCritic)
        ZonedDateTime fakeNow = zonedDateTime('16:49')
        ZonedDateTime then = zonedDateTime('15:29')
        Date thenDate = date(then)
        StoredTweet storedTweet = storedTweet('42', thenDate)
        Status status = status(42, thenDate, rt, fv)
        Stream stream = stream('15:20')
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([status])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [storedTweet], stream, imageFile)

        then:
        decision.isTweetingTime()
        decision.inReplyToStatusId == null
        decision.tweetIdsToDelete == del
        2 * config.getMinIntervalMinutes() >> 39
        2 * config.getMedIntervalMinutes() >> 59
        2 * config.getMaxIntervalMinutes() >> 79
        1 * artCritic.isImageInteresting(imageFile) >> true
        1 * twitter.showStatus(42L) >> status
        1 * twitter.getUserTimeline() >> responseList
        0 * _._

        where:
        rt  | fv  | replyId | del   | percent
        2   | 0   | '42'    | []    | 10
        0   | 2   | '42'    | []    | 30
        2   | 2   | '42'    | []    | 50
        500 | 0   | '42'    | []    | 70
        0   | 998 | '42'    | []    | 80
        0   | 0   | null    | [42L] | 99
    }

    @Unroll
    def "should decide to tweet 66% of the time if last tweet was 60 min ago and image is good"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, percent, artCritic)
        ZonedDateTime fakeNow = zonedDateTime('16:49')
        ZonedDateTime sixtyMinAgo = zonedDateTime('15:49')
        Date sixtyMinAgoDate = date(sixtyMinAgo)
        StoredTweet tweet = storedTweet('92', sixtyMinAgoDate)
        Status status = status(92, sixtyMinAgoDate, 0, 2)
        Stream stream = stream('15:20')
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([status])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [tweet], stream, imageFile)

        then:
        decision.isTweetingTime() == outcome
        decision.inReplyToStatusId == replyId
        decision.tweetIdsToDelete == []
        twitter.showStatus(92L) >> status
        artChecks * artCritic.isImageInteresting(imageFile) >> goodImage
        timelineChecks * twitter.getUserTimeline() >> responseList
        _ * config.getMinIntervalMinutes() >> 39
        _ * config.getMedIntervalMinutes() >> 59
        _ * config.getMaxIntervalMinutes() >> 79
        0 * _._

        where:
        percent | outcome | replyId | artChecks | goodImage | timelineChecks
        10      | true    | null    | 1         | true      | 1
        20      | true    | null    | 1         | true      | 1
        30      | true    | null    | 1         | true      | 1
        30      | false   | null    | 1         | false     | 0
        40      | true    | null    | 1         | true      | 1
        40      | false   | null    | 1         | false     | 0
        50      | true    | null    | 1         | true      | 1
        60      | true    | null    | 1         | true      | 1
        60      | false   | null    | 1         | false     | 0
        70      | false   | null    | 0         | true      | 0
        80      | false   | null    | 0         | true      | 0
        90      | false   | null    | 0         | true      | 0
        99      | false   | null    | 0         | true      | 0
        99      | false   | null    | 0         | true      | 0
    }

    @Unroll
    def "should decide to tweet 33% of the time if the last tweet was 40 minutes ago"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, percent, artCritic)
        ZonedDateTime fakeNow = zonedDateTime('16:49')
        ZonedDateTime fortyMinAgo = zonedDateTime('16:09')
        Date fortyMinAgoDate = date(fortyMinAgo)
        StoredTweet tweet = storedTweet('847436', fortyMinAgoDate)
        Status status = status(847436, fortyMinAgoDate, 1, 2)
        Stream stream = stream('15:20')
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([status])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [tweet], stream, imageFile)

        then:
        decision.isTweetingTime() == outcome
        decision.inReplyToStatusId == replyId
        decision.tweetIdsToDelete == []
        twitter.showStatus(847436L) >> status
        artChecks * artCritic.isImageInteresting(imageFile) >> true
        timelineChecks * twitter.getUserTimeline() >> responseList
        _ * config.getMinIntervalMinutes() >> 39
        _ * config.getMedIntervalMinutes() >> 59
        _ * config.getMaxIntervalMinutes() >> 79
        0 * _._

        where:
        percent | outcome | replyId | artChecks | timelineChecks
        0       | true    | null    | 1         | 1
        10      | true    | null    | 1         | 1
        20      | true    | null    | 1         | 1
        30      | true    | null    | 1         | 1
        40      | false   | null    | 0         | 0
        50      | false   | null    | 0         | 0
        60      | false   | null    | 0         | 0
        70      | false   | null    | 0         | 0
        80      | false   | null    | 0         | 0
        90      | false   | null    | 0         | 0
        99      | false   | null    | 0         | 0
    }

    @Unroll
    def "should decide to start a new thread if stream started after last good tweet"() {
        setup:
        Twitter twitter = Mock()
        ArtCritic artCritic = Mock()
        Config config = Mock()
        Decider decider = new Decider(config, twitter, 25, artCritic)
        ZonedDateTime fakeNow = zonedDateTime('16:49')
        ZonedDateTime fortyMinAgo = zonedDateTime('16:09')
        Date fortyMinAgoDate = date(fortyMinAgo)
        StoredTweet tweet = storedTweet('847436', fortyMinAgoDate)
        Status status = status(847436L, fortyMinAgoDate, 4, 0)
        Stream stream = stream(start)
        File imageFile = Mock()
        ResponseList<Status> responseList = new FakeResponseList<Status>([status])

        when:
        Decision decision = decider.decideWhatToDo(fakeNow, [tweet], stream, imageFile)

        then:
        decision.isTweetingTime() == outcome
        decision.inReplyToStatusId == replyId
        decision.tweetIdsToDelete == []
        1 * twitter.showStatus(847436L) >> status
        artChecks * artCritic.isImageInteresting(imageFile) >> true
        1 * twitter.getUserTimeline() >> responseList
        _ * config.getMinIntervalMinutes() >> 39
        _ * config.getMedIntervalMinutes() >> 59
        _ * config.getMaxIntervalMinutes() >> 79
        0 * _._

        where:
        start   | percent | outcome | replyId | artChecks
        '15:20' | 99      | true    | null    | 1
        '16:11' | 99      | true    | null    | 1
    }

    def "should pick most recent tweet from list"() {
        setup:
        Config config = Mock()
        Decider decider = new Decider(config, Mock(Twitter), 69, null)
        List<StoredTweet> tweets = ['12:30', '15:14', '16:09', '14:30', '13:51'].collect {
            storedTweet(null, date(it))
        }

        when:
        StoredTweet mostRecent = decider.pickMostRecent(tweets)

        then:
        mostRecent == tweets[2]
    }

    def "should pick single tweet when it's the only one"() {
        setup:
        Config config = Mock()
        Decider decider = new Decider(config, Mock(Twitter), 88, null)
        Date timestamp = date('12:30')
        List<StoredTweet> tweets = [storedTweet(null, timestamp)]

        when:
        StoredTweet mostRecent = decider.pickMostRecent(tweets)

        then:
        mostRecent == tweets[0]
    }

    def "pickMostRecent should return null when there are no tweets"() {
        setup:
        Config config = Mock()
        Decider decider = new Decider(config, null, 1, null)

        when:
        StoredTweet mostRecent = decider.pickMostRecent([])

        then:
        mostRecent == null
    }
}
