package sondow.twitchpromo

import com.amazonaws.auth.AWSCredentials
import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import sondow.common.Environment
import sondow.common.Keymaster
import spock.lang.Specification
import twitter4j.conf.Configuration

class ConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration with environment variables"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        ConfigFactory factory = new ConfigFactory(environment, 'unittest')
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter_credentials", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")
        envVars.set("twitch_access_token", "${filler}fred")
        envVars.set("twitch_client_id", "${filler}shaggy")
        envVars.set("twitch_target_username", "${filler}${filler}${filler}scooby${filler}${filler}")
        envVars.set("CRED_AWS_ACCESS_KEY", "${filler}${filler}velma${filler}${filler}${filler}")
        envVars.set("CRED_AWS_SECRET_KEY", "${filler}${filler}daphne${filler}${filler}${filler}")

        when:
        Config overallConfig = factory.configure()
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        AWSCredentials awsCredentials = overallConfig.getCredentials()

        then:
        with(overallConfig) {
            clientId == 'shaggy'
            targetUsername == 'scooby'
        }
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
        with(awsCredentials) {
            AWSAccessKeyId == 'velma'
            AWSSecretKey == 'daphne'
        }
    }

    def "configure should populate a configuration with account specific env vars"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "picardtips")
        envVars.set("picardtips_twitter_credentials",
                "${filler}picardtips,makeitso,teaearlgreyhot,therearefourlight,engage${filler}")
        envVars.set("rikergoogling_twitter_credentials",
                "${filler}rikergoogling,whatthehell,imzadi,jazz,beard${filler}")
        envVars.set("twitch_access_token",
                "${filler}${filler}${filler}nose${filler}${filler}${filler}")
        envVars.set("twitch_client_id",
                "${filler}${filler}${filler}smile${filler}${filler}${filler}")
        envVars.set("twitch_target_username", "${filler}happydude")
        envVars.set("CRED_AWS_ACCESS_KEY", "${filler}${filler}velma${filler}${filler}${filler}")
        envVars.set("CRED_AWS_SECRET_KEY", "${filler}${filler}daphne${filler}${filler}${filler}")

        when:
        Config overallConfig = new ConfigFactory(environment, 'unittest').configure()
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        AWSCredentials awsCredentials = overallConfig.getCredentials()

        then:
        with(overallConfig) {
            clientId == 'smile'
            accessToken == 'nose'
            targetUsername == 'happydude'
        }
        with(twitterConfig) {
            OAuthConsumerKey == 'makeitso'
            OAuthConsumerSecret == 'teaearlgreyhot'
            OAuthAccessToken == 'therearefourlight'
            OAuthAccessTokenSecret == 'engage'
        }
        with(awsCredentials) {
            AWSAccessKeyId == 'velma'
            AWSSecretKey == 'daphne'
        }
    }

    def "configure should use publicity accounts if present"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        ConfigFactory factory = new ConfigFactory(environment, 'unittest')
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter_credentials", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")
        envVars.set("twitch_access_token", "${filler}fred")
        envVars.set("twitch_client_id", "${filler}shaggy")
        envVars.set("twitch_target_username", "${filler}${filler}${filler}scooby${filler}${filler}")
        envVars.set("CRED_AWS_ACCESS_KEY", "${filler}${filler}velma${filler}${filler}${filler}")
        envVars.set("CRED_AWS_SECRET_KEY", "${filler}${filler}daphne${filler}${filler}${filler}")
        envVars.set("PUBLICITY_ACCOUNTS", "PorkyPig,DaffyDuck,Tweety")
        envVars.set("PorkyPig_twitter_credentials", "${filler}${filler}PorkyPig,bedeebedee,thats," +
                "all,folks${filler}")
        envVars.set("DaffyDuck_twitter_credentials", "${filler}${filler}DaffyDuck,wild,duck," +
                "youre,despicable${filler}")
        envVars.set("Tweety_twitter_credentials", "${filler}${filler}Tweety,itaught,itaw,aputty," +
                "tat${filler}")

        when:
        Config overallConfig = factory.configure()
        List<Configuration> publicityConfigs = overallConfig.publicityConfigs
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        AWSCredentials awsCredentials = overallConfig.getCredentials()

        then:
        with(overallConfig) {
            clientId == 'shaggy'
            accessToken == 'fred'
            targetUsername == 'scooby'
        }
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
        with(publicityConfigs[0]) {
            user == 'PorkyPig'
            OAuthConsumerKey == 'bedeebedee'
            OAuthConsumerSecret == 'thats'
            OAuthAccessToken == 'all'
            OAuthAccessTokenSecret == 'folks'
        }
        with(publicityConfigs[1]) {
            user == 'DaffyDuck'
            OAuthConsumerKey == 'wild'
            OAuthConsumerSecret == 'duck'
            OAuthAccessToken == 'youre'
            OAuthAccessTokenSecret == 'despicable'
        }
        with(publicityConfigs[2]) {
            user == 'Tweety'
            OAuthConsumerKey == 'itaught'
            OAuthConsumerSecret == 'itaw'
            OAuthAccessToken == 'aputty'
            OAuthAccessTokenSecret == 'tat'
        }
        with(awsCredentials) {
            AWSAccessKeyId == 'velma'
            AWSSecretKey == 'daphne'
        }
    }

    def "configure should still work if publicity accounts is an empty string if present"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        ConfigFactory factory = new ConfigFactory(environment, 'unittest')
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter_credentials", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")
        envVars.set("twitch_access_token", "${filler}fred")
        envVars.set("twitch_client_id", "${filler}shaggy")
        envVars.set("twitch_target_username", "${filler}${filler}${filler}scooby${filler}${filler}")
        envVars.set("CRED_AWS_ACCESS_KEY", "${filler}${filler}velma${filler}${filler}${filler}")
        envVars.set("CRED_AWS_SECRET_KEY", "${filler}${filler}daphne${filler}${filler}${filler}")
        envVars.set("PUBLICITY_ACCOUNTS", "")

        when:
        Config overallConfig = factory.configure()
        List<Configuration> publicityConfigs = overallConfig.publicityConfigs
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        AWSCredentials awsCredentials = overallConfig.getCredentials()

        then:
        with(overallConfig) {
            clientId == 'shaggy'
            accessToken == 'fred'
            targetUsername == 'scooby'
        }
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
        publicityConfigs == []
        with(awsCredentials) {
            AWSAccessKeyId == 'velma'
            AWSSecretKey == 'daphne'
        }
    }

    def "configure should still work if publicity accounts references missing env vars"() {
        setup:
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        ConfigFactory factory = new ConfigFactory(environment, 'unittest')
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter_credentials", "${filler}cartoons,bartsimpson,snaggletooth," +
                "fredflintstone,bugsbunny${filler}")
        envVars.set("twitch_access_token", "${filler}fred")
        envVars.set("twitch_client_id", "${filler}shaggy")
        envVars.set("twitch_target_username", "${filler}${filler}${filler}scooby${filler}${filler}")
        envVars.set("CRED_AWS_ACCESS_KEY", "${filler}${filler}velma${filler}${filler}${filler}")
        envVars.set("CRED_AWS_SECRET_KEY", "${filler}${filler}daphne${filler}${filler}${filler}")
        envVars.set("PUBLICITY_ACCOUNTS", "notfound,alsonotfound")

        when:
        Config overallConfig = factory.configure()
        List<Configuration> publicityConfigs = overallConfig.publicityConfigs
        Configuration twitterConfig = overallConfig.getTwitterConfig()
        AWSCredentials awsCredentials = overallConfig.getCredentials()

        then:
        with(overallConfig) {
            clientId == 'shaggy'
            accessToken == 'fred'
            targetUsername == 'scooby'
        }
        with(twitterConfig) {
            OAuthConsumerKey == 'bartsimpson'
            OAuthConsumerSecret == 'snaggletooth'
            OAuthAccessToken == 'fredflintstone'
            OAuthAccessTokenSecret == 'bugsbunny'
        }
        publicityConfigs == []
        with(awsCredentials) {
            AWSAccessKeyId == 'velma'
            AWSSecretKey == 'daphne'
        }
    }
}
