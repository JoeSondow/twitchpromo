package sondow.twitchpromo

import twitter4j.MediaEntity

class FakeMediaEntity implements MediaEntity {

    @Override
    long getId() {
        return 0
    }

    @Override
    String getMediaURL() {
        return null
    }

    @Override
    String getMediaURLHttps() {
        return null
    }

    @Override
    Map<Integer, Size> getSizes() {
        return null
    }

    @Override
    String getType() {
        return null
    }

    @Override
    int getVideoAspectRatioWidth() {
        return 0
    }

    @Override
    int getVideoAspectRatioHeight() {
        return 0
    }

    @Override
    long getVideoDurationMillis() {
        return 0
    }

    @Override
    Variant[] getVideoVariants() {
        return new Variant[0]
    }

    @Override
    String getExtAltText() {
        return null
    }

    @Override
    String getText() {
        return null
    }

    @Override
    String getURL() {
        return null
    }

    @Override
    String getExpandedURL() {
        return null
    }

    @Override
    String getDisplayURL() {
        return null
    }

    @Override
    int getStart() {
        return 0
    }

    @Override
    int getEnd() {
        return 0
    }
}


