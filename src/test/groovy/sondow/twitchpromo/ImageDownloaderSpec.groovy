package sondow.twitchpromo

import spock.lang.Specification

class ImageDownloaderSpec extends Specification {

    def "should build image url"() {
        setup:
        Config config = new Config('unittest', 'xyzaccesstoken', 5, null, "sam", null, null, 700,
                500,
                null, null,
                '', '', '', [],
                39, 59, 79, false)

        when:
        ImageUrls imageUrls = new ImageDownloader().buildImageUrls(config)
        String previewUrl = imageUrls.previewUrl
        String notFoundUrl = imageUrls.notFoundUrl

        then:
        'https://static-cdn.jtvnw.net/previews-ttv/live_user_sam-700x500.jpg' == previewUrl
        'https://static-cdn.jtvnw.net/ttv-static/404_preview-700x500.jpg' == notFoundUrl
    }
}
