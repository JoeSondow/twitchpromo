package sondow.twitchpromo

import com.amazonaws.auth.BasicAWSCredentials
import com.sybit.airtable.Airtable
import com.sybit.airtable.Base
import com.sybit.airtable.Query
import com.sybit.airtable.Table
import com.sybit.airtable.exception.AirtableException
import sondow.common.Time
import spock.lang.Specification
import twitter4j.conf.Configuration
import static sondow.twitchpromo.Dates.d
import static sondow.twitchpromo.StoredTweet.CORE_TEXT_KEY
import static sondow.twitchpromo.StoredTweet.TIMESTAMP_KEY
import static sondow.twitchpromo.StoredTweet.TWEET_ID_KEY

class AirtableDatabaseSpec extends Specification {

    def "storing tweet in database should retry a few times on error"() {
        setup:
        Airtable airtable = Mock()
        Configuration twitterConfig = Mock()
        Config config = new Config('unittest', 'xyzaccesstoken', 5, 'abcclientid',
                'BarbaraTweets', '12345', '/tmp',
                1920, 1280,
                twitterConfig, new BasicAWSCredentials('ac', 'sk'), 'atpk', 'ab', '', [], 0, 0,
                0, false)
        StoredTweet tweet = new StoredTweet("435", "Blah",
                d('2018-04-30T12:45:56Z'), "BarbaraTweets", null)
        Base base = Mock()
        Table<StoredTweet> table = Mock()
        String tblName = 'TWITCHPROMO_TWEETS_BARBARATWEETS'
        Time time = Mock()

        when:
        AirtableDatabase database = new AirtableDatabase(config, airtable, time)
        database.storeTweet(tweet)

        then:
        1 * twitterConfig.getUser() >> 'BarbaraTweets'
        1 * airtable.base('ab') >> base
        1 * base.table(tblName, StoredTweet.class) >> table
        9 * table.create(tweet) >> { throw new AirtableException("borked") }
        1 * time.sleep(250)
        1 * time.sleep(500)
        1 * time.sleep(1000)
        1 * time.sleep(2000)
        1 * time.sleep(4000)
        1 * time.sleep(8000)
        1 * time.sleep(16000)
        1 * time.sleep(32000)
        thrown CollectedExceptions
        0 * _._
    }

    def "tweet should get stored in the database"() {
        setup:
        Airtable airtable = Mock()
        Configuration twitterConfig = Mock()
        Config config = new Config('unittest', 'xyzaccesstoken', 5, 'abcclientid',
                'BarbaraTweets', '12345', '/tmp',
                1920, 1280,
                twitterConfig, new BasicAWSCredentials('ac', 'sk'), 'atpk', 'ab', '', [], 0, 0,
                0, false)

        String timestampString = '2018-04-30T12:45:56Z'
        Date dateTime = d(timestampString)
        String coreText = "Come hang out!"
        StoredTweet tweet = new StoredTweet("435", coreText, dateTime, "BarbaraTweets", inReply)
        StoredTweet result = new StoredTweet("435", coreText, dateTime, "BarbaraTweets", inReply)
        result.setId('rec123')
        Base base = Mock()
        Table<StoredTweet> table = Mock()
        String tblName = 'TWITCHPROMO_TWEETS_BARBARATWEETS'
        Time time = Mock()

        when:
        AirtableDatabase database = new AirtableDatabase(config, airtable, time)
        database.storeTweet(tweet)

        then:
        1 * twitterConfig.getUser() >> 'BarbaraTweets'
        1 * airtable.base('ab') >> base
        1 * base.table(tblName, StoredTweet.class) >> table
        1 * table.create(tweet) >> result
        0 * _._

        where:
        inReply | inReplyToStatusId
        null    | null
        '35345' | '35345'
    }

    def 'should retrieve recent tweets from database'() {
        setup:
        Airtable airtable = Mock()
        Configuration twitterConfig = Mock()
        Config config = new Config('unittest', 'xyzaccesstoken', 5, 'abcclientid',
                'BarbaraTweets', '12345', '/tmp',
                1920, 1280,
                twitterConfig, new BasicAWSCredentials('ac', 'sk'), 'atpk', 'ab', '', [], 0, 0,
                0, false)
        Base base = Mock()
        Table<StoredTweet> table = Mock()
        String tblName = 'TWITCHPROMO_TWEETS_HOTTAKES'

        List<StoredTweet> expected = [
                new StoredTweet('968673', 'I love you!',
                        d('2018-05-01T14:02:45Z'), 'HotTakes',
                        null),
                new StoredTweet('765432', 'Hello there!',
                        d('2018-05-02T14:02:45Z'), 'HotTakes',
                        '968673')]
        Query query = new AirtableQuery("AND(LOWER(twitter_handle) = LOWER('HotTakes'), timestamp" +
                " >= DATEADD(TODAY(), -5, 'days'))", [TWEET_ID_KEY, CORE_TEXT_KEY, TIMESTAMP_KEY])
        Time time = Mock()

        when:
        AirtableDatabase database = new AirtableDatabase(config, airtable, time)
        List<StoredTweet> tweets = database.retrieveRecentTweets(5, 'HotTakes')

        then:
        tweets == expected
        1 * twitterConfig.getUser() >> 'HotTakes'
        1 * airtable.base('ab') >> base
        1 * base.table(tblName, StoredTweet.class) >> table
        1 * table.select(query) >> expected
        0 * _._
    }

    def 'should mark tweet deleted in database'() {
        setup:
        Airtable airtable = Mock()
        Configuration twitterConfig = Mock()
        Config config = new Config('unittest', 'xyzaccesstoken', 5, 'abcclientid',
                'BarbaraTweets', '12345', '/tmp',
                1920, 1280,
                twitterConfig, new BasicAWSCredentials('ac', 'sk'), 'atpk', 'ab', '', [], 0, 0,
                0, false)
        Base base = Mock()
        Table<StoredTweet> table = Mock()
        String tblName = 'TWITCHPROMO_TWEETS_BARBARATWEETS'
        Query query = new AirtableQuery("tweet_id = 98", [])
        StoredTweet retrieved = new StoredTweet('98', 'I love you!',
                d('2018-05-01T14:02:45Z'), 'BarbaraTweets', null)
        List<StoredTweet> expired = [retrieved]
        StoredTweet updated = new StoredTweet('98', 'I love you!',
                d('2018-05-01T14:02:45Z'), 'BarbaraTweets', null)
        updated.setDeleted(true)
        Time time = Mock()

        when:
        AirtableDatabase database = new AirtableDatabase(config, airtable, time)
        database.markTweetDeleted(98L)

        then:
        1 * twitterConfig.getUser() >> 'BarbaraTweets'
        1 * airtable.base('ab') >> base
        1 * base.table(tblName, StoredTweet.class) >> table
        1 * table.select(query) >> expired
        1 * table.update(updated) >> updated
        0 * _._
    }

    def 'should remove expired items'() {
        setup:
        Airtable airtable = Mock()
        Configuration twitterConfig = Mock()
        Config config = new Config('unittest', 'xyzaccesstoken', 5, 'abcclientid',
                'BarbaraTweets', '12345', '/tmp',
                1920, 1280,
                twitterConfig, new BasicAWSCredentials('ac', 'sk'), 'atpk', 'ab', '', [], 0, 0,
                0, false)
        Base base = Mock()
        Table<StoredTweet> table = Mock()
        String tblName = 'TWITCHPROMO_TWEETS_HOTTAKES'
        StoredTweet tweet1 = new StoredTweet('968673', 'I love you!',
                d('2018-05-01T14:02:45Z'), 'HotTakes', null)
        tweet1.setId('rec111')
        StoredTweet tweet2 = new StoredTweet('765432', 'Hello there!',
                d('2018-05-02T14:02:45Z'), 'HotTakes', '968673')
        tweet2.setId('rec222')
        List<StoredTweet> expected = [tweet1, tweet2]
        Query query = new AirtableQuery("AND(LOWER(twitter_handle) = LOWER('HotTakes'), timestamp" +
                " < DATEADD(TODAY(), -5, 'days'))", [TWEET_ID_KEY])
        Time time = Mock()

        when:
        AirtableDatabase database = new AirtableDatabase(config, airtable, time)
        database.removeExpiredItems(5, 'HotTakes')

        then:
        1 * twitterConfig.getUser() >> 'HotTakes'
        1 * airtable.base('ab') >> base
        1 * base.table(tblName, StoredTweet.class) >> table
        1 * table.select(query) >> expected
        1 * table.destroy('rec111')
        1 * table.destroy('rec222')
        0 * _._
    }
}
