package sondow.twitchpromo

import twitter4j.RateLimitStatus
import twitter4j.Status
import twitter4j.URLEntity
import twitter4j.User

/**
 * Simple implementation of User, for unit testing only.
 */
class TestUser implements User {

    long id

    /**
     * Returns the name of the user
     *
     * @return the name of the user
     */
    @Override
    String getName() {
        return null
    }

    /**
     * Returns the email of the user, if the app is whitelisted by Twitter
     *
     * @return the email of the user
     */
    @Override
    String getEmail() {
        return null
    }

    /**
     * Returns the screen name of the user
     *
     * @return the screen name of the user
     */
    @Override
    String getScreenName() {
        return null
    }

    /**
     * Returns the location of the user
     *
     * @return the location of the user
     */
    @Override
    String getLocation() {
        return null
    }

    /**
     * Returns the description of the user
     *
     * @return the description of the user
     */
    @Override
    String getDescription() {
        return null
    }

    /**
     * Tests if the user is enabling contributors
     *
     * @return if the user is enabling contributors
     * @since Twitter4J 2.1.2
     */
    @Override
    boolean isContributorsEnabled() {
        return false
    }

    /**
     * Returns the profile image url of the user
     *
     * @return the profile image url of the user
     */
    @Override
    String getProfileImageURL() {
        return null
    }

    @Override
    String getBiggerProfileImageURL() {
        return null
    }

    @Override
    String getMiniProfileImageURL() {
        return null
    }

    @Override
    String getOriginalProfileImageURL() {
        return null
    }

    @Override
    String get400x400ProfileImageURL() {
        return null
    }

    @Override
    String getProfileImageURLHttps() {
        return null
    }

    @Override
    String getBiggerProfileImageURLHttps() {
        return null
    }

    @Override
    String getMiniProfileImageURLHttps() {
        return null
    }

    @Override
    String getOriginalProfileImageURLHttps() {
        return null
    }

    @Override
    String get400x400ProfileImageURLHttps() {
        return null
    }

    /**
     * Tests if the user has not uploaded their own avatar
     *
     * @return if the user has not uploaded their own avatar
     */
    @Override
    boolean isDefaultProfileImage() {
        return false
    }

    /**
     * Returns the url of the user
     *
     * @return the url of the user
     */
    @Override
    String getURL() {
        return null
    }

    /**
     * Test if the user status is protected
     *
     * @return true if the user status is protected
     */
    @Override
    boolean isProtected() {
        return false
    }

    /**
     * Returns the number of followers
     *
     * @return the number of followers
     * @since Twitter4J 1.0.4
     */
    @Override
    int getFollowersCount() {
        return 0
    }

    /**
     * Returns the current status of the user<br>
     * This can be null if the instance if from Status.getUser().
     *
     * @return current status of the user
     * @since Twitter4J 2.1.1
     */
    @Override
    Status getStatus() {
        return null
    }

    @Override
    String getProfileBackgroundColor() {
        return null
    }

    @Override
    String getProfileTextColor() {
        return null
    }

    @Override
    String getProfileLinkColor() {
        return null
    }

    @Override
    String getProfileSidebarFillColor() {
        return null
    }

    @Override
    String getProfileSidebarBorderColor() {
        return null
    }

    @Override
    boolean isProfileUseBackgroundImage() {
        return false
    }

    /**
     * Tests if the user has not altered the theme or background
     *
     * @return if the user has not altered the theme or background
     */
    @Override
    boolean isDefaultProfile() {
        return false
    }

    @Override
    boolean isShowAllInlineMedia() {
        return false
    }

    /**
     * Returns the number of users the user follows (AKA "followings")
     *
     * @return the number of users the user follows
     */
    @Override
    int getFriendsCount() {
        return 0
    }

    @Override
    Date getCreatedAt() {
        return null
    }

    @Override
    int getFavouritesCount() {
        return 0
    }

    @Override
    int getUtcOffset() {
        return 0
    }

    @Override
    String getTimeZone() {
        return null
    }

    @Override
    String getProfileBackgroundImageURL() {
        return null
    }

    @Override
    String getProfileBackgroundImageUrlHttps() {
        return null
    }

    /**
     * @since Twitter4J 3.0.0
     * @return profile banner URL
     */
    @Override
    String getProfileBannerURL() {
        return null
    }

    /**
     * @since Twitter4J 3.0.0
     * @return profile banner retina URL
     */
    @Override
    String getProfileBannerRetinaURL() {
        return null
    }

    /**
     * @since Twitter4J 3.0.0
     * @return profile banner iPad URL
     */
    @Override
    String getProfileBannerIPadURL() {
        return null
    }

    /**
     * @since Twitter4J 3.0.0
     * @return profile banner iPad retina URL
     */
    @Override
    String getProfileBannerIPadRetinaURL() {
        return null
    }

    /**
     * @since Twitter4J 3.0.0
     * @return profile banner mobile URL
     */
    @Override
    String getProfileBannerMobileURL() {
        return null
    }

    /**
     * @since Twitter4J 3.0.0
     * @return profile banner mobile retina URL
     */
    @Override
    String getProfileBannerMobileRetinaURL() {
        return null
    }

    @Override
    String getProfileBanner300x100URL() {
        return null
    }

    @Override
    String getProfileBanner600x200URL() {
        return null
    }

    @Override
    String getProfileBanner1500x500URL() {
        return null
    }

    @Override
    boolean isProfileBackgroundTiled() {
        return false
    }

    /**
     * Returns the preferred language of the user
     *
     * @return the preferred language of the user
     * @since Twitter4J 2.1.2
     */
    @Override
    String getLang() {
        return null
    }

    @Override
    int getStatusesCount() {
        return 0
    }

    /**
     * @return the user is enabling geo location
     * @since Twitter4J 2.0.10
     */
    @Override
    boolean isGeoEnabled() {
        return false
    }

    /**
     * @return returns true if the user is a verified celebrity
     * @since Twitter4J 2.0.10
     */
    @Override
    boolean isVerified() {
        return false
    }

    /**
     * @return returns true if the user is a translator
     * @since Twitter4J 2.1.9
     */
    @Override
    boolean isTranslator() {
        return false
    }

    /**
     * Returns the number of public lists the user is listed on, or -1
     * if the count is unavailable.
     *
     * @return the number of public lists the user is listed on.
     * @since Twitter4J 2.1.4
     */
    @Override
    int getListedCount() {
        return 0
    }

    /**
     * Returns true if the authenticating user has requested to follow this user,
     * otherwise false.
     *
     * @return true if the authenticating user has requested to follow this user.
     * @since Twitter4J 2.1.4
     */
    @Override
    boolean isFollowRequestSent() {
        return false
    }

    /**
     * Returns URL entities for user description.
     *
     * @return URL entities for user description
     * @since Twitter4J 3.0.3
     */
    @Override
    URLEntity[] getDescriptionURLEntities() {
        return new URLEntity[0]
    }

    /**
     * Returns URL entity for user's URL.
     *
     * @return URL entity for user's URL.
     * @since Twitter4J 3.0.3
     */
    @Override
    URLEntity getURLEntity() {
        return null
    }

    /**
     *  Returns the list of country codes where the user is withheld
     *
     * @return list of country codes where the tweet is withheld - null if not withheld
     * @since Twitter4j 4.0.3
     */
    @Override
    String[] getWithheldInCountries() {
        return new String[0]
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
     * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
     * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
     * <tt>y.compareTo(x)</tt> throws an exception.)
     *
     * <p>The implementor must also ensure that the relation is transitive:
     * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
     * <tt>x.compareTo(z)&gt;0</tt>.
     *
     * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
     * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
     * all <tt>z</tt>.
     *
     * <p>It is strongly recommended, but <i>not</i> strictly required that
     * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
     * class that implements the <tt>Comparable</tt> interface and violates
     * this condition should clearly indicate this fact.  The recommended
     * language is "Note: this class has a natural ordering that is
     * inconsistent with equals."
     *
     * <p>In the foregoing description, the notation
     * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
     * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
     * <tt>0</tt>, or <tt>1</tt> according to whether the value of
     * <i>expression</i> is negative, zero or positive.
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     *          is less than, equal to, or greater than the specified object.
     *
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException if the specified object's type prevents it
     *         from being compared to this object.
     */
    @Override
    int compareTo(User o) {
        return 0
    }

    /**
     * Returns the current rate limit status if available.
     *
     * @return current rate limit status
     * @since Twitter4J 2.1.0
     */
    @Override
    RateLimitStatus getRateLimitStatus() {
        return null
    }

    /**
     * @return application permission model
     * @see <ahref="https://dev.twitter
     * .com/pages/application-permission-model-faq#how-do-we-know-what-the-access-level-of-a-user
     * -token-is"  >  Application Permission Model FAQ - How do we know what the access level of a
     * user token is?</a>
     * @since Twitter4J 2.2.3
     */
    @Override
    int getAccessLevel() {
        return 0
    }
}
