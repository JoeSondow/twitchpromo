package sondow.twitchpromo

import spock.lang.Specification

import java.time.ZonedDateTime

class ZonedDateTimeSpec extends Specification {

    def "should read in and output ISO8601 format"() {
        when:
        ZonedDateTime dateTime = ZonedDateTime.parse("2018-01-03T10:45:32Z")

        then:
        dateTime.toString() == "2018-01-03T10:45:32Z"
    }
}
