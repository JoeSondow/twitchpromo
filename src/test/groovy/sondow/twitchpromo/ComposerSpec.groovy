package sondow.twitchpromo

import sondow.common.FileClerk
import spock.lang.Specification
import spock.lang.Unroll
import static sondow.twitchpromo.Dates.d

class ComposerSpec extends Specification {

    @Unroll("it is #result that '#a' is similar to '#b'")
    def 'should determine whether two strings are similar'() {

        when:
        boolean similar = Composer.areStringsSimilar(a, b)

        then:
        similar == result

        where:
        a                                         | b                                      | result
        ''                                        | ''                                     | true
        null                                      | ''                                     | false
        null                                      | 'sdf'                                  | false
        ''                                        | null                                   | false
        'ssdf'                                    | null                                   | false
        null                                      | null                                   | false
        'A'                                       | 'A'                                    | true
        'The only winning move is not to play'    | 'The only winning move is not to play' | true
        'The only losing move is not to play'     | 'The only winning move is not to play' | true
        "The only winnin' move is not to play"    | "The only winning move is not to play" | true
        "The only triumphant move is not to play" | "The only winning move is not to play" | true
        "The best way to play is not to play"     | "The only winning move is not to play" | false
        "Just for good measure"                   | "The only winning move is not to play" | false
    }

    List<String> programmingMessages = [
            "Live streaming my process for coding friendly twitter bots.",
            "Doing some coding on a Twitter bot. Feel free to come ask questions.",
            "Live streaming some Twitter bot coding.",
            "Making Twitter bots.",
            "🎵 Twitter bots\n" +
                    "🎵 Twitter bots\n" +
                    "🎵 I’m coding bots… with emoji\n" +
                    "\n" +
                    "🎵 Tweet-a-leet\n" +
                    "🎵 See them tweet\n" +
                    "🎵 Soon it will be git push day",
            "🎶 Domo Arigato, Twitter Roboto!\n" +
                    "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98…\n" +
                    "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98… ",
            "🎶 Codin' robots, codin' Twitter robots,\n" +
                    "🎶 Got some Twitter and I put it in a robot.\n" +
                    "🎶 Twitter robots, that's what I code a lot,\n" +
                    "🎶 Twitter ro-bottttts!",
            "🎶 Supercalifragilistic-coding-twitter-ro-bots\n" +
                    "🎶 Even though the sound of it reminds us all of Go-Bots",
            "Hanging out with chat while I make progress on one of my Twitter bots.",
            "Doing a bit of coding on twitch.",
            "Yo dawg, I heard you like technology so I put some technology in your technology" +
                    " so you can technology while you technology.",
            "Working on one of my twitter bots, chatting with people who stop by.",
            "Programming. Talking. Drinking. Goofing off."
    ]

    List<StoredTweet> recentTweets1 = [
            new StoredTweet('991821148995252224', "Live streaming my process for coding friendly " +
                    "twitter bots.", d("2018-05-02T23:26:03Z"),
                    "JoeSondow", '991811234222649344'),
            new StoredTweet('991844291013046272', "I think I program better when Im doing it out " +
                    "loud on stream.", d("2018-05-03T00:58:01Z"),
                    "JoeSondow", '991821148995252224'),
            new StoredTweet('991854123736694784', "🎶 Codin' robots, codin' " +
                    "Twitter robots,\n🎶 Got some Twitter and I put it in a robot.\n🎶 " +
                    "Twitter robots, that's what I code a lot,\n🎶 Twitter ro-bottttts!",
                    d("2018-05-03T01:37:05Z"), "JoeSondow",
                    '991844291013046272'),
            new StoredTweet('991869198128242688', "Programming. Talking. Drinking. Goofing off.",
                    d("2018-05-03T02:36:59Z"), "JoeSondow",
                    '991854123736694784'),
            new StoredTweet('992582087017349120', "🎵 Twitter bots\n🎵  Twitter bots\n🎵 Im " +
                    "coding" +
                    " bots with emoji\n\n🎵 Tweet-a-leet\n🎵 See them tweet\n🎵 Soon it will be" +
                    " git push day", d("2018-05-05T01:49:45Z"),
                    "JoeSondow", '992571568134897664'),
            new StoredTweet('992597184414605312', "🎶 Domo Arigato, Twitter Roboto!\n🎶  Domo. " +
                    "𝙳𝚘𝚖𝚘…\n🎶 Domo. 𝙳𝚘𝚖𝚘…", d("2018-05-05T02:49:44Z"),
                    "JoeSondow", '992582087017349120'),
            new StoredTweet('992868969223569408', "Yo dawg, I heard you like technology so I put " +
                    "some technology in your technology so you can technology while you " +
                    "technology.", d("2018-05-05T20:49:43Z"),
                    "JoeSondow", '992857726832205824'),
            new StoredTweet('992895641389907968', "Working on one of my twitter bots, chatting " +
                    "with people who stop by.", d("2018-05-05T22:35:42Z"),
                    "JoeSondow", '992868969223569408'),
            new StoredTweet('992907976372502528', "Doing a bit of " +
                    "coding on twitch.", d("2018-05-05T23:24:43Z"),
                    "JoeSondow", '992895641389907968')
    ]

    List<StoredTweet> recentTweets2 = [

            new StoredTweet('992919553431687168', "Working " +
                    "on a Twitter bot while I live stream.",
                    d("2018-05-06T00:10:43Z"), "JoeSondow", '992907976372502528'),
            new StoredTweet('992930121613885440', "Come hang out while I talk through my " +
                    "process of programming a Twitter bot.",
                    d("2018-05-06T00:52:43Z"), "JoeSondow", '992919553431687168'),
            new StoredTweet('996927940423073792',
                    "Doing some coding on a Twitter bot. Feel free to come ask questions" +
                            ".", d("2018-05-17T01:38:37Z"),
                    "JoeSondow", '996901321667108864'),
            new StoredTweet('996941275248017408',
                    "🎶 Supercalifragilistic-coding-twitter-ro-bots\n🎶 Even though the " +
                            "sound of it reminds us all of Go-Bots",
                    d("2018-05-17T02:31:36Z"), "JoeSondow", '996927940423073792'),
            new StoredTweet('996952097315680256', "Making Twitter bots.",
                    d("2018-05-17T03:14:37Z"), "JoeSondow", '996941275248017408'),
            new StoredTweet('996963422905417729',
                    "Hanging out with chat while I make progress on one of my Twitter " +
                            "bots.", d("2018-05-17T03:59:37Z"),
                    "JoeSondow", '996952097315680256'),
            new StoredTweet('996974245749055493', "Live streaming some Twitter bot " +
                    "coding.", d("2018-05-17T04:42:37Z"),
                    "JoeSondow", '996963422905417729')
    ]

    List<StoredTweet> recentTweetsAll = recentTweets1 + recentTweets2

    def 'should find no relevant messages that have not been tweeted recently if all tweeted'() {
        setup:
        Composer composer = new Composer(new Random(), null)
        List<String> msgs = programmingMessages
        List<StoredTweet> tweets = recentTweetsAll

        when:
        List<String> unusedMsgs = composer.findUnusedMessages(msgs, tweets)

        then:
        unusedMsgs == []
    }

    def 'should find all relevant messages if no recent tweets'() {
        setup:
        TextFileProvider textFileProvider = Mock()
        Composer composer = new Composer(null, textFileProvider)

        when:
        List<String> unused = composer.findUnusedMessages(programmingMessages, [])

        then:
        unused == programmingMessages
    }

    def 'should find relevant messages that have not been tweeted recently'() {
        setup:
        Composer composer = new Composer(new Random(), null)
        List<String> msgs = [
                "Live streaming my process for coding friendly twitter bots.",
                "Doing some coding on a Twitter bot. Feel free to come ask questions.",
                "Live streaming some Twitter bot coding.",
                "Making Twitter bots.",
                "🎶 Domo Arigato, Twitter Roboto!\n" +
                        "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98…\n" +
                        "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98… ",
                "🎶 Supercalifragilistic-coding-twitter-ro-bots\n" +
                        "🎶 Even though the sound of it reminds us all of Go-Bots",
                "Hanging out with chat while I make progress on one of my Twitter bots.",
                "Doing a bit of coding on twitch.",
                "Yo dawg, I heard you like technology so I put some technology in your technology" +
                        " so you can technology while you technology.",
                "Working on one of my twitter bots, chatting with people who stop by.",
                "Programming. Talking. Drinking. Goofing off."
        ]
        List<StoredTweet> tweets = [
                tweet('Live streaming my process for coding friendly twitter bots.'),
                tweet('I think I program better when I’m doing it out loud on stream.'),
                tweet('Programming. Talking. Drinking. Goofing off.'),
                tweet("🎶 Domo Arigato, Twitter Roboto!\n🎶 Domo. 𝙳𝚘𝚖𝚘…\n🎶 Domo. 𝙳𝚘𝚖𝚘…"),
                tweet("Yo dawg, I heard you like technology so I put some technology in your " +
                        "technology so you can technology while you technology."),
                tweet("Working on one of my twitter bots, chatting with people who stop by."),
                tweet("Doing a bit of coding on twitch.")
        ]

        when:
        List<String> unusedMsgs = composer.findUnusedMessages(msgs, tweets)

        then:
        unusedMsgs == [
                'Doing some coding on a Twitter bot. Feel free to come ask questions.',
                'Live streaming some Twitter bot coding.',
                'Making Twitter bots.',
                '🎶 Supercalifragilistic-coding-twitter-ro-bots\n' +
                        '🎶 Even though the sound of it reminds us all of Go-Bots',
                'Hanging out with chat while I make progress on one of my Twitter bots.'
        ]
    }

    private static StoredTweet tweet(String text) {
        tweet('2018-05-02T03:05:34Z', text)
    }

    private static StoredTweet tweet(String timeStampString, String text) {
        Date timeStamp = d(timeStampString)
        new StoredTweet(null, text, timeStamp, null, null)
    }

    def "should compose an appropriate message for current stream when recent tweets used title"() {
        setup:
        TextFileProvider textFileProvider = Mock()
        Random random = new Random(seed)
        Composer composer = new Composer(random, textFileProvider)
        String json = new FileClerk().readTextFile('test-messages-long.json')
        Stream stream = buildStream(game, title, 4)
        List<StoredTweet> recentTweets = [tweet(title), tweet("Hello")]
        String user = "Joe"

        when:
        String message = composer.compose(new Config('unittest', 'xyzaccesstoken', 5, null,
                user, null, null, 1900, 1200, null, null, '', '', 'mock-url-here', [], 39, 59,
                79, false), recentTweets, stream).getFullMessage()

        then:
        1 * textFileProvider.download('mock-url-here') >> json
        0 * _._
        message == output

        where:
        seed | game          | title       | output
        11   | "Board Games" | "Playin Z"  | "Board game stream. https://twitch.tv/Joe"
        293  | "Art"         | "Coloring!" | "Super chill coloring stream. https://twitch.tv/Joe"
        36   | "Alien"       | "So spoopy" |
                "Let's hang out while I play Alien. https://twitch.tv/Joe"
        37   | "Alien"       | "So spoopy" | "Live on Twitch playing Alien. https://twitch.tv/Joe"
    }

    List may3Tweets = [
            new StoredTweet('991844291013046272', "I think I" +
                    " program better when Im doing it out loud on stream.",
                    d("2018-05-03T00:58:01Z"),
                    "JoeSondow", '991821148995252224'),
            new StoredTweet('991854123736694784', "Codin' robots, codin' Twitter robots,\n Got " +
                    "some Twitter and I put it in a robot.\n Twitter robots, that's what I code a" +
                    " lot,\n Twitter ro-bottttts!", d("2018-05-03T01:37:05Z"),
                    "JoeSondow", '991844291013046272'),
            new StoredTweet('991869198128242688', "Programming. Talking. " +
                    "Drinking. Goofing off.", d("2018-05-03T02:36:59Z"),
                    "JoeSondow", '991854123736694784'),
            new StoredTweet('991880894444007429', "Working on adding features and fixing bugs in " +
                    "my bot that tweets twitch screenshots.", d("2018-05-03T03:23:28Z"),
                    "JoeSondow", '991869198128242688'),
            new StoredTweet('991906642257051648', "Working on adding features and fixing bugs in " +
                    "my bot that tweets twitch screenshots.", d("2018-05-03T05:05:46Z"),
                    "JoeSondow", '991880894444007429'),
            new StoredTweet('991917432141897728', "Working on adding features and fixing bugs in " +
                    "my bot that tweets twitch screenshots.", d("2018-05-03T05:48:39Z"),
                    "JoeSondow", '991906642257051648'),
            new StoredTweet('991927958016147458', "Working on adding features and fixing bugs in " +
                    "my bot that tweets twitch screenshots.", d("2018-05-03T06:30:28Z"),
                    "JoeSondow", '991917432141897728'),
            new StoredTweet('991938455524425728', "Working on adding features and fixing bugs in " +
                    "my bot that tweets twitch screenshots.", d("2018-05-03T07:12:11Z"),
                    "JoeSondow", '991927958016147458'),
            new StoredTweet('991948325313167362', "Working on adding features and fixing bugs in " +
                    "my bot that tweets twitch screenshots.", d("2018-05-03T07:51:24Z"),
                    "JoeSondow", '991938455524425728'),
    ]

    def "should compose a message for stream when recent tweets used title, real example"() {
        setup:
        TextFileProvider textFileProvider = Mock()
        Composer composer = new Composer(new Random(11), textFileProvider)
        String title = "Working on adding features and fixing bugs in my bot that tweets twitch " +
                "screenshots. 🐦"
        Stream stream = buildStream("Science & Technology", title, 4)
        List<StoredTweet> recentTweets = may3Tweets
        String json = new FileClerk().readTextFile('test-messages-long.json')

        when:
        String message = composer.compose(new Config('unittest', 'xyzaccesstoken', 5, null,
                "JoeSondow", null, null, 1900, 1200, null, null, '', '', 'mock-url-here', [], 39,
                59, 79, false), recentTweets, stream).getFullMessage()

        then:
        1 * textFileProvider.download('mock-url-here') >> json
        message == 'Doing some coding on a Twitter bot. Feel free to come ask questions. ' +
                'https://twitch.tv/JoeSondow'
        0 * _._
    }

    private static Stream buildStream(String game, String title, int viewerCount) {
        Stream stream = new Stream(game, title, viewerCount, null)
        return stream
    }

    def "should find most forgotten message"() {
        setup:
        Composer composer = new Composer(null, null)
        List<String> processedCoreMessages = [
                'Here I am', 'Come hang out', 'Hello there', 'Slow down you move too fast'
        ]
        List<StoredTweet> tweets = [
                tweet('2018-05-17T01:00:00Z', 'Hello there'),
                tweet('2018-05-17T02:00:00Z', 'Come hang out'),
                tweet('2018-05-17T03:00:00Z', 'Here I am'),
                tweet('2018-05-17T04:00:00Z', 'Slow down you move too fast'),
                tweet('2018-05-17T05:00:00Z', 'Come hang out'),
                tweet('2018-05-17T06:00:00Z', 'Hello there')
        ]

        when:
        String msg = composer.findMostForgottenMessage(processedCoreMessages, tweets)

        then:
        msg == 'Here I am'
    }

    def "should find most forgotten message when not all messages and tweets match each other"() {
        setup:
        Composer composer = new Composer(null, null)
        List<String> processedCoreMessages = [
                'Here I am', 'Come hang out', 'Hello there', "Oops this isn't used"
        ]
        List<StoredTweet> tweets = [
                tweet('2018-05-17T00:00:00Z', 'Here I am'),
                tweet('2018-05-17T01:00:00Z', 'Hello there'),
                tweet('2018-05-17T02:00:00Z', 'Come hang out'),
                tweet('2018-05-17T03:00:00Z', 'Here I am'),
                tweet('2018-05-17T04:00:00Z', 'Wait a minute this is random'),
                tweet('2018-05-17T05:00:00Z', 'Come hang out'),
                tweet('2018-05-17T06:00:00Z', 'Hello there')
        ]

        when:
        String msg = composer.findMostForgottenMessage(processedCoreMessages, tweets)

        then:
        msg == 'Here I am'
    }

    def "should find most forgotten message when tweets are out of order"() {
        setup:
        Composer composer = new Composer(null, null)
        List<String> processedCoreMessages = [
                'Here I am', 'Come hang out', 'Hello there', "Oops this isn't used"
        ]
        List<StoredTweet> tweets = [
                tweet('2018-05-17T02:00:00Z', 'Come hang out'),
                tweet('2018-05-17T04:00:00Z', 'Wait a minute this is random'),
                tweet('2018-05-17T05:00:00Z', 'Come hang out'),
                tweet('2018-05-17T01:00:00Z', 'Hello there'),
                tweet('2018-05-17T06:00:00Z', 'Hello there'),
                tweet('2018-05-17T03:00:00Z', 'Here I am')
        ]

        when:
        String msg = composer.findMostForgottenMessage(processedCoreMessages, tweets)

        then:
        msg == 'Here I am'
    }

    def "should find most forgotten message when there are tweets but no source core messages"() {
        setup:
        Composer composer = new Composer(null, null)
        List<String> processedCoreMessages = []
        List<StoredTweet> tweets = [
                tweet('2018-05-17T01:00:00Z', 'Hello there'),
                tweet('2018-05-17T02:00:00Z', 'Come hang out'),
                tweet('2018-05-17T03:00:00Z', 'Here I am'),
                tweet('2018-05-17T04:00:00Z', 'Wait a minute this is random'),
                tweet('2018-05-17T05:00:00Z', 'Come hang out'),
                tweet('2018-05-17T06:00:00Z', 'Hello there')
        ]

        when:
        String msg = composer.findMostForgottenMessage(processedCoreMessages, tweets)

        then:
        msg == null
    }

    def "should find most forgotten message when there are messages but no tweets"() {
        setup:
        Composer composer = new Composer(null, null)
        List<String> processedCoreMessages = [
                'Here I am', 'Come hang out', 'Hello there', 'Slow down you move too fast'
        ]
        List<StoredTweet> tweets = []

        when:
        String msg = composer.findMostForgottenMessage(processedCoreMessages, tweets)

        then:
        msg == null
    }

    def "should find most forgotten message where no tweets and messages match"() {
        setup:
        Composer composer = new Composer(null, null)

        List<String> processedCoreMessages = [
                'Hey there ho there', 'I love you', "What's up y'all"
        ]
        List<StoredTweet> tweets = [
                tweet('2018-05-17T01:00:00Z', 'Hello there'),
                tweet('2018-05-17T02:00:00Z', 'Come hang out'),
                tweet('2018-05-17T03:00:00Z', 'Here I am'),
                tweet('2018-05-17T04:00:00Z', 'Slow down you move too fast'),
                tweet('2018-05-17T05:00:00Z', 'Come hang out'),
                tweet('2018-05-17T06:00:00Z', 'Hello there')
        ]

        when:
        String msg = composer.findMostForgottenMessage(processedCoreMessages, tweets)

        then:
        msg == null
    }

    def "should pick a novel message when all messages have been tweeted"() {
        setup:
        Random random = new Random(99)
        Composer composer = new Composer(random, null)
        String title = "Turns out I unexpectedly have tonight free, so I'm streaming some more " +
                "progress fixing bugs in a twitter bot. 🤖"
        String msg1 = "Live streaming some Twitter bot coding."
        String msg2 = "#game# on stream."
        String msg3 = "🎶 Domo Arigato, Twitter Roboto!\n" +
                "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98…\n" +
                "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98… "
        List<String> sourceMsgs = [msg1, msg2, msg3]
        List<StoredTweet> recentTweets = [
                tweet('2018-05-17T01:00:00Z', msg3),
                tweet('2018-05-17T01:00:00Z', 'Some old title'),
                tweet('2018-05-17T02:00:00Z', msg1),
                tweet('2018-05-17T03:00:00Z', "Programming on stream."),
                tweet('2018-05-17T04:00:00Z', title),
                tweet('2018-05-17T05:00:00Z', msg3),
                tweet('2018-05-17T06:00:00Z', msg1)
        ]
        String game = 'Programming'

        when:
        String message = composer.pickNovelMessage(title, sourceMsgs, recentTweets, game)

        then:
        message == 'Programming on stream.'
    }

    def "should pick a novel message when one message has not been tweeted recently"() {
        setup:
        Random random = new Random(99)
        Composer composer = new Composer(random, null)
        String title = "Turns out I unexpectedly have tonight free, so I'm streaming some more " +
                "progress fixing bugs in a twitter bot. 🤖"
        String msg1 = "Live streaming some Twitter bot coding."
        String msg2 = "#game# on stream."
        String msg3 = "🎶 Domo Arigato, Twitter Roboto!\n" +
                "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98…\n" +
                "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98… "
        List<String> sourceMsgs = [msg1, msg2, msg3]
        List<StoredTweet> recentTweets = [
                tweet('2018-05-17T01:00:00Z', 'Some old title'),
                tweet('2018-05-17T02:00:00Z', msg1),
                tweet('2018-05-17T03:00:00Z', "Programming on stream."),
                tweet('2018-05-17T04:00:00Z', title),
                tweet('2018-05-17T06:00:00Z', msg1)
        ]
        String game = 'Programming'

        when:
        String message = composer.pickNovelMessage(title, sourceMsgs, recentTweets, game)

        then:
        message == msg3
    }

    def "should pick a novel message when two messages have not been tweeted recently"() {
        setup:
        Random random = new Random(seed)
        Composer composer = new Composer(random, null)
        String title = "Turns out I unexpectedly have tonight free, so I'm streaming some more " +
                "progress fixing bugs in a twitter bot. 🤖"
        String msg1 = "Live streaming some Twitter bot coding."
        String msg2 = "#game# on stream."
        String msg3 = "🎶 Domo Arigato, Twitter Roboto!\n" +
                "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98…\n" +
                "🎶 Domo. \uD835\uDE73\uD835\uDE98\uD835\uDE96\uD835\uDE98… "
        List<String> sourceMsgs = [msg1, msg2, msg3]
        List<StoredTweet> recentTweets = [
                tweet('2018-05-17T01:00:00Z', 'Some old title'),
                tweet('2018-05-17T02:00:00Z', msg3),
                tweet('2018-05-17T04:00:00Z', title),
                tweet('2018-05-17T06:00:00Z', msg3)
        ]
        String game = 'Programming'

        when:
        String message = composer.pickNovelMessage(title, sourceMsgs, recentTweets, game)

        then:
        message == expected

        where:
        seed | expected
        25   | 'Programming on stream.'
        4189 | 'Live streaming some Twitter bot coding.'
        13   | 'Programming on stream.'
        7921 | 'Live streaming some Twitter bot coding.'
    }

    def "should pick a novel message when no relevant messages have been tweeted recently"() {
        setup:
        Random random = new Random(seed)
        Composer composer = new Composer(random, null)
        String title = "Come join me on stream 👒"
        String msg1 = "Live streaming some Twitter bot coding."
        String msg2 = "#game# on stream."
        String msg3 = "🎶 Domo Arigato, Twitter Roboto!"
        List<String> sourceMsgs = [msg1, msg2, msg3]
        List<StoredTweet> recentTweets = [
                tweet('2018-05-17T01:00:00Z', 'Some old title'),
                tweet('2018-05-17T02:00:00Z', tweetText),
                tweet('2018-05-17T04:00:00Z', 'Whatever')
        ]
        String game = 'Programming'

        when:
        String message = composer.pickNovelMessage(title, sourceMsgs, recentTweets, game)

        then:
        message == expected

        where:
        seed | tweetText                   | expected
        25   | 'Maybe another tweet'       | 'Come join me on stream 👒'
        25   | 'Come join me on stream 👒' | '🎶 Domo Arigato, Twitter Roboto!'
        25   | 'Come join me on stream'    | '🎶 Domo Arigato, Twitter Roboto!'
    }
}
