package sondow.twitchpromo

import twitter4j.GeoLocation
import twitter4j.HashtagEntity
import twitter4j.MediaEntity
import twitter4j.Place
import twitter4j.RateLimitStatus
import twitter4j.Scopes
import twitter4j.Status
import twitter4j.SymbolEntity
import twitter4j.URLEntity
import twitter4j.User
import twitter4j.UserMentionEntity

/**
 * Simple implementation of Status, for unit testing only.
 */
class Tweet implements Status {

    long id
    String text
    Date createdAt
    int retweetCount
    int favoriteCount
    long inReplyToStatusId = -1
    boolean retweet
    User user
    MediaEntity[] mediaEntities
    URLEntity[] URLEntities

    @Override
    int getDisplayTextRangeStart() {
        return 0
    }

    @Override
    int getDisplayTextRangeEnd() {
        return 0
    }

    @Override
    String getSource() {
        return null
    }

    @Override
    boolean isTruncated() {
        return false
    }

    @Override
    long getInReplyToUserId() {
        return 0
    }

    @Override
    String getInReplyToScreenName() {
        return null
    }

    @Override
    GeoLocation getGeoLocation() {
        return null
    }

    @Override
    Place getPlace() {
        return null
    }

    @Override
    boolean isFavorited() {
        return false
    }

    @Override
    boolean isRetweeted() {
        return false
    }

    @Override
    Status getRetweetedStatus() {
        return null
    }

    @Override
    long[] getContributors() {
        return new long[0]
    }

    @Override
    boolean isRetweetedByMe() {
        return false
    }

    @Override
    long getCurrentUserRetweetId() {
        return 0
    }

    @Override
    boolean isPossiblySensitive() {
        return false
    }

    @Override
    String getLang() {
        return null
    }

    @Override
    Scopes getScopes() {
        return null
    }

    @Override
    String[] getWithheldInCountries() {
        return new String[0]
    }

    @Override
    long getQuotedStatusId() {
        return 0
    }

    @Override
    Status getQuotedStatus() {
        return null
    }

    @Override
    URLEntity getQuotedStatusPermalink() {
        return null
    }

    @Override
    int compareTo(Status o) {
        return 0
    }

    @Override
    UserMentionEntity[] getUserMentionEntities() {
        return new UserMentionEntity[0]
    }

    @Override
    HashtagEntity[] getHashtagEntities() {
        return new HashtagEntity[0]
    }

    @Override
    SymbolEntity[] getSymbolEntities() {
        return new SymbolEntity[0]
    }

    @Override
    RateLimitStatus getRateLimitStatus() {
        return null
    }

    @Override
    int getAccessLevel() {
        return 0
    }
}
