package sondow.twitchpromo

import twitter4j.RateLimitStatus
import twitter4j.ResponseList

class FakeResponseList<T> extends ArrayList<T> implements ResponseList<T> {

    FakeResponseList(List<T> items) {
        addAll(items);
    }

    @Override
    RateLimitStatus getRateLimitStatus() {
        return null
    }

    @Override
    int getAccessLevel() {
        return 0
    }
}
