package sondow.twitchpromo;

import java.time.ZonedDateTime

class Dates {

    static Date d(String isoDate) {
        return Date.from(ZonedDateTime.parse(isoDate).toInstant());
    }
}
