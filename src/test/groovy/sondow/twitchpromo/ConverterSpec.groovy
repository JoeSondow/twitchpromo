package sondow.twitchpromo

import com.amazonaws.services.dynamodbv2.model.AttributeValue
import sondow.common.FileClerk
import sondow.common.Time
import spock.lang.Specification
import static sondow.twitchpromo.Dates.d

class ConverterSpec extends Specification {

    def "should convert an item map to a StoredTweet"() {
        setup:
        Converter converter = new Converter()
        Date timeStamp = d('2018-04-05T10:45:27Z')
        Map<String, AttributeValue> item = [
                tweet_id      : converter.attr(8765L),
                core_text     : converter.attr('Come hang out!'),
                time_stamp    : converter.attr('2018-04-05T10:45:27Z'),
                twitter_handle: converter.attr('BarbaraTweets'),
                ttl           : converter.attr(8723857983L)
        ]
        if (inReplyMap != null) {
            item.put('in_reply_to_status_id', converter.attr(inReplyMap))
        }

        when:
        StoredTweet tweet = converter.convertItemMapToStoredTweet(item)

        then:
        tweet == new StoredTweet('8765', 'Come hang out!', timeStamp,
                'BarbaraTweets', inReplyObj)

        where:
        inReplyMap | inReplyObj
        null       | null
        6778L      | '6778'
    }

    def "should convert a StoredTweet into an item map"() {
        setup:
        Time time = Mock()
        Converter converter = new Converter()
        Date timeStamp = d('2018-04-05T10:45:27Z')
        StoredTweet tweet = new StoredTweet('8765', 'Come hang out!', timeStamp,
                'BarbaraTweets', inReplyObj)
        Map result = [
                tweet_id      : converter.attr(8765L),
                core_text     : converter.attr('Come hang out!'),
                time_stamp    : converter.attr('2018-04-05T10:45:27Z'),
                twitter_handle: converter.attr('BarbaraTweets'),
                ttl           : converter.attr(354345L)
        ]
        if (inReplyMap != null) {
            result.put 'in_reply_to_status_id', converter.attr(inReplyMap)
        }

        when:
        Map<String, AttributeValue> map = converter.convertStoredTweetToItemMap(tweet, time)

        then:
        1 * time.epochSecondInDays(15) >> 354345L
        map == result

        where:
        inReplyObj | inReplyMap
        null       | null
        '23423'    | 23423L
    }

    def 'should parse messages json into Ruleset'() {
        setup:
        Converter converter = new Converter()
        FileClerk fileClerk = new FileClerk()
        String json = fileClerk.readTextFile("test-messages-short.json")

        when:
        Ruleset ruleset = converter.convertMessagesJsonToRuleset(json)
        List<String> messages = ruleset.getMessages("Fortnite", "Playing some Fortnite")

        then:
        ruleset.toString() == "Ruleset{rules={default=Rule{messages=[Hi. I'm live streaming " +
                "#game#. Let's talk., Going live with #game#, come say hi!], }, Just " +
                "Chatting=Rule{titleFiltersToRules={tech=Rule{referencedGame='Science & " +
                "Technology', }, book club|reading|audiobook=Rule{messages=[Book club!, Story " +
                "time!], }}, }, Talk Shows & Podcasts=Rule{referencedGame='default', }, Science &" +
                " Technology=Rule{messages=[Live streaming my process for coding friendly twitter" +
                " bots., Programming. Talking. Drinking. Goofing off.], }}}"
        messages == ["Hi. I'm live streaming Fortnite. Let's talk.",
                     "Going live with Fortnite, come say hi!"]
    }
}
