package sondow.twitchpromo

import sondow.common.FileClerk
import spock.lang.Specification

class RulesetSpec extends Specification {

    def 'should get a simple message list from the document'() {
        setup:
        String json = new FileClerk().readTextFile("test-messages.json")
        Ruleset ruleset = new Converter().convertMessagesJsonToRuleset(json)

        when:
        List<String> techMsgs = ruleset.getMessages("Science & Technology", "Coding")
        List<String> boardGameMsgs = ruleset.getMessages("Board Games", "Zombicide")

        then:
        techMsgs == ['Live streaming my process for coding friendly twitter bots.',
                     'Programming. Talking. Drinking. Goofing off.']
        boardGameMsgs == [
                "Time for a cooperative board game. Wanna play?",
                "Streaming an analog game on a digital platform.",
                "Board game stream."
        ]
    }

    def 'should get a message list with game replacements from the document'() {
        setup:
        String json = new FileClerk().readTextFile("test-messages.json")
        Ruleset ruleset = new Converter().convertMessagesJsonToRuleset(json)

        when:
        List<String> fortniteMsgs = ruleset.getMessages("Fortnite", "Go Fortnite!")
        List<String> batmanMsgs = ruleset.getMessages("Batman", "I'm Batman")
        List<String> talkMsgs = ruleset.getMessages("Talk Shows & Podcasts", "Round table")

        then:
        fortniteMsgs == ["Hi. I'm live streaming Fortnite. Let's talk.",
                         "Going live with Fortnite, come say hi!"]
        batmanMsgs == ["Hi. I'm live streaming Batman. Let's talk.",
                       "Going live with Batman, come say hi!"]
        talkMsgs == ["Hi. I'm live streaming Talk Shows & Podcasts. Let's talk.",
                     "Going live with Talk Shows & Podcasts, come say hi!"]
    }

    def 'should get messages based on pipe-or stream title or main default game if no match'() {
        setup:
        String json = new FileClerk().readTextFile("test-messages.json")
        Ruleset ruleset = new Converter().convertMessagesJsonToRuleset(json)

        when:
        List<String> bookMsgs = ruleset.getMessages("Just Chatting", "Let's do Book Club!")
        List<String> readMsgs = ruleset.getMessages("Just Chatting", "I'm reading to you.")
        List<String> chatMsgs = ruleset.getMessages("Just Chatting", "This doesn't match.")

        then:
        bookMsgs == ["Time for a bedtime story. I'm gonna read to you on my stream.",
                     "Book club!",
                     "Story time!"]
        readMsgs == bookMsgs
        chatMsgs == ["Hi. I'm live streaming Just Chatting. Let's talk.",
                     "Going live with Just Chatting, come say hi!"]
    }

    def 'should get messages based on plus-and stream title or inner default title if no match'() {
        setup:
        String json = new FileClerk().readTextFile("test-messages.json")
        Ruleset ruleset = new Converter().convertMessagesJsonToRuleset(json)

        when:
        List<String> colorMsgs = ruleset.getMessages("Art", "I feel like coloring.")
        List<String> codeMsgs = ruleset.getMessages("Art", "Coding an art bot.")
        List<String> minisMsgs = ruleset.getMessages("Art", "Painting Zombicide miniatures.")
        List<String> paintMsgs = ruleset.getMessages("Art", "Bob Ross style painting.")
        List<String> figMsgs = ruleset.getMessages("Art", "Not quite nude figure drawing.")
        List<String> figPerMsgs = ruleset.getMessages("Art", "Figure person drawing.")

        then:
        colorMsgs == ["Coloring.",
                      "Stop by and help me pick colors for this coloring book.",
                      "Super chill coloring stream."]
        codeMsgs == ["Live streaming my process for coding friendly twitter bots.",
                     "Programming. Talking. Drinking. Goofing off."]
        minisMsgs == ["Painting some board game miniatures on my twitch stream.",
                      "Live streaming board game mini painting.",
                      "Come hang out with me on Twitch while I paint minis."]
        paintMsgs == ["Doin' some artwork.",
                      "Drawing stuff on twitch.",
                      "Doing art on stream.",
                      "Arting."]
        figMsgs == ["I'm sketching people on Twitch.",
                    "Live streaming some figure drawing."]
        figPerMsgs == ["Doin' some artwork.",
                       "Drawing stuff on twitch.",
                       "Doing art on stream.",
                       "Arting."]
    }

    def "should reject json where a game refers to another game which refers to a third game"() {
        setup:
        String json = new FileClerk().readTextFile("test-messages-invalid.json")
        Ruleset ruleset = new Converter().convertMessagesJsonToRuleset(json)

        when:
        ruleset.getMessages("Art", "I feel like coloring.")

        then:
        def e = thrown RuntimeException
        e.message == "Multiple levels of indirection found in messages file for game 'Art' with " +
                "stream title 'I feel like coloring.' referencing game 'Just Chatting' which " +
                "references 'Science & Technology'"
    }
}
