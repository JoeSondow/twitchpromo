package sondow.twitchpromo

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.AttributeAction
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult
import com.amazonaws.services.dynamodbv2.model.DescribeTimeToLiveRequest
import com.amazonaws.services.dynamodbv2.model.DescribeTimeToLiveResult
import com.amazonaws.services.dynamodbv2.model.PutItemRequest
import com.amazonaws.services.dynamodbv2.model.QueryRequest
import com.amazonaws.services.dynamodbv2.model.QueryResult
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException
import com.amazonaws.services.dynamodbv2.model.TableDescription
import com.amazonaws.services.dynamodbv2.model.TimeToLiveDescription
import com.amazonaws.services.dynamodbv2.model.TimeToLiveSpecification
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest
import com.amazonaws.services.dynamodbv2.model.UpdateTimeToLiveRequest
import java.time.ZonedDateTime
import sondow.common.Time
import spock.lang.Specification
import static sondow.twitchpromo.Dates.d

class DatabaseSpec extends Specification {

    def "table should be created if missing and tweet should get stored in the database"() {
        setup:
        Time time = Mock()
        AmazonDynamoDB db = Mock()
        AwsDynamoDatabase database = new AwsDynamoDatabase("BarbaraTweets", time, db)
        String timestampString = '2018-04-30T12:45:56Z'
        Date dateTime = d(timestampString)
        StoredTweet tweet = new StoredTweet('435', "Come hang out!", dateTime, "BarbaraTweets",
                inReplyToStatusId)

        String tblName = 'TWITCHPROMO_TWEETS_BARBARATWEETS'
        DescribeTableResult creatingTable = makeDescTableResult(tblName, 'CREATING')
        DescribeTableResult activeTable = makeDescTableResult(tblName, 'ACTIVE')
        int describeCount = 0
        DescribeTimeToLiveResult ttlDisabled = new DescribeTimeToLiveResult(timeToLiveDescription:
                new TimeToLiveDescription(timeToLiveStatus: "DISABLED"))

        Converter converter = new Converter()
        Map<String, AttributeValue> item = [
                tweet_id      : converter.attr(435L),
                core_text     : converter.attr("Come hang out!"),
                time_stamp    : converter.attr(timestampString),
                twitter_handle: converter.attr('BarbaraTweets'),
                ttl           : converter.attr(1530505289L)]
        if (inReplyToStatusId != null) {
            item.put('in_reply_to_status_id', converter.attr(inReply))
        }
        PutItemRequest putItemRequest = new PutItemRequest(tblName, item)

        when:
        database.storeTweet(tweet)

        then:
        10 * db.describeTable(tblName) >> { name ->
            describeCount++
            if (describeCount <= 1) {
                throw new ResourceNotFoundException()
            } else if (describeCount <= 3) {
                return creatingTable
            }
            return activeTable
        }
        1 * db.createTable(_ as CreateTableRequest)
        8 * time.waitASec()
        1 * db.describeTimeToLive(new DescribeTimeToLiveRequest(tableName: tblName)) >> ttlDisabled
        1 * db.updateTimeToLive(new UpdateTimeToLiveRequest(tableName: tblName,
                timeToLiveSpecification: new TimeToLiveSpecification(enabled: true,
                        attributeName: "ttl")))
        1 * time.epochSecondInDays(15) >> 1530505289L
        1 * db.putItem(putItemRequest)
        0 * _._

        where:
        inReply | inReplyToStatusId
        null    | null
        35345L  | '35345'
    }

    private static DescribeTableResult makeDescTableResult(String tableName, String status) {
        DescribeTableResult creatingTable = new DescribeTableResult()
        TableDescription descCreating = new TableDescription()
        descCreating.withTableName(tableName).withTableStatus(status)
        creatingTable.withTable(descCreating)
        creatingTable
    }

    def 'should retrieve recent tweets from database'() {
        setup:
        Time time = Mock()
        AmazonDynamoDB db = Mock()
        ZonedDateTime now = ZonedDateTime.parse('2018-05-05T14:02:45Z')
        String fiveDaysAgo = '2018-04-30T14:02:45Z'
        AwsDynamoDatabase database = new AwsDynamoDatabase('HotTakes', time, db)
        String tblName = 'TWITCHPROMO_TWEETS_HOTTAKES'
        DescribeTableResult tableResult = makeDescTableResult(tblName, 'ACTIVE')
        DescribeTimeToLiveRequest ttlRequest = new DescribeTimeToLiveRequest().
                withTableName(tblName)
        DescribeTimeToLiveResult ttlResult = new DescribeTimeToLiveResult(timeToLiveDescription:
                new TimeToLiveDescription(timeToLiveStatus: "ENABLED"))
        String queryString = 'twitter_handle = :v_twitter_handle and time_stamp > :v_time_stamp'
        Converter conv = new Converter()
        Map<String, AttributeValue> values = [
                ':v_twitter_handle': conv.attr('HotTakes'),
                ':v_time_stamp'    : conv.attr(fiveDaysAgo)
        ]

        QueryRequest request = new QueryRequest(tblName).
                withKeyConditionExpression(queryString).
                withExpressionAttributeValues(values).
                withIndexName('TWITCHPROMO_TWEETS_HOTTAKES_TIMESTAMP_INDEX')
        List<Map<String, AttributeValue>> items = [
                [twitter_handle: conv.attr('HotTakes'),
                 time_stamp    : conv.attr('2018-05-01T14:02:45Z'),
                 core_text     : conv.attr('I love you!'),
                 ttl           : conv.attr(2222222L),
                 tweet_id      : conv.attr(968673L)],
                [twitter_handle       : conv.attr('HotTakes'),
                 time_stamp           : conv.attr('2018-05-02T14:02:45Z'),
                 core_text            : conv.attr('Hello there!'),
                 ttl                  : conv.attr(3333333L),
                 tweet_id             : conv.attr(765432L),
                 in_reply_to_status_id: conv.attr(968673L)
                ]
        ]
        QueryResult queryResult = new QueryResult().withItems(items)

        when:
        List<StoredTweet> tweets = database.retrieveRecentTweets(5, 'HotTakes')

        then:
        tweets == [
                new StoredTweet('968673', 'I love you!',
                        d('2018-05-01T14:02:45Z'), 'HotTakes',
                        null),
                new StoredTweet('765432', 'Hello there!',
                        d('2018-05-02T14:02:45Z'), 'HotTakes',
                        '968673')]
        1 * db.describeTable(tblName) >> tableResult
        1 * db.describeTimeToLive(ttlRequest) >> ttlResult
        1 * time.nowZonedDateTime() >> now
        1 * db.query(request) >> queryResult
        0 * _._
    }

    def 'should mark tweet deleted in database'() {
        setup:
        AmazonDynamoDB db = Mock()
        AwsDynamoDatabase database = new AwsDynamoDatabase('Stuff', Mock(Time), db)
        AttributeValue boolTrue = new AttributeValue().withBOOL(true)
        AttributeValueUpdate valueUpdate = new AttributeValueUpdate(boolTrue, AttributeAction.PUT)
        UpdateItemRequest request = new UpdateItemRequest().
                withTableName('TWITCHPROMO_TWEETS_STUFF').
                withKey([tweet_id: Converter.attr(98L)]).
                withAttributeUpdates([deleted: valueUpdate])

        when:
        database.markTweetDeleted(98L)

        then:
        1 * db.updateItem(request)
        0 * _._
    }
}
