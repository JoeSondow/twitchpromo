package sondow.twitchpromo

import java.time.ZonedDateTime
import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import sondow.common.Environment
import sondow.common.Keymaster
import sondow.common.Time
import spock.lang.Specification
import twitter4j.AltTextTwitterImpl
import twitter4j.FakeMaker
import twitter4j.Status
import twitter4j.StatusUpdate
import twitter4j.UploadedMedia
import static sondow.twitchpromo.Dates.d

class PromoterSpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    ImageUrls imageUrls = new ImageUrls(
            "https://static-cdn.jtvnw.net/previews-ttv/live_user_scooby-1280x720.jpg",
            "https://static-cdn.jtvnw.net/ttv-static/404_preview-1280x720.jpg")

    private void initEnvironment() {
        String filler = Environment.SPACE_FILLER
        envVars.set("twitter_account", "cartoons")
        envVars.set("cartoons_twitter_credentials",
                "${filler}cartoons,bartsimpson,snaggletooth,fredflintstone,bugsbunny${filler}")
        envVars.set("twitch_access_token", "${filler}fred")
        envVars.set("twitch_client_id", "${filler}shaggy")
        envVars.set("twitch_target_username", "${filler}${filler}${filler}scooby${filler}${filler}")
        envVars.set("CRED_AWS_ACCESS_KEY", "${filler}${filler}velma${filler}${filler}${filler}")
        envVars.set("CRED_AWS_SECRET_KEY", "${filler}${filler}daphne${filler}${filler}${filler}")
    }

    def "promoter should not tweet if stream is offline"() {
        setup:
        initEnvironment()
        Random random = new Random(4)
        ImageDownloader imageDownloader = Mock()
        TextFileProvider textFileDownloader = Mock()
        Database database = Mock()
        AltTextTwitterImpl twitter = Mock()
        StreamFetcher streamFetcher = Mock()
        Time time = Mock()
        Decider decider = Mock()
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        Promoter promoter = new Promoter(
                environment, 'unittest', random, imageDownloader, textFileDownloader,
                database, twitter, streamFetcher, time, decider, null)
        ImageDownloadResult imageDownloadResult = new ImageDownloadResult(null, false)

        when:
        Status tweet = promoter.promoteIfLiveAndDueForPromotion()

        then:
        1 * imageDownloader.checkAndDownload(imageUrls, '/tmp') >> imageDownloadResult
        0 * _._
        tweet == null
    }

    def "promoter should not tweet if decider decides not to tweet"() {
        setup:
        initEnvironment()
        Random random = new Random(4)
        ImageDownloader imageDownloader = Mock()
        TextFileProvider textFileProvider = Mock()
        Database database = Mock()
        AltTextTwitterImpl twitter = Mock()
        StreamFetcher streamFetcher = Mock()
        Time time = Mock()
        Decider decider = Mock()
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        Promoter promoter = new Promoter(environment, 'unittest', random, imageDownloader,
                textFileProvider, database, twitter, streamFetcher, time, decider, null)
        ImageDownloadResult imageDownloadResult = new ImageDownloadResult(null, true)
        Stream stream = new Stream("Hi", "Yo", 0, null)
        ZonedDateTime fakeNow = TimeUtil.zonedDateTimeWithSeconds('15:24:40')
        Decision decision = new Decision(itIsTimeToTweet: false)

        when:
        Status tweet = promoter.promoteIfLiveAndDueForPromotion()

        then:
        1 * imageDownloader.checkAndDownload(_, '/tmp') >> imageDownloadResult
        1 * streamFetcher.fetchStreamInfo('scooby', 'shaggy', 'fred') >> stream
        1 * time.nowZonedDateTime() >> fakeNow
        1 * database.retrieveRecentTweets(15, 'cartoons') >> []
        1 * decider.decideWhatToDo(fakeNow, _, stream, _) >> decision
        0 * twitter._ // Nothing gets tweeted
        0 * _._
        tweet == null
    }

    def "promoter should tweet and reply and destroy as decider chooses"() {
        setup:
        initEnvironment()
        Random random = new Random(4)
        ImageDownloader imageDownloader = Mock()
        TextFileProvider textFileDownloader = Mock()
        Database database = Mock()
        AltTextTwitterImpl twitter = Mock()
        StreamFetcher streamFetcher = Mock()
        Time time = Mock()
        Decider decider = Mock()
        Composer composer = Mock()
        File imageFile = Mock()
        UploadedMedia uploadedMedia = new FakeMaker().makeFakeUploadedMedia(56789L, 1234L);
        Keymaster keymaster = Mock()
        Environment environment = new Environment(keymaster)
        Promoter promoter = new Promoter(
                environment, 'unittest', random, imageDownloader, textFileDownloader,
                database, twitter, streamFetcher, time, decider, composer)
        ImageDownloadResult imageDownloadResult = new ImageDownloadResult(imageFile, true)
        Stream stream = new Stream("Hey", "Yeah", 0, null)
        ZonedDateTime fakeNow = TimeUtil.zonedDateTimeWithSeconds('15:24:40')
        Decision decision = new Decision(itIsTimeToTweet: true, inReplyToStatusId: inReplyDecision)
        deleteIds.each { decision.deleteTweet(it) }
        String fullText = 'Hi there #love'
        StatusUpdate statusUpdate = new StatusUpdate(fullText)
        statusUpdate.setInReplyToStatusId(inReplyOutcome)
        statusUpdate.setMediaIds([56789L] as long[])
        Date timestamp = d('2018-04-05T20:48:23Z')
        Date createdAt = Date.from(timestamp.toInstant())
        Status createdTweet = new Tweet(id: 89L, text: 'Hi there #love', createdAt: createdAt,
                user: new TestUser(id: 456L), inReplyToStatusId: inReplyOutcome)
        StoredTweet storedTweet = new StoredTweet('89', 'Hi there', timestamp,
                'cartoons', inReplyDb)

        when:
        Status tweet = promoter.promoteIfLiveAndDueForPromotion()

        then:
        1 * imageDownloader.checkAndDownload(_, '/tmp') >> imageDownloadResult
        1 * streamFetcher.fetchStreamInfo('scooby', 'shaggy', 'fred') >> stream
        1 * time.nowZonedDateTime() >> fakeNow
        1 * database.retrieveRecentTweets(15, 'cartoons') >> []
        1 * decider.decideWhatToDo(fakeNow, _, stream, _) >> decision
        1 * composer.compose(_, _, stream) >>
                new Composition("Hi there #love", "Hi there", "Pic of greeting")
        1 * twitter.uploadMedia(imageFile) >> uploadedMedia
        1 * twitter.createMediaMetadata(56789L, 'Pic of greeting')
        1 * twitter.updateStatus(statusUpdate) >> createdTweet
        1 * database.storeTweet(storedTweet)
        1 * imageFile.getName()
        1 * imageFile.delete()
        delete5 * twitter.destroyStatus(5L)
        delete6 * twitter.destroyStatus(6L)
        delete5 * database.markTweetDeleted(5L)
        delete6 * database.markTweetDeleted(6L)
        0 * _._
        tweet == createdTweet

        where:
        inReplyDecision | inReplyOutcome | inReplyDb | deleteIds | delete5 | delete6
        null            | -1L            | null      | [5L]      | 1       | 0
        null            | -1L            | null      | []        | 0       | 0
        '887'           | 887L           | '887'     | [5L, 6L]  | 1       | 1
        '887'           | 887L           | '887'     | []        | 0       | 0
    }
}
