package sondow.cv;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest;
import sondow.common.FileClerk;

/**
 * Inspired by solution from http://mindmeat.blogspot.com/2008/07/java-image-comparison.html
 */
public class ImageComparator {

    private Map<File, ColorHistograms> cache = new HashMap<>();

    // Preload the cache with the files and histograms of the reference boring images.
    {
        FileClerk fileClerk = new FileClerk();
        Map<String, ColorHistograms> fileNamesToHistograms =
                new BoringImages().fileNamesToHistograms;
        Set<String> fileNames = fileNamesToHistograms.keySet();
        for (String fileName : fileNames) {
            File file = fileClerk.getFile(fileName);
            cache.put(file, fileNamesToHistograms.get(fileName));
        }
    }

    public boolean areSimilar(ColorHistograms colorHist1, ColorHistograms colorHist2) {
        double red = compareHistograms(colorHist1.getRed(), colorHist2.getRed());
        double green = compareHistograms(colorHist1.getGreen(), colorHist2.getGreen());
        double blue = compareHistograms(colorHist1.getBlue(), colorHist2.getBlue());
        double limit = 0.0001d; // Based on tests of real examples
        return (red > limit) && (green > limit) && (blue > limit);
    }

    public boolean areSimilar(File file1, File file2) {
        ColorHistograms colorHist1 = getHistograms(file1);
        ColorHistograms colorHist2 = getHistograms(file2);
        return areSimilar(colorHist1, colorHist2);
    }

    public ColorHistograms getHistograms(File file) {
        ColorHistograms colorHistograms = cache.get(file);
        if (colorHistograms == null) {
            colorHistograms = generateHistograms(loadJpg(file));
            cache.put(file, colorHistograms);
        }
        return colorHistograms;
    }

    private BufferedImage loadJpg(File file) {
        BufferedImage bi;
        try {
            bi = ImageIO.read(file);
        } catch (java.io.IOException io) {
            throw new RuntimeException("IOException");
        }
        return bi;
    }

    private double compareHistograms(int[] histogram1, int[] histogram2) {
        double[] hist1 = Arrays.stream(histogram1).asDoubleStream().toArray();
        double[] hist2 = Arrays.stream(histogram2).asDoubleStream().toArray();
        return new KolmogorovSmirnovTest().kolmogorovSmirnovTest(hist1, hist2);
    }

    private ColorHistograms generateHistograms(BufferedImage image) {
        Multiset<Integer> redMultiset = HashMultiset.create();
        Multiset<Integer> greenMultiset = HashMultiset.create();
        Multiset<Integer> blueMultiset = HashMultiset.create();

        for (int y = 0; y < image.getHeight(); y++) {
            for (int x = 0; x < image.getWidth(); x++) {
                Color color = new Color(image.getRGB(x, y));

                redMultiset.add(color.getRed());
                greenMultiset.add(color.getGreen());
                blueMultiset.add(color.getBlue());
            }
        }
        int[] redHistogram = new int[256];
        int[] greenHistogram = new int[256];
        int[] blueHistogram = new int[256];
        for (int i = 0; i < 256; i++) {
            // At least 1 instance of each value, because of a curious requirement of Chi-Square.
            // This is technically inaccurate but only a negligible amount.
            redHistogram[i] = redMultiset.count(i);
            greenHistogram[i] = greenMultiset.count(i);
            blueHistogram[i] = blueMultiset.count(i);
        }
        return new ColorHistograms(redHistogram, greenHistogram, blueHistogram);
    }
}
