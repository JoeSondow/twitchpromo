package sondow.cv;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ArtCritic {

    private ImageComparator imageComparator = new ImageComparator();

    public boolean isImageInteresting(File imageFile) {

        //        Stopwatch stopwatch = Stopwatch.createStarted();

        boolean foundBoringMatch = false;

        // If the image is similar to a boring image then the image is also boring.
        // TODO make the boring images configurable, maybe fetchable from URLs or a database
        BoringImages boringImages = new BoringImages();
        Map<String, ColorHistograms> fileNamesToHistograms = boringImages.fileNamesToHistograms;
        List<String> fileNames = new ArrayList<>(fileNamesToHistograms.keySet());
        for (int i = 0; i < fileNames.size() && !foundBoringMatch; i++) {
            String fileName = fileNames.get(i);
            ColorHistograms histogramsOfBoringImage = fileNamesToHistograms.get(fileName);
            ColorHistograms histograms = imageComparator.getHistograms(imageFile);
            if (imageComparator.areSimilar(histograms, histogramsOfBoringImage)) {
                foundBoringMatch = true;
            }
        }
        //        stopwatch.stop();
        //        log.info("isImageInteresting took " + stopwatch.toString());
        return !foundBoringMatch;
    }
}
