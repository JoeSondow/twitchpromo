package sondow.common;

import java.time.ZonedDateTime;
import java.util.Date;

public class Time {

    public static Date parse(String string) {
        return Date.from(ZonedDateTime.parse(string).toInstant());
    }

    public ZonedDateTime nowZonedDateTime() {
        return ZonedDateTime.now();
    }

    public long epochSecondInDays(int days) {
        return nowZonedDateTime().plusDays(days).toInstant().getEpochSecond();
    }

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public void waitASec() {
        sleep(1000L);
    }
}
