package sondow.twitchpromo;

public class ImageUrls {

    private String previewUrl;
    private String notFoundUrl;

    public ImageUrls(String previewUrl, String notFoundUrl) {
        this.previewUrl = previewUrl;
        this.notFoundUrl = notFoundUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getNotFoundUrl() {
        return notFoundUrl;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof ImageUrls)) { return false; }

        ImageUrls imageUrls = (ImageUrls) o;

        if (getPreviewUrl() != null ? !getPreviewUrl().equals(imageUrls.getPreviewUrl()) :
                imageUrls.getPreviewUrl() != null) { return false; }
        return getNotFoundUrl() != null ? getNotFoundUrl().equals(imageUrls.getNotFoundUrl()) :
                imageUrls.getNotFoundUrl() == null;
    }

    @Override public int hashCode() {
        int result = getPreviewUrl() != null ? getPreviewUrl().hashCode() : 0;
        result = 31 * result + (getNotFoundUrl() != null ? getNotFoundUrl().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ImageUrls{" +
                "previewUrl='" + previewUrl + '\'' +
                ", notFoundUrl='" + notFoundUrl + '\'' +
                '}';
    }
}
