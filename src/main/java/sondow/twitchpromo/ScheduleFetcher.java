package sondow.twitchpromo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.mirrobot.twitch.api.Twitch;
import ru.mirrobot.twitch.api.handlers.ScheduleResponseHandler;
import ru.mirrobot.twitch.api.models.Game;
import ru.mirrobot.twitch.api.models.Schedule;
import ru.mirrobot.twitch.api.models.Segment;
import ru.mirrobot.twitch.api.models.Vacation;
import sondow.common.Time;

public class ScheduleFetcher {

    public static void mainLib(String[] args) {
        //        Environment environment = new Environment();
        //        String authUsername = environment.get("twitch_auth_username");
        //        String clientId = environment.get("twitch_client_id");
        //        String accessToken = environment.get("twitch_access_token");
        //        String targetUsername = environment.get("twitch_target_username");
        Config config = new ConfigFactory("ScheduleFetcher.main").configure();
        String clientId = config.getClientId();
        String accessToken = config.getAccessToken();
        String targetUsername = config.getTargetUsername();
        String scheduleBroadcasterId = config.getScheduleBroadcasterId();
        Twitch twitch = new Twitch();
        twitch.setClientId(clientId);
        twitch.auth().setAccessToken(accessToken);
        twitch.schedule().get(Long.parseLong(scheduleBroadcasterId), new ScheduleResponseHandler() {

            @Override
            public void onSuccess(final Schedule schedule) {
                Segment segment = schedule.getData().get(0);
                String title = segment.getTitle();
                Game category = segment.getCategory();
                Date startTime = segment.getStartTime();

                System.out.println("Schedule: " + title + "  - " + category + " - " + startTime);
            }

            @Override
            public void onFailure(int statusCode, String statusMessage, String errorMessage) {
                System.out.println(statusCode + " " + statusMessage + " " + errorMessage);
            }

            @Override public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        });

        //        Stream stream = new StreamFetcher().fetchStreamInfo(authUsername, clientId);
        //        System.out.println(stream.getTitle());
    }

    public static void main(String[] args) {
        Config config = new ConfigFactory("ScheduleFetcher.main").configure();
        String targetUsername = config.getTargetUsername();
        String scheduleBroadcasterId = config.getScheduleBroadcasterId();
        String clientId = config.getClientId();
        String accessToken = config.getAccessToken();
        Schedule schedule = new ScheduleFetcher().fetchScheduleInfo(scheduleBroadcasterId, clientId,
                accessToken);
        List<Segment> scheduledSegments = schedule.getData();
        System.out.println(scheduledSegments);
    }

    public Schedule fetchScheduleInfo(String scheduleBroadcasterId, String clientId,
            String accessToken) {
        OkHttpClient client = new OkHttpClient();
        String gameName, startedAt, title;
        String schedUrl =
                "https://api.twitch.tv/helix/schedule?first=1&broadcaster_id=" +
                        scheduleBroadcasterId;
        Request schedReq = buildRequest(schedUrl, clientId, accessToken);
        Schedule schedule;
        try {
            Response scheduleResponse = client.newCall(schedReq).execute();
            String scheduleResponseText = Objects.requireNonNull(scheduleResponse.body()).string();
            schedule = parseSchedule(scheduleResponseText);
            System.out.println(scheduleResponseText);
            //            String errorMsg = "No stream is li for ";
            //            JSONObject scheduleJsonObj = getDataJsonObject(errorMsg,
            //            scheduleResponseText);
            //
            //            title = scheduleJsonObj.getString("title");
            //            viewerCount = scheduleJsonObj.getInt("viewer_count");
            //            startedAt = scheduleJsonObj.getString("started_at");
            //            gameName = scheduleJsonObj.getString("game_name");
            System.out.println(schedule);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        //        Date createdAt = Date.from(ZonedDateTime.parse(startedAt).toInstant());
        return schedule;
        //        return new Schedule(gameName, title, viewerCount, createdAt);
    }

    private Request buildRequest(String url, String clientId, String accessToken) {
        return new Request.Builder().url(url).header("Client-ID", clientId)
                .header("Authorization", String.format("Bearer %s", accessToken)).build();
    }

    private Schedule parseSchedule(String responseText) {
        // {"data":{"segments":[{"id":"VERYlongSTRINGtruncatedHEREeyJzZWdtZW50pc29XZWVrIjo0M30=",
        // "start_time":"2022-10-24T01:00:00Z","end_time":"2022-10-24T05:00:00Z",
        // "title":"Test scheduled stream","canceled_until":null,
        // "category":{"id":"1469308723","name":"Software and Game Development"},
        // "is_recurring":true}],
        // "broadcaster_id":"51696548","broadcaster_name":"JoeSondow",
        // "broadcaster_login":"joesondow",
        // "vacation":{"start_time":"2022-10-23T07:00:00Z","end_time":"2022-10-25T06:59:59Z"}},
        // "pagination":{"cursor":"VERYlongSTRINGtruncatedHERE5sZEUxcGJuVjBaWE1pT2pCOSJ9fQ"}}
        System.out.println(responseText);
        JSONObject jsonObject = new JSONObject(responseText);
        if (jsonObject.has("error")) {
            System.out.println(responseText);
            throw new RuntimeException(responseText);
        }
        JSONObject jsonObjectData = jsonObject.getJSONObject("data");
        JSONArray jsonArraySegments = jsonObjectData.getJSONArray("segments");
        int segmentCount = jsonArraySegments.length();
        List<Segment> segments = new ArrayList<>();
        for (int i = 0; i < segmentCount; i++) {
            JSONObject jsonObjSegment = jsonArraySegments.getJSONObject(i);
            String segmentId = jsonObjSegment.getString("id");
            String startTimeStr = jsonObjSegment.getString("start_time");
            Date startTime = Time.parse(startTimeStr);
            String endTimeStr = jsonObjSegment.getString("end_time");
            Date endTime = Time.parse(endTimeStr);
            String title = jsonObjSegment.getString("title");
            boolean isRecurring = jsonObjSegment.getBoolean("is_recurring");
            Date canceledUntil = null;
            if (!jsonObjSegment.isNull("canceled_until")) {
                String canceledUntilStr = jsonObjSegment.getString("canceled_until");
                canceledUntil = Time.parse(canceledUntilStr);
            }
            JSONObject categoryObj = jsonObjSegment.getJSONObject("category");
            long categoryId = categoryObj.getLong("id");
            String categoryName = categoryObj.getString("name");
            Game category = new Game();
            category.setId(categoryId);
            category.setName(categoryName);

            Segment segment = new Segment();
            segment.setId(segmentId);
            segment.setStartTime(startTime);
            segment.setEndTime(endTime);
            segment.setTitle(title);
            segment.setRecurring(isRecurring);
            segment.setCanceledUntil(canceledUntil);
            segment.setCategory(category);
            segments.add(segment);
        }
        long broadcasterId = jsonObjectData.getLong("broadcaster_id");
        String broadcasterName = jsonObjectData.getString("broadcaster_name");
        String broadcasterLogin = jsonObjectData.getString("broadcaster_login");

        Vacation vacation = null;
        if (!jsonObjectData.isNull("vacation")) {
            JSONObject vacationObj = jsonObjectData.getJSONObject("vacation");
            String startTimeStr = vacationObj.getString("start_time");
            String endTimeStr = vacationObj.getString("end_time");
            Date vacationStartTime = Time.parse(startTimeStr);
            Date vacationEndTime = Time.parse(endTimeStr);
            vacation = new Vacation();
            vacation.setStartTime(vacationStartTime);
            vacation.setEndTime(vacationEndTime);
        }
        Schedule schedule = new Schedule();
        schedule.setData(segments);
        schedule.setBroadcasterId(broadcasterId);
        schedule.setBroadcasterName(broadcasterName);
        schedule.setBroadcasterLogin(broadcasterLogin);
        schedule.setVacation(vacation);
        return schedule;
    }
}
