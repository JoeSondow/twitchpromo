package sondow.twitchpromo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * This class serves the dual purposes of checking whether the specified Twitch user is currently
 * online, and saving that user's latest preview image file if they are online.
 *
 * @author @JoeSondow
 */
public class ImageDownloader {

    /**
     * Example output:
     * <p>
     * https://static-cdn.jtvnw.net/previews-ttv/live_user_joesondow-1900x1200.jpg
     * https://static-cdn.jtvnw.net/ttv-static/404_preview-1900x1200.jpg
     *
     * @param config the object containing the username and stream resolution
     * @return the URL of the stream preview image to download
     */
    public static ImageUrls buildImageUrls(Config config) {

        String lowercaseName = config.getTargetUsername()
                .toLowerCase(); // Username must be lowercase.
        int width = config.getWidth();
        int height = config.getHeight();
        String res = width + "x" + height;
        String previewUrlPrefix = "https://static-cdn.jtvnw.net/previews-ttv/live_user_";
        String previewUrl = previewUrlPrefix + lowercaseName + "-" + res + ".jpg";
        String notFoundUrl = "https://static-cdn.jtvnw.net/ttv-static/404_preview-" + res + ".jpg";
        return new ImageUrls(previewUrl, notFoundUrl);
    }

    public ImageDownloadResult checkAndDownload(ImageUrls imageUrls, String saveDir) throws
            IOException {

        String previewUrl = imageUrls.getPreviewUrl();
        URL url = new URL(previewUrl);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setInstanceFollowRedirects(false);
        int responseCode = httpConn.getResponseCode();

        boolean isOnline;
        File file;

        String jpgUrl404 = imageUrls.getNotFoundUrl();
        if (HttpURLConnection.HTTP_MOVED_TEMP == responseCode) {
            // Redirect almost certainly means channel is offline.
            String location = httpConn.getHeaderField("Location");
            if (!jpgUrl404.equals(location)) {
                String msg = "Surprise redirect location: " + location + " instead of " + jpgUrl404;
                throw new RuntimeException(msg);
            }

            isOnline = false;
            file = null;
        } else if (HttpURLConnection.HTTP_OK == responseCode) {

            ReadableByteChannel rbc = Channels.newChannel(httpConn.getInputStream());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH-mm-ss-SSS");
            String now = ZonedDateTime.now().format(formatter);
            int indexOfUrlFileName = previewUrl.lastIndexOf("/") + 1;
            String filenameFromUrl = previewUrl.substring(indexOfUrlFileName);
            String filenameToSaveLocally = now + "-" + filenameFromUrl;

            String saveFilePath = saveDir + File.separator + filenameToSaveLocally;
            File downloadFolder = new File(saveDir);

            // downloadFolder.deleteOnExit();
            //noinspection ResultOfMethodCallIgnored
            downloadFolder.mkdirs();

            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
            outputStream.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);

            outputStream.close();
            rbc.close();

            isOnline = true;
            file = new File(saveFilePath);
        } else {
            String msg = "Surprise response code " + responseCode + " for url " + previewUrl;
            throw new RuntimeException(msg);
        }

        ImageDownloadResult imageDownloadResult = new ImageDownloadResult(file, isOnline);
        System.out.println("Image download result " + imageDownloadResult);
        return imageDownloadResult;
    }
}
