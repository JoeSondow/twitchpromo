package sondow.twitchpromo;

/**
 * Container for core message as well as full message that includes extra stuff to tweet.
 */
public class Composition {

    private String fullMessage;
    private String coreMessage;
    private String imageAltText;

    public Composition(String fullMessage, String coreMessage, String imageAltText) {
        this.fullMessage = fullMessage;
        this.coreMessage = coreMessage;
        this.imageAltText = imageAltText;
    }

    public String getFullMessage() {
        return fullMessage;
    }

    String getCoreMessage() {
        return coreMessage;
    }

    public String getImageAltText() {
        return imageAltText;
    }
}
