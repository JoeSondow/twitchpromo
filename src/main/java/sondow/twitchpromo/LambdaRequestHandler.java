package sondow.twitchpromo;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * The function that AWS Lambda will invoke.
 *
 * @author @JoeSondow
 */
public class LambdaRequestHandler implements RequestHandler<Object, Object> {

    public static void main(String[] args) {
        new LambdaRequestHandler().handleRequest(null, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.amazonaws.services.lambda.runtime.RequestHandler#handleRequest(java. lang.Object,
     * com.amazonaws.services.lambda.runtime.Context)
     */
    @Override
    public Object handleRequest(Object input, Context context) {

        String functionName = null;
        if (context != null) {
            functionName = context.getFunctionName();
        }
        return new Promoter(functionName).promoteIfLiveAndDueForPromotion();
    }
}
