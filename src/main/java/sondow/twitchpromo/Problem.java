package sondow.twitchpromo;

/**
 * Wrapper for fields that need to get passed to the Retriable#handleException function.
 */
public class Problem {

    private Exception exception;

    private int triesDoneSoFar;

    public Problem(Exception exception, int triesDoneSoFar) {
        this.exception = exception;
        this.triesDoneSoFar = triesDoneSoFar;
    }
}
