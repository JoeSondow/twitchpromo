package sondow.twitchpromo;

import com.google.common.collect.ImmutableList;
import com.sybit.airtable.Query;
import com.sybit.airtable.Sort;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class AirtableQuery implements Query {

    private String filterByFormula;
    private List<String> fields;

    public AirtableQuery(String filterByFormula, List<String> fields) {
        this.filterByFormula = filterByFormula;
        this.fields = ImmutableList.copyOf(fields);
    }

    @Override public String[] getFields() {
        return fields.toArray(new String[0]);
    }

    @Override public Integer getPageSize() {
        return null;
    }

    @Override public Integer getMaxRecords() {
        return null;
    }

    @Override public String getView() {
        return null;
    }

    @Override public List<Sort> getSort() {
        return null;
    }

    @Override public String filterByFormula() {
        return this.filterByFormula;
    }

    @Override public String getOffset() {
        return null;
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (!(o instanceof AirtableQuery)) { return false; }

        AirtableQuery that = (AirtableQuery) o;

        if (!Objects.equals(filterByFormula, that.filterByFormula)) { return false; }
        return getFields() != null ? Arrays.equals(getFields(), that.getFields()) :
                that.getFields() == null;
    }

    @Override public int hashCode() {
        int result = filterByFormula != null ? filterByFormula.hashCode() : 0;
        result = 31 * result + (getFields() != null ? getFields().hashCode() : 0);
        return result;
    }
}
