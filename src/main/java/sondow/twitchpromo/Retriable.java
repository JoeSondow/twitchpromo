package sondow.twitchpromo;

import com.google.common.math.IntMath;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import sondow.common.Time;

/**
 * Mechanism for retrying actions multiple times, with customized error handling behavior.
 *
 * @param <R> the return type of the work to be done
 */
public class Retriable<R> {

    /**
     * The base of the exponent to be used for exponential backoff.
     * <p>
     * A value of 1 disables exponential backoff. A value of 2 causes retry delays to grow
     * exponentially based on powers of 2.
     */
    private static final Integer EXPONENTIAL_BACKOFF_BASE_MULTIPLIER = 2;

    /**
     * The number of times to try to perform the work before giving up and re-throwing the
     * exceptions from the attempts.
     */
    private static final Integer MAX_TRIES = 9;

    /**
     * The maximum time in milliseconds to wait between attempts. Defaults to four minutes.
     */
    private static final Integer MAX_DELAY_MILLIS = 2 * 60 * 1000;
    /**
     * The time in milliseconds to wait after the first failed attempt. This is also the base factor
     * for subsequent retry delay lengths when using exponential backoff.
     */
    private static final Integer FIRST_DELAY_MILLIS = 250;
    /**
     * (Optional) Strategy for handling exceptions thrown during failed attempts to complete the
     * work.
     */
    private final Function<Problem, Object> handleException;
    private final Time time;
    /**
     * The work to try to execute without an exception getting thrown.
     * <p>
     * T work(int iterationCounter)
     */
    private final Function<Integer, R> work;
    /**
     * The name of the process that will be attempted, to be used in error messages.
     */
    private final String name;

    public Retriable(Function<Integer, R> work, String name,
            Function<Problem, Object> handleException, Time time) {
        this.work = work;
        this.name = name;
        this.handleException = handleException;
        this.time = time;
    }

    /**
     * Repeatedly attempts to perform some work without throwing an exception, up to a chosen number
     * of times
     *
     * @return the object returned by the work closure, with unknown type
     */
    public R performWithRetries() {
        List<Exception> exceptions = new ArrayList<>();
        for (int i = 0; i < MAX_TRIES; i++) {
            if (i > 0) {
                int multiplier = Math.max(1, EXPONENTIAL_BACKOFF_BASE_MULTIPLIER);
                int calculatedDelayMillis = IntMath.pow(multiplier, i - 1) * FIRST_DELAY_MILLIS;
                int delayMillis = Math.min(calculatedDelayMillis, MAX_DELAY_MILLIS);
                delay(delayMillis);
            }
            try {
                return work.apply(i);
            } catch (Exception e) {
                exceptions.add(e);
                if (handleException != null) {
                    handleException.apply(new Problem(e, i + 1));
                }
            }
        }

        String msg = "Failed retriable process '" + name + "', max retries: " + MAX_TRIES;
        throw new CollectedExceptions(msg, exceptions);
    }

    /**
     * Does some sleeping. This method can be overridden by subclasses to substitute behavior for
     * cases such as unit testing.
     *
     * @param milliseconds the number of milliseconds to sleep
     */
    public void delay(int milliseconds) {
        time.sleep(milliseconds);
    }
}