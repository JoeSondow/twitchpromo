package sondow.twitchpromo;

/**
 * Some cases require twitter user data, and some don't.
 */
public enum TrimUser {
    ENABLED(true),
    DISABLED(false);

    private final boolean value;

    TrimUser(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }
}
