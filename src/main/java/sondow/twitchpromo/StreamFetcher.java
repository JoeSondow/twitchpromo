package sondow.twitchpromo;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Objects;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.mirrobot.twitch.api.Twitch;
import ru.mirrobot.twitch.api.handlers.StreamsResponseHandler;
import ru.mirrobot.twitch.api.models.Streams;
import sondow.common.Environment;

public class StreamFetcher {

    public static void mainWithLib(String[] args) {
        Environment environment = new Environment();
        String authUsername = environment.get("twitch_auth_username");
        String clientId = environment.get("twitch_client_id");
        String accessToken = environment.get("twitch_access_token");
        String targetUsername = environment.get("twitch_target_username");
        Twitch twitch = new Twitch();
        twitch.setClientId(clientId);
        twitch.auth().setAccessToken(accessToken);
        twitch.streams().get(targetUsername, new StreamsResponseHandler() {

            @Override
            public void onSuccess(final Streams streams) {
                ru.mirrobot.twitch.api.models.Stream stream = streams.getData().get(0);
                System.out.println("Stream: " + stream.getTitle());
            }

            @Override
            public void onFailure(int statusCode, String statusMessage, String errorMessage) {
                System.out.println(statusCode + " " + statusMessage + " " + errorMessage);
            }

            @Override public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        });

        //        Stream stream = new StreamFetcher().fetchStreamInfo(authUsername, clientId);
        //        System.out.println(stream.getTitle());
    }

    public static void main(String[] args) {
        Config config = new ConfigFactory("StreamFetcher.main").configure();
        String targetUsername = config.getTargetUsername();
        String clientId = config.getClientId();
        String accessToken = config.getAccessToken();
        Stream stream = new StreamFetcher().fetchStreamInfo(targetUsername, clientId, accessToken);
        System.out.println(stream.getTitle());
        System.out.println(stream.getGame());
    }

    public Stream fetchStreamInfo(String authUsername, String clientId, String accessToken) {
        OkHttpClient client = new OkHttpClient();
        int viewerCount;
        String gameName, startedAt, title;
        String strmUrl = "https://api.twitch.tv/helix/streams?user_login=" + authUsername;
        Request strmReq = buildRequest(strmUrl, clientId, accessToken);

        try {
            Response streamResponse = client.newCall(strmReq).execute();
            String streamsResponseText = Objects.requireNonNull(streamResponse.body()).string();
            String errorMsg = "No stream is live for " + authUsername;
            JSONObject streamJsonObj = getDataJsonObject(errorMsg, streamsResponseText);
            title = streamJsonObj.getString("title");
            viewerCount = streamJsonObj.getInt("viewer_count");
            startedAt = streamJsonObj.getString("started_at");
            gameName = streamJsonObj.getString("game_name");
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        Date createdAt = Date.from(ZonedDateTime.parse(startedAt).toInstant());
        return new Stream(gameName, title, viewerCount, createdAt);
    }

    private Request buildRequest(String url, String clientId, String accessToken) {
        return new Request.Builder().url(url).header("Client-ID", clientId)
                .header("Authorization", String.format("Bearer %s", accessToken)).build();
    }

    private JSONObject getDataJsonObject(String errorIfNotFound, String responseText) {
        JSONObject jsonObject = new JSONObject(responseText);
        JSONArray data = jsonObject.getJSONArray("data");
        if (data.length() < 1) {
            throw new RuntimeException(errorIfNotFound);
        }
        return data.getJSONObject(0);
    }
}
