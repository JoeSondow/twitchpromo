package sondow.twitchpromo;

import java.io.File;

/**
 * Has a reference to the saved screenshot file if the Twitch user is online, or else indicates that
 * the user is offline.
 *
 * @author @JoeSondow
 */
public class ImageDownloadResult {

    private final File file;
    private final boolean online;

    public ImageDownloadResult(File file, boolean online) {
        super();
        this.file = file;
        this.online = online;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @return true if and online if Twitch account is online
     */
    public boolean isOnline() {
        return online;
    }

    /**
     * @return true if the account is offline, false if the account is online
     */
    public boolean isOffline() {
        return !online;
    }

    @Override
    public String toString() {
        return "ImageDownloadResult{" +
                "file=" + file +
                ", online=" + online +
                '}';
    }
}
