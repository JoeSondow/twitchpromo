package sondow.twitchpromo;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import sondow.common.Time;
import twitter4j.JSONArray;
import twitter4j.JSONException;
import twitter4j.JSONObject;
import static sondow.twitchpromo.AwsDynamoDatabase.TTL_KEY;
import static sondow.twitchpromo.StoredTweet.CORE_TEXT_KEY;
import static sondow.twitchpromo.StoredTweet.IN_REPLY_TO_STATUS_ID_KEY;
import static sondow.twitchpromo.StoredTweet.TWEET_ID_KEY;
import static sondow.twitchpromo.StoredTweet.TWITTER_HANDLE_KEY;

public class Converter {

    static AttributeValue attr(Long value) {
        AttributeValue attrVal = new AttributeValue();
        attrVal.setN(Long.toString(value));
        return attrVal;
    }

    static AttributeValue attr(String value) {
        return new AttributeValue(value);
    }

    /**
     * This is only used for storing data in DynamoDB
     *
     * @param tweet the tweet to convert
     * @param time  the time utility to use
     * @return the item map
     */
    Map<String, AttributeValue> convertStoredTweetToItemMap(StoredTweet tweet, Time time) {
        Map<String, AttributeValue> item = new LinkedHashMap<>();
        item.put(TWEET_ID_KEY, attr(Long.parseLong(tweet.getTweetId())));
        item.put(CORE_TEXT_KEY, attr(tweet.getCoreText()));
        item.put(AwsDynamoDatabase.TIME_STAMP_KEY, attr(tweet.getTimestampString()));
        item.put(TWITTER_HANDLE_KEY, attr(tweet.getTwitterHandle()));
        String inReplyToStatusId = tweet.getInReplyToStatusId();
        if (inReplyToStatusId != null) {
            Long inReply = Long.parseLong(inReplyToStatusId);
            item.put(IN_REPLY_TO_STATUS_ID_KEY, attr(inReply));
        }
        item.put(TTL_KEY, attr(time.epochSecondInDays(15)));
        return item;
    }

    /**
     * This is only used to read data from DynamoDB.
     *
     * @param item the item map to convert
     * @return the tweet object
     */
    StoredTweet convertItemMapToStoredTweet(Map<String, AttributeValue> item) {
        String tweetId = item.get(TWEET_ID_KEY).getN();
        String coreText = item.get(CORE_TEXT_KEY).getS();
        ZonedDateTime zonedDateTime = ZonedDateTime
                .parse(item.get(AwsDynamoDatabase.TIME_STAMP_KEY).getS());
        Date timestamp = Date.from(zonedDateTime.toInstant());
        String handle = item.get(TWITTER_HANDLE_KEY).getS();
        AttributeValue inReplyAttr = item.get(IN_REPLY_TO_STATUS_ID_KEY);
        String inReply = null;
        if (inReplyAttr != null) {
            inReply = inReplyAttr.getN();
        }
        return new StoredTweet(tweetId, coreText, timestamp, handle, inReply);
    }

    public Ruleset convertMessagesJsonToRuleset(String messagesJson) {
        Map<String, Rule> rules = new HashMap<>();
        JSONObject obj = null;
        try {
            obj = new JSONObject(messagesJson);
            Iterator keys = obj.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                Rule rule = null;
                Object ruleObj = obj.get(key);
                if (ruleObj instanceof JSONObject) {
                    rule = createRule((JSONObject) ruleObj);
                } else if (ruleObj instanceof JSONArray) {
                    rule = createRule((JSONArray) ruleObj);
                } else if (ruleObj instanceof String) {
                    rule = createRule((String) ruleObj);
                } else {
                    throw new RuntimeException("key " + key +
                            " points to something other than a map, array, or string");
                }
                rules.put(key, rule);
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        return new Ruleset(rules);
    }

    private Rule createRule(String stringRule) {
        assert stringRule.startsWith("#") && stringRule.endsWith("#");
        String referencedGame = stringRule.substring(1, stringRule.length() - 1);
        return new Rule().withReferencedGame(referencedGame);
    }

    private Rule createRule(JSONArray jsonArray) throws JSONException {
        List<String> messages = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            // Handle trailing commas that get interpreted as null references but
            // are not marked as errors in some editors such as GitLab Web IDE
            if (!jsonArray.isNull(i)) {
                String msg = jsonArray.getString(i);
                messages.add(msg);
            }
        }
        return new Rule().withMessages(messages);
    }

    private Rule createRule(JSONObject jsonObj) throws JSONException {
        Iterator innerKeysIter = jsonObj.keys();
        Rule innerRule;
        Map<String, Rule> innerMap = new HashMap<>();
        while (innerKeysIter.hasNext()) {
            String innerKey = (String) innerKeysIter.next();
            Object innerValue = jsonObj.get(innerKey);
            if (innerValue instanceof String) {
                innerRule = createRule((String) innerValue);
            } else if (innerValue instanceof JSONArray) {
                innerRule = createRule((JSONArray) innerValue);
            } else {
                throw new RuntimeException("inner key " + innerKey +
                        " points to something other than an array or string");
            }
            innerMap.put(innerKey, innerRule);
        }
        return new Rule().withTitleFiltersToRules(innerMap);
    }
}
