package sondow.twitchpromo;

import com.google.common.collect.ImmutableList;
import java.util.List;

public class CollectedExceptions extends RuntimeException {

    /**
     * The list of exceptions that occurred while doing a multi-step action.
     */
    private ImmutableList<Exception> exceptions;

    /**
     * Constructor
     *
     * @param message    the detail message for the overall collection of exceptions
     * @param exceptions the ordered list of exceptions that happened before they were collected
     *                   here
     */
    public CollectedExceptions(String message, List<Exception> exceptions) {
        super(message);
        this.exceptions = ImmutableList.copyOf(exceptions);
    }

    /**
     * Gets exceptions that occurred while doing a multi-step action.
     *
     * @return the list of exceptions
     */
    public List<Exception> getExceptions() {
        return exceptions;
    }

    @Override public String toString() {
        return "CollectedExceptions{" +
                "exceptions=" + exceptions +
                '}';
    }
}
