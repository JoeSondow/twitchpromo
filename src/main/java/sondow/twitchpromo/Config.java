package sondow.twitchpromo;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import java.util.List;
import java.util.Objects;
import twitter4j.conf.Configuration;

public class Config implements AWSCredentialsProvider {

    private final String messagesJsonUrl;

    /**
     * The name of the calling function that started this program, if any.
     */
    private String functionName;

    /**
     * Twitch access token.
     */
    private String accessToken;

    /**
     * Number of days back to search for old tweets, to avoid repeating their core messages.
     */
    private int days;

    /**
     * Twitch client ID number.
     */
    private String clientId;

    /**
     * Numerical ID of the twitch account whose schedule we want to look up, as required by the
     * schedule API.
     */
    private String scheduleBroadcasterId;

    /**
     * Twitch target username. The channel whose stream will get examined and shared. Probably the
     * same as the user who is authenticating to the API, but not always.
     */
    private String targetUsername;

    /**
     * The full path to the local directory where screenshot images should be saved.
     */
    private String saveDir;

    /**
     * The number of pixels wide that the screenshot should be.
     */
    private int width;

    /**
     * The number of pixels tall that the screenshot should be.
     */
    private int height;

    private Configuration twitterConfig;

    private AWSCredentials awsCredentials;

    private String airtableAccessToken;

    private String airtableBase;

    /**
     * Configurations for API access for the twitter accounts that are to be used for retweeting the
     * most interesting game polls.
     */
    private List<Configuration> publicityConfigs;

    private int minIntervalMinutes;
    private int medIntervalMinutes;
    private int maxIntervalMinutes;

    /** Override to force a database clean up now */
    private boolean removeNow;

    public Config(String functionName, String accessToken, int days, String clientId,
            String targetUsername, String scheduleBroadcasterId, String saveDir,
            int width, int height, Configuration twitterConfig, AWSCredentials awsCredentials,
            String airtableAccessToken, String airtableBase, String messagesJsonUrl,
            List<Configuration> publicityConfigs, int minIntervalMinutes, int medIntervalMinutes,
            int maxIntervalMinutes, boolean removeNow) {
        super();
        this.functionName = functionName;
        this.accessToken = accessToken;
        this.days = days;
        this.clientId = clientId;
        this.targetUsername = targetUsername;
        this.scheduleBroadcasterId = scheduleBroadcasterId;
        this.saveDir = saveDir;
        this.width = width;
        this.height = height;
        this.twitterConfig = twitterConfig;
        this.awsCredentials = awsCredentials;
        this.airtableAccessToken = airtableAccessToken;
        this.airtableBase = airtableBase;
        this.messagesJsonUrl = messagesJsonUrl;
        this.publicityConfigs = publicityConfigs;
        this.minIntervalMinutes = minIntervalMinutes;
        this.medIntervalMinutes = medIntervalMinutes;
        this.maxIntervalMinutes = maxIntervalMinutes;
        this.removeNow = removeNow;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    /**
     * @return the number of days back to look for tweets about this channel, to avoid repeats
     */
    public int getDays() {
        return days;
    }

    public String getClientId() {
        return clientId;
    }

    public String getScheduleBroadcasterId() {
        return scheduleBroadcasterId;
    }

    public String getTargetUsername() {
        return targetUsername;
    }

    String getSaveDir() {
        return saveDir;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }

    public Configuration getTwitterConfig() {
        return twitterConfig;
    }

    /**
     * Returns AWSCredentials which the caller can use to authorize an AWS request. Each
     * implementation of AWSCredentialsProvider can chose its own strategy for loading credentials.
     * For example, an implementation might load credentials from an existing key management system,
     * or load new credentials when credentials are rotated.
     *
     * @return AWSCredentials which the caller can use to authorize an AWS request.
     */
    @Override
    public AWSCredentials getCredentials() {
        return awsCredentials;
    }

    public String getAirtableAccessToken() {
        return airtableAccessToken;
    }

    public String getAirtableBase() {
        return airtableBase;
    }

    public String getMessagesJsonUrl() {
        return messagesJsonUrl;
    }

    public List<Configuration> getPublicityConfigs() {
        return publicityConfigs;
    }

    public int getMinIntervalMinutes() {
        return minIntervalMinutes;
    }

    public int getMedIntervalMinutes() {
        return medIntervalMinutes;
    }

    public int getMaxIntervalMinutes() {
        return maxIntervalMinutes;
    }

    public boolean isRemoveNow() {
        return removeNow;
    }

    /**
     * Forces this credentials provider to refresh its credentials. For many implementations of
     * credentials provider, this method may simply be a no-op, such as any credentials provider
     * implementation that vends static/non-changing credentials. For other implementations that
     * vend different credentials through out their lifetime, this method should force the
     * credentials provider to refresh its credentials.
     */
    @Override
    public void refresh() {

    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Config config = (Config) o;
        return days == config.days && width == config.width && height == config.height &&
                minIntervalMinutes == config.minIntervalMinutes &&
                medIntervalMinutes == config.medIntervalMinutes &&
                maxIntervalMinutes == config.maxIntervalMinutes && removeNow == config.removeNow &&
                Objects.equals(messagesJsonUrl, config.messagesJsonUrl) &&
                Objects.equals(accessToken, config.accessToken) &&
                Objects.equals(clientId, config.clientId) &&
                Objects.equals(targetUsername, config.targetUsername) &&
                Objects.equals(saveDir, config.saveDir) &&
                Objects.equals(twitterConfig, config.twitterConfig) &&
                Objects.equals(awsCredentials, config.awsCredentials) &&
                Objects.equals(airtableAccessToken, config.airtableAccessToken) &&
                Objects.equals(airtableBase, config.airtableBase) &&
                Objects.equals(publicityConfigs, config.publicityConfigs);
    }

    @Override public int hashCode() {
        return Objects
                .hash(messagesJsonUrl, accessToken, days, clientId, targetUsername, saveDir, width,
                        height, twitterConfig, awsCredentials, airtableAccessToken, airtableBase,
                        publicityConfigs, minIntervalMinutes, medIntervalMinutes,
                        maxIntervalMinutes, removeNow);
    }
}
