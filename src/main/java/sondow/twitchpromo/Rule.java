package sondow.twitchpromo;

import java.util.List;
import java.util.Map;

public class Rule {

    private List<String> messages = null;
    private Map<String, Rule> titleFiltersToRules = null;
    private String referencedGame = null;

    public List<String> getMessages() {
        return messages;
    }

    public Rule withMessages(List<String> messages) {
        this.messages = messages;
        return this;
    }

    public boolean isToPickMessageFromList() {
        return messages != null && messages.size() >= 1;
    }

    public boolean isToCheckTitle() {
        return titleFiltersToRules != null && titleFiltersToRules.size() >= 1;
    }

    public Map<String, Rule> getTitleFiltersToRules() {
        return titleFiltersToRules;
    }

    public boolean isToReferenceOtherGameRule() {
        return referencedGame != null && referencedGame.length() >= 1;
    }

    public String getReferencedGame() {
        return referencedGame;
    }

    public Rule withReferencedGame(String referencedGame) {
        this.referencedGame = referencedGame;
        return this;
    }

    public Rule withTitleFiltersToRules(Map<String, Rule> titleFiltersToRules) {
        this.titleFiltersToRules = titleFiltersToRules;
        return this;
    }

    @Override public String toString() {
        String m = (messages == null) ? "" : ("messages=" + messages + ", ");
        String t = (titleFiltersToRules == null) ? "" :
                ("titleFiltersToRules=" + titleFiltersToRules + ", ");
        String r = (referencedGame == null) ? "" : ("referencedGame='" + referencedGame + "', ");
        return "Rule{" + m + t + r + '}';
    }
}
