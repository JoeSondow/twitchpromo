package sondow.twitchpromo;

import com.sybit.airtable.Airtable;
import com.sybit.airtable.Base;
import com.sybit.airtable.Query;
import com.sybit.airtable.Table;
import com.sybit.airtable.exception.AirtableException;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import sondow.common.Logger;
import sondow.common.Time;
import static sondow.twitchpromo.StoredTweet.CORE_TEXT_KEY;
import static sondow.twitchpromo.StoredTweet.TIMESTAMP_KEY;
import static sondow.twitchpromo.StoredTweet.TWEET_ID_KEY;

public class AirtableDatabase implements Database {

    private final static Logger logger = Logger.getLogger(AirtableDatabase.class);

    private final String tableSuffix;
    private final Config config;

    //    private final Config config;
    private Table<StoredTweet> table;
    private Airtable airtable;
    private Time time;

    /**
     * Constructor for unit tests.
     */
    public AirtableDatabase(Config config, Airtable airtable, Time time) {
        this(config);
        this.airtable = airtable;
        this.time = time;
    }

    /**
     * Constructor for production.
     */
    AirtableDatabase(Config config) {
        this.config = config;
        this.tableSuffix = config.getTwitterConfig().getUser().toUpperCase();
        this.time = new Time();
    }

    public static void main(String[] args) {
        Config config = new ConfigFactory("AirtableDatabase.main").configure();
        AirtableDatabase database = new AirtableDatabase(config);
        database.doQuery();
        //        database.retrieveRecentTweets(15, "joesondow");
    }

    private String buildTableName() {
        return "TWITCHPROMO_TWEETS_" + tableSuffix;
    }

    private void doQuery() {
        boolean yesCreate = true;
        boolean yesDelete = true;
        boolean yesClean = true;

        // Comment these out to do the actions
        yesCreate = false;
        yesDelete = false;
        //        yesClean = false;

        Query query = new AirtableQuery("", new ArrayList<>());

        Table<StoredTweet> table = getTable();

        //        String screenName = config.getWritingTwitterConfig().getUser();
        //        Query query = new AirtableQuery(screenName, Arrays.asList(fields));
        List<StoredTweet> results;
        try {
            if (yesCreate) {
                for (int i = 0; i < 30; i++) {
                    StoredTweet item = new StoredTweet();
                    Random random = new Random();
                    int daysInFuture = random.nextInt(30) - 20;
                    item.setTweetId("" + Math.abs(random.nextLong()));

                    Date date = new Date();

                    item.setTimestamp(date);

                    item.setCoreText("ts is " + date + ", ttl is " + daysInFuture +
                            " days in future. Random number: " + Math.abs(random.nextInt()));
                    item.setTwitterHandle("JoeSondow");
                    item.setDeleted(random.nextBoolean());

                    insert(item, table);
                }
            }

            // Epoch or ISO date format? See airtable sql reference:
            // https://support.airtable.com/hc/en-us/articles/203255215-Formula-Field-Reference

            results = table.select(query);

            Instant now = Instant.now();
            for (StoredTweet item : results) {

                if (yesDelete) {
                    delete(item, table);
                    logger.info("destroyed " + item);
                }

                if (yesClean) {
                    removeExpiredItems(15, "SchoolsOfFish");
                }
            }
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        }
        //        logger.info(results);
    }

    @SuppressWarnings("unchecked")
    private Table<StoredTweet> getTable() {
        if (table == null) {
            Base base;
            try {
                if (airtable == null) {
                    String airtableAccessToken = config.getAirtableAccessToken();
                    airtable = new Airtable().configure(airtableAccessToken);
                }
                String baseId = config.getAirtableBase();
                base = airtable.base(baseId);
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            }
            String tableName = buildTableName();
            table = base.table(tableName, StoredTweet.class);
        }
        return table;
    }

    @Override public void storeTweet(StoredTweet tweet) {
        // TODO implement and test store tweet
        Table<StoredTweet> table = getTable();
        logger.info("Storing data: " + tweet);
        StoredTweet storedTweet = insert(tweet, table);
        logger.info("Data storage result: " + storedTweet);
    }

    private StoredTweet insert(StoredTweet tweet, Table<StoredTweet> table) {
        return mutate(Action.CREATE, tweet, table);
    }

    private StoredTweet update(StoredTweet tweet, Table<StoredTweet> table) {
        return mutate(Action.UPDATE, tweet, table);
    }

    private void delete(StoredTweet tweet, Table<StoredTweet> table) {
        mutate(Action.DESTROY, tweet, table);
    }

    @SuppressWarnings("TryWithIdenticalCatches") // AWS Lambda only allows Java 8
    private StoredTweet mutate(Action action, StoredTweet tweet, Table<StoredTweet> table) {

        return new Retriable<>((howManyTriesSoFar) -> {
            StoredTweet innerResult = null;
            try {
                if (action == Action.CREATE) {
                    innerResult = table.create(tweet);
                } else if (action == Action.UPDATE) {
                    innerResult = table.update(tweet);
                } else if (action == Action.DESTROY) {
                    table.destroy(tweet.getId());
                } else {
                    throw new RuntimeException("Action " + action + " is unknown, wtf?");
                }
            } catch (AirtableException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
            return innerResult;
        }, "mutate database " + action, null, time).performWithRetries();
    }

    @Override public List<StoredTweet> retrieveRecentTweets(int days, String twitterHandle) {
        // TODO finish implementing and testing retrieve recent tweets

        // TODO only get the fields you have use for, to reduce bandwidth usage
        List<String> fields = Arrays.asList(TWEET_ID_KEY, CORE_TEXT_KEY, TIMESTAMP_KEY);
        String filterByFormula = "AND(" +
                "LOWER(twitter_handle) = LOWER('" + twitterHandle + "'), " +
                "timestamp >= DATEADD(TODAY(), -" + days + ", 'days'))";
        Query query = new AirtableQuery(filterByFormula, fields);
        List<StoredTweet> retrievedStoredTweets;
        try {
            retrievedStoredTweets = getTable().select(query);
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        }
        return retrievedStoredTweets;
    }

    @Override public void markTweetDeleted(long tweetId) {
        // TODO implement and test mark tweet deleted
        Table<StoredTweet> table = getTable();
        String filterByFormula = "tweet_id = " + tweetId;
        Query query = new AirtableQuery(filterByFormula, new ArrayList<>());
        try {
            List<StoredTweet> results = table.select(query);
            if (results.size() == 1) {
                StoredTweet storedTweet = results.get(0);
                logger.info("Tweet before it's marked deleted in database: " + storedTweet);
                storedTweet.setDeleted(true);
                StoredTweet updated = update(storedTweet, table);
                logger.info("Tweet after it's been marked deleted in database: " + updated);
            } else {
                logger.info(
                        "Error: expected 1 database entry for tweet id " + tweetId + " but found " +
                                results.size() + " instead: " + results);
            }
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        }
    }

    @Override public void removeExpiredItems(int days, String twitterHandle) {

        // TODO test removeExpiredItems

        List<String> fields = Collections.singletonList(TWEET_ID_KEY);
        String filterByFormula = "AND(" +
                "LOWER(twitter_handle) = LOWER('" + twitterHandle + "'), " +
                "timestamp < DATEADD(TODAY(), -" + days + ", 'days'))";
        Query query = new AirtableQuery(filterByFormula, fields);
        List<StoredTweet> retrievedStoredTweets;
        try {
            Table<StoredTweet> table = getTable();
            retrievedStoredTweets = table.select(query);
            for (StoredTweet tweet : retrievedStoredTweets) {
                table.destroy(tweet.getId());
            }
        } catch (AirtableException e) {
            throw new RuntimeException(e);
        }
    }

    private enum Action {
        CREATE,
        UPDATE,
        DESTROY
    }
}
