package sondow.twitchpromo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.apache.commons.text.similarity.LevenshteinDistance;

public class Composer {

    private Random random;
    private TextFileProvider textFileProvider;

    public Composer(Random random, TextFileProvider textFileProvider) {
        this.random = random;
        this.textFileProvider = textFileProvider;
    }

    static boolean areStringsSimilar(String s1, String s2) {
        boolean isSimilar = false;
        if (s1 != null && s2 != null) {
            // threshold should be a percentage of the length of the longer string.
            String longer = s1.length() > s2.length() ? s1 : s2;
            int threshold = (int) Math.round(longer.length() * 0.3);
            Integer edits = new LevenshteinDistance(threshold).apply(s1, s2);
            // -1 means lots of edits, so 0 thru threshold edits means the two strings are similar.
            if (edits >= 0 && edits <= threshold) {
                isSimilar = true;
            }
        }
        return isSimilar;
    }

    /**
     * For manual testing of JSON file to see if it can be parsed.
     */
    public static void main(String[] args) {
        Config config = new ConfigFactory("Composer.main").configure();
        Stream stream = new Stream("Horizon Zero Dawn", "Shooting robots", 0, new Date());
        TextFileProvider textFileProvider = new TextFileProvider();
        Composer composer = new Composer(new Random(), textFileProvider);
        Composition composition = composer.compose(config, new ArrayList<>(), stream);
        String fullMessage = composition.getFullMessage();
        String imageAltText = composition.getImageAltText();
        System.out.println(fullMessage);
        System.out.println(imageAltText);
    }

    /**
     * Composes the body of the tweet based on the Game and Title fields of the stream, and a json
     * file of messages based on user-created rules.
     * <p>
     * Three things go in an automated twitch promo tweet:
     * <p>
     * 1. Text about coming to the stream. Zero emoji, zero hashtags, zero @-mentions in this. Just
     * alphanumeric, spaces, periods, commas, question marks, apostrophes.
     * <p>
     * 2. Photo of the stream preview.
     * <p>
     * 3. Link to the stream, which is searchable as twitch.tv/JoeSondow (no http nor https)
     * <p>
     * Optional: one or more hashtags but beware Twitter's dislike of automated accounts using
     * hashtags too much. #twitch #SupportSmallStreamers #twitchtv #Livestream (#LetsPlay if a
     * game)
     * <p>
     * Optional: emojis related to the topic
     * <p>
     * Optional: one or more retweet support account mentions, but beware Twitter's dislike of
     * automated accounts mentioning other accounts.
     * <p>
     * Some retweet helper accounts do not require you to follow them.
     * <p>
     * Accts: TwitchShare Twitch_RT TwitchSharing Share_stream Small_Streamers TwitchTVOnline
     * <p>
     * Other retweet helper accounts state that they do require you to follow them:
     * <p>
     * Accts: NightRTs TwitchHelpers Retweet_Twitch TwitchSharer SupStreamers HyperRTs
     * Retweet_Twitch
     * <p>
     * It's risky to tag accounts in automated tweets, though. Might get your account suspended if
     * you do it a lot. If you do it, make it rare.
     * <p>
     * Gathers text content from previous 10 twitch tweets, stripped of image, link, hashtags,
     * mentions, and emoji, trimmed of trailing whitespace, and then pick a message that is not the
     * same as any of these recent options.
     *
     * @param config       objects containing strings for accessing the Twitch API to gather
     *                     relevant info about what is streaming
     * @param recentTweets List < StoredTweet > the promo tweets that have already happened
     *                     recently, which is helpful in order to avoid duplicating a similar
     *                     message to one that was used recently
     * @param stream       the currently running twitch stream
     * @return the entire message to tweet including link, but not image
     */
    public Composition compose(Config config, List<StoredTweet> recentTweets, Stream stream) {

        System.out.println("Composing based on " + recentTweets.size() + " recent tweets: ");
        String username = config.getTargetUsername();
        String game = stream.getGame();
        String title = stream.getTitle();
        //        int viewerCount = stream.getViewers();
        //        Date streamStart = stream.getCreatedAt();
        String messagesJsonUrl = config.getMessagesJsonUrl();
        String json = textFileProvider.download(messagesJsonUrl);
        Ruleset ruleset = new Converter().convertMessagesJsonToRuleset(json);
        List<String> relevantMessages = ruleset.getMessages(game, title);
        String newCoreMessage = pickNovelMessage(title, relevantMessages, recentTweets, game);

        // If we still couldn't find one, abort I guess. I mean, we could go through the list in
        // order until we find an unused message but this really seems like a slim edge case to me.
        if (newCoreMessage == null) {
            throw new RuntimeException("Abort to avoid spammy repetition. After many tries picking "
                    + "randomly from list: " + relevantMessages + " no message found that was "
                    + "very different from recent tweets: " + recentTweets);
        }
        String newFullMessage = newCoreMessage + " https://twitch.tv/" + username;
        String imageAltText = "Current auto-captured screenshot of " + username +
                " Twitch livestream. Category: " + game + ". Title: " + title;
        return new Composition(newFullMessage, newCoreMessage, imageAltText);
    }

    String pickNovelMessage(String title, List<String> sourceMsgs,
            List<StoredTweet> recentTweets, String game) {

        String newMessage;

        // If the stream title has not recently been used as a tweet, then use the title.
        Predicate<StoredTweet> matchesTitle = t -> areStringsSimilar(t.getCoreText(), title);
        if (recentTweets.stream().noneMatch(matchesTitle)) {
            newMessage = title;
        } else {
            Function<String, String> process = msg -> msg.replaceAll("#game#", game);
            Collector<String, ?, List<String>> toList = Collectors.toList();
            List<String> processedCoreMsgs = sourceMsgs.stream().map(process).collect(toList);

            // Find all relevant messages that have not been used recently.
            List<String> freshMessages = findUnusedMessages(processedCoreMsgs, recentTweets);
            if (freshMessages.isEmpty()) {
                // Then pick the message that has gone the longest without use.
                newMessage = findMostForgottenMessage(processedCoreMsgs, recentTweets);
            } else {
                // Pick one of the freshMessages at random to use.
                newMessage = freshMessages.get(random.nextInt(freshMessages.size()));
            }
        }
        return newMessage;
    }

    List<String> findUnusedMessages(List<String> processedCoreMessages,
            List<StoredTweet> recentTweets) {

        // Note that this algorithm could be reimplemented with streams and predicates if you want.
        List<String> messagesNotUsedRecently = new ArrayList<>();
        for (String coreMsg : processedCoreMessages) {

            // Is this msg something fresh to the twitter audience?
            boolean messageAlreadyUsed = false;
            for (int i = 0; !messageAlreadyUsed && i < recentTweets.size(); i++) {
                StoredTweet tweet = recentTweets.get(i);
                if (areStringsSimilar(tweet.getCoreText(), coreMsg)) {
                    // Exclude this message
                    messageAlreadyUsed = true;
                }
            }
            if (!messageAlreadyUsed) {
                messagesNotUsedRecently.add(coreMsg);
            }
        }
        return messagesNotUsedRecently;
    }

    /**
     * This method assumes that all the specified relevant messages have been used in the specified
     * recent tweets. This method seeks to identify which of those messages has gone the longest
     * without being used in a tweet.
     * <p>
     * The messages are original source messages, so they might not be stripped of non-ascii
     * characters yet. The core messages in the tweets most likely have been stripped already.
     *
     * @param processedCoreMessages the messages from which to draw the most forgotten one
     * @param tweets                the tweets that used all those messages, with tweet timestamps
     * @return the core message that has gone the longest without having been used
     */
    String findMostForgottenMessage(List<String> processedCoreMessages,
            List<StoredTweet> tweets) {

        Comparator<StoredTweet> byTimeStamp = Comparator.comparing(StoredTweet::getTimestamp);
        List<StoredTweet> mostRecentUsages = new ArrayList<>();
        for (String coreMsg : processedCoreMessages) {
            // Find all usages of this message.
            Predicate<StoredTweet> similar = t -> areStringsSimilar(t.getCoreText(), coreMsg);
            Collector<StoredTweet, ?, List<StoredTweet>> toList = Collectors.toList();
            List<StoredTweet> usages = tweets.stream().filter(similar).collect(toList);

            // Find the newest usage of coreMsg
            usages.stream().max(byTimeStamp).ifPresent(mostRecentUsages::add);
        }
        // Find the oldest tweet in mostRecentUsages
        Optional<StoredTweet> opt = mostRecentUsages.stream().min(byTimeStamp);
        return opt.map(StoredTweet::getCoreText).orElse(null);
    }
}
