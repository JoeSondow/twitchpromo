package sondow.twitchpromo;

import java.util.Date;
import java.util.Objects;

public class Stream {
    private final String game;
    private final String title;
    private final int viewers;
    private final Date createdAt;

    public Stream(String game, String title, int viewers, Date createdAt) {
        this.game = game;
        this.title = title;
        this.viewers = viewers;
        this.createdAt = createdAt;
    }

    public String getGame() {
        return game;
    }

    public Stream withGame(String game) {
        return new Stream(game, this.title, this.viewers, this.createdAt);
    }

    public String getTitle() {
        return title;
    }

    public Stream withTitle(String title) {
        return new Stream(this.game, title, this.viewers, this.createdAt);
    }

    public int getViewers() {
        return viewers;
    }

    public Stream withViewers(int viewers) {
        return new Stream(this.game, this.title, viewers, this.createdAt);
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Stream withCreatedAt(Date createdAt) {
        return new Stream(this.game, this.title, this.viewers, createdAt);
    }

    @Override public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Stream stream = (Stream) o;
        return viewers == stream.viewers &&
                Objects.equals(game, stream.game) &&
                Objects.equals(title, stream.title) &&
                Objects.equals(createdAt, stream.createdAt);
    }

    @Override public int hashCode() {
        return Objects.hash(game, title, viewers, createdAt);
    }

    @Override public String toString() {
        return "Stream{" +
                "game='" + game + '\'' +
                ", title='" + title + '\'' +
                ", viewers=" + viewers +
                ", createdAt=" + createdAt +
                '}';
    }
}
