package sondow.twitchpromo;

import java.util.List;

public interface Database {

    void storeTweet(StoredTweet tweet);

    List<StoredTweet> retrieveRecentTweets(int days, String twitterHandle);

    void markTweetDeleted(long tweetId);

    void removeExpiredItems(int days, String twitterHandle);
}
