package sondow.twitchpromo;

import java.io.File;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Random;
import sondow.cv.ArtCritic;
import twitter4j.HttpResponseCode;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import static java.time.ZoneOffset.UTC;

/**
 * Decides whether to tweet and whether to delete any tweets.
 */
public class Decider {

    /**
     * A pseudorandom number from 0-99 for choosing whether or not to tweet when the timing is in
     * the medium or short time range.
     */
    private int percentileChance;

    private Config config;
    private Twitter twitter;
    private ArtCritic artCritic;

    public Decider(Config config, Twitter twitter, int percentileChance,
            ArtCritic artCritic) {
        this.config = config;
        this.twitter = twitter;
        this.percentileChance = percentileChance;
        this.artCritic = artCritic;
    }

    public Decider(Config config, Twitter twitter) {
        this(config, twitter, new Random().nextInt(100), new ArtCritic());
    }

    /**
     * If no recent promo tweet is found, start a new thread.
     * <p>
     * If it's been 79 minutes or more since the last tweet, then tweet.
     * <p>
     * If it's been 59-79 minutes since the last tweet, there's a 66% chance of tweeting.
     * <p>
     * If it's been 39-59 minutes since the last tweet, there's a 33% chance of tweeting.
     * <p>
     * If it's been less than 39 minutes since the last tweet, don't tweet.
     * <p>
     * If the stream started before the last tweet, add to the existing twitter thread (if any).
     *
     * @param now       the current time to compare against the most recent tweet
     * @param tweets    the most recent tweets, including the timestamps to compare with now
     * @param stream    the currently running twitch stream
     * @param imageFile the current screenshot
     * @return the decision of whether to tweet, and what tweet to reply to if continuing a thread
     */
    Decision decideWhatToDo(ZonedDateTime now, List<StoredTweet> tweets, Stream stream,
            File imageFile) {
        Decision decision = new Decision();
        StoredTweet recentTweet = pickMostRecent(tweets);

        // Don't tweet if the stream started less than a minute ago. The first minute is boring.
        Instant streamStartInstant = stream.getCreatedAt().toInstant();
        ZonedDateTime streamStartTime = ZonedDateTime.ofInstant(streamStartInstant, UTC);
        boolean hasEnoughTimePassed = false;
        boolean shouldDeleteBoringTweets = false;
        int minutesBeforeTweeting = 1; // TODO make minutesBeforeTweeting configurable
        if (now.minusMinutes(minutesBeforeTweeting).isBefore(streamStartTime)) {
            // The stream just started. Probably not worth taking a screenshot now. Don't tweet yet.
            hasEnoughTimePassed = false;
        } else if (recentTweet == null) {
            // Stream's been on a while. No recent tweet. Start new thread.
            hasEnoughTimePassed = true;
        } else if (decideIfItIsTimeToTweet(now, recentTweet)) {
            // Stream has tweets. Reply to latest interesting tweet. Delete boring tweets.
            hasEnoughTimePassed = true;
            shouldDeleteBoringTweets = true;
        }
        if (hasEnoughTimePassed && isImageInteresting(imageFile)) {
            StoredTweet recent = getMostRecentPromoTweetFromTwitterApi();
            if (recent == null || decideIfItIsTimeToTweet(now, recent)) {
                decision.setItIsTimeToTweet(true);
                if (shouldDeleteBoringTweets) {
                    decideWhatToDeleteAndReplyTo(tweets, decision, streamStartTime);
                }
            }
        }
        return decision;
    }

    private StoredTweet getMostRecentPromoTweetFromTwitterApi() {
        // Assume that the database might be unreliable, and that the previous attempts to write
        // to the database might have failed due to database server errors.
        // Double-check the Twitter API to see what relevant tweets have occurred.
        List<StoredTweet> fromTwitterApi = new ArrayList<>();
        ResponseList<Status> userTimeline;
        try {
            userTimeline = twitter.getUserTimeline();
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
        for (Status status : userTimeline) {
            if (!status.isRetweet() && status.getMediaEntities().length == 1 &&
                    status.getURLEntities().length == 1 &&
                    status.getURLEntities()[0].getExpandedURL().contains("twitch.tv")) {
                long inReply = status.getInReplyToStatusId();
                String inReplyString = (inReply == -1) ? null : "" + inReply;
                StoredTweet storedTweet = new StoredTweet("" + status.getId(),
                        status.getText(), status.getCreatedAt(),
                        null, inReplyString);
                fromTwitterApi.add(storedTweet);
            }
        }
        return pickMostRecent(fromTwitterApi);
    }

    private boolean isImageInteresting(File imageFile) {
        return artCritic.isImageInteresting(imageFile);
    }

    private boolean decideIfItIsTimeToTweet(ZonedDateTime now, StoredTweet recentTweet) {
        // Some tweets found, so analyze them.
        Date timestamp = recentTweet.getTimestamp();
        ZonedDateTime recentTweetTime = ZonedDateTime.ofInstant(timestamp.toInstant(), UTC);

        boolean isItTimeToDeleteAndTweet = false;

        int minIntervalMinutes = config.getMinIntervalMinutes();
        int medIntervalMinutes = config.getMedIntervalMinutes();
        int maxIntervalMinutes = config.getMaxIntervalMinutes();

        // We don't want to spam followers. Only tweet if it's been a while.
        if (recentTweetTime.isBefore(now.minusMinutes(maxIntervalMinutes))) {
            // It's been a very long time so definitely tweet now.
            isItTimeToDeleteAndTweet = true;
        } else if (recentTweetTime.isBefore(now.minusMinutes(medIntervalMinutes))) {
            // It's been a medium length time, so probably tweet now but not necessarily.
            if (percentileChance < 66) {
                isItTimeToDeleteAndTweet = true;
            }
        } else if (recentTweetTime.isBefore(now.minusMinutes(minIntervalMinutes))) {
            // It's been a short time, so maybe tweet now but probably not.
            if (percentileChance < 33) {
                isItTimeToDeleteAndTweet = true;
            }
        }
        return isItTimeToDeleteAndTweet;
    }

    private void decideWhatToDeleteAndReplyTo(List<StoredTweet> tweets, Decision decision,
            ZonedDateTime streamStartTime) {
        // If it's time to tweet, then delete recent twitch tweets that have not met the criteria
        // for being kept (default rule is 1 Like or 1 Retweet lets the tweet remain.)

        // If there is a thread with a bad tweet, then a good tweet, then a bad tweet, ensure
        // that tweets on the later end of the thread get deleted, not the early end.
        List<StoredTweet> chronological = new ArrayList<>(tweets);
        chronological.sort(Comparator.comparing(StoredTweet::getTimestamp));
        StoredTweet latestGoodTweet = null;
        boolean goodTweetFound = false;
        int minRtsOrFaves = 2; // TODO make minRtsOrFaves configurable
        for (int i = chronological.size() - 1; i >= 0 && !goodTweetFound; i--) {
            StoredTweet tweet = chronological.get(i);
            Status status;
            long tweetId = Long.parseLong(tweet.getTweetId());
            try {
                status = twitter.showStatus(tweetId);
            } catch (TwitterException e) {
                if (e.getStatusCode() == HttpResponseCode.NOT_FOUND) {
                    status = null;
                } else {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
            }
            if (status == null) {
                decision.markTweetDeleted(tweetId);
            } else if (status.getRetweetCount() >= minRtsOrFaves ||
                    status.getFavoriteCount() >= minRtsOrFaves) {
                goodTweetFound = true;
                latestGoodTweet = tweet;
            } else {
                decision.deleteTweet(status.getId());
                System.out.println("Deleting tweet " + status.getId() + " with text: ' " +
                        status.getText() + " '");
            }
        }

        // If latestGoodTweet time is after streamStartTime then we're still in the same
        // stream, so continue the twitter thread.
        if (latestGoodTweet != null) {
            Date timestamp = latestGoodTweet.getTimestamp();
            ZonedDateTime tweetTime = ZonedDateTime.ofInstant(timestamp.toInstant(), UTC);
            if (tweetTime.isAfter(streamStartTime)) {
                // Threading is broken in my Twitter account so don't thread the tweets for now.
                // decision.setInReplyToStatusId(latestGoodTweet.getTweetId());
            }
        }
    }

    StoredTweet pickMostRecent(List<StoredTweet> tweets) {
        StoredTweet mostRecentTweet = null;
        for (StoredTweet tweet : tweets) {
            if (mostRecentTweet == null) {
                mostRecentTweet = tweet;
            } else {
                Instant instant = tweet.getTimestamp().toInstant();
                Instant mostRecentTweetInstant = mostRecentTweet.getTimestamp().toInstant();
                if (instant.isAfter(mostRecentTweetInstant)) {
                    mostRecentTweet = tweet;
                }
            }
        }
        return mostRecentTweet;
    }
}
