package sondow.twitchpromo;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.AttributeValueUpdate;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult;
import com.amazonaws.services.dynamodbv2.model.DescribeTimeToLiveRequest;
import com.amazonaws.services.dynamodbv2.model.DescribeTimeToLiveResult;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.QueryResult;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.dynamodbv2.model.TimeToLiveDescription;
import com.amazonaws.services.dynamodbv2.model.TimeToLiveSpecification;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.amazonaws.services.dynamodbv2.model.UpdateTimeToLiveRequest;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import sondow.common.Time;
import static com.amazonaws.services.dynamodbv2.model.AttributeAction.PUT;
import static sondow.twitchpromo.StoredTweet.DELETED_KEY;
import static sondow.twitchpromo.StoredTweet.TWEET_ID_KEY;
import static sondow.twitchpromo.StoredTweet.TWITTER_HANDLE_KEY;

public class AwsDynamoDatabase implements Database {

    // Special case because DynamoDB reserves the name timestamp internally
    static final String TIME_STAMP_KEY = "time_stamp";

    /**
     * Special case field for DynamoDB, which does automatic cleaning of expired records but only if
     * you provide an epoch ttl field.
     */
    static final String TTL_KEY = "ttl";

    private final String tableSuffix;
    private final AWSCredentialsProvider cred;
    private final Time time;
    private AmazonDynamoDB db;

    /**
     * Constructor for unit tests.
     */
    public AwsDynamoDatabase(String tableSuffix, Time time, AmazonDynamoDB db) {
        this.cred = null;
        this.tableSuffix = tableSuffix.toUpperCase();
        this.time = time;
        this.db = db;
    }

    /**
     * Constructor for production.
     */
    AwsDynamoDatabase(AWSCredentialsProvider awsCredentialsProvider, String tableSuffix) {
        this.cred = awsCredentialsProvider;
        this.tableSuffix = tableSuffix.toUpperCase();
        this.time = new Time();
    }

    public static void main(String[] args) {
        Config config = new ConfigFactory("AwsDynamoDatabase.main").configure();
        String handle = config.getTwitterConfig().getUser();
        AwsDynamoDatabase database = new AwsDynamoDatabase(config, handle);
        List<StoredTweet> storedTweets = database.retrieveRecentTweets(14, handle);
        System.out.println("[");
        for (StoredTweet tweet : storedTweets) {

            String tweetId = tweet.getTweetId();
            String coreText = tweet.getCoreText().replaceAll("\n", "\\\\n");
            Date timestamp = tweet.getTimestamp();
            String twitterHandle = tweet.getTwitterHandle();
            String inReplyToStatusId = tweet.getInReplyToStatusId();
            String inReply = inReplyToStatusId == null ? "null" : inReplyToStatusId + "L";

            new StoredTweet(tweetId, coreText, timestamp, twitterHandle, inReplyToStatusId);
            System.out.println("new StoredTweet(" + tweetId + "L, \"" + coreText + "\", " +
                    "ZonedDateTime.parse(\"" + timestamp + "\"), \"" + twitterHandle + "\", " +
                    inReply + "),");
        }
        System.out.println("]");
    }

    private String buildTableName() {
        return "TWITCHPROMO_TWEETS_" + tableSuffix;
    }

    private String buildIndexName() {
        return buildTableName() + "_TIMESTAMP_INDEX";
    }

    /**
     * An item should look like this: tweet_id : 234324345435 full_text: "Come watch my stream! 😉
     * #neat @TwitchRT https://twitch.tv/scooby" core_text: "Come watch my stream!" timestamp :
     * "2018-01-03T10:45:32Z" account_number: 457856754674 twitter_handle: ScoobyTweets
     *
     * @param tweet the tweet data to store in the database
     */
    @Override
    public void storeTweet(StoredTweet tweet) {
        ensureTableExists();

        Converter converter = new Converter();
        Map<String, AttributeValue> item = converter.convertStoredTweetToItemMap(tweet, time);
        PutItemRequest request = new PutItemRequest(buildTableName(), item);

        System.out.println("Storing data: " + item);
        PutItemResult putItemResult = getDb().putItem(request);
        System.out.println("PutItemResult: " + putItemResult);
    }

    private DescribeTableResult createTable() {
        // Make the table cuz it ain't there yet.
        String tableName = buildTableName();

        KeySchemaElement tweetIdHashSchemaEl = new KeySchemaElement(TWEET_ID_KEY, KeyType.HASH);
        List<KeySchemaElement> tweetIdSchema = Collections.singletonList(tweetIdHashSchemaEl);
        CreateTableRequest createTableRequest = new CreateTableRequest(tableName, tweetIdSchema);

        KeySchemaElement handleSchemaEl = new KeySchemaElement(TWITTER_HANDLE_KEY, KeyType.HASH);
        KeySchemaElement timeStampSchemaEl = new KeySchemaElement(TIME_STAMP_KEY, KeyType.RANGE);
        List<KeySchemaElement> timeStampSchema = Arrays.asList(handleSchemaEl, timeStampSchemaEl);

        AttributeDefinition tweetIdAttrDef = new AttributeDefinition(TWEET_ID_KEY, "N");
        AttributeDefinition handleAttrDef = new AttributeDefinition(TWITTER_HANDLE_KEY, "S");
        AttributeDefinition timeAttrDef = new AttributeDefinition(TIME_STAMP_KEY, "S");
        List<AttributeDefinition> attrs = Arrays.asList(tweetIdAttrDef, handleAttrDef, timeAttrDef);
        createTableRequest.setAttributeDefinitions(attrs);

        ProvisionedThroughput throughput = new ProvisionedThroughput(10L, 10L);
        GlobalSecondaryIndex index = new GlobalSecondaryIndex();
        index.withIndexName(buildIndexName()).withKeySchema(timeStampSchema);
        index.withProjection(new Projection().withProjectionType(ProjectionType.ALL));
        index.withProvisionedThroughput(throughput);

        createTableRequest.setGlobalSecondaryIndexes(Collections.singletonList(index));
        createTableRequest.setProvisionedThroughput(throughput);
        AmazonDynamoDB db = getDb();
        System.out.println("Creating DynamoDB table: " + tableName);
        db.createTable(createTableRequest);
        return waitForActiveTable(tableName, db);
    }

    private void ensureTimeToLiveIsSet() {
        System.out.println("ensureTimeToLiveIsSet called");
        String tableName = buildTableName();
        DescribeTimeToLiveRequest describeTtlRequest = new DescribeTimeToLiveRequest();
        describeTtlRequest.setTableName(tableName);
        DescribeTimeToLiveResult describeTtlResult = getDb().describeTimeToLive(describeTtlRequest);
        TimeToLiveDescription ttlDescription = describeTtlResult.getTimeToLiveDescription();
        String timeToLiveStatus = ttlDescription.getTimeToLiveStatus();
        // https://docs.aws.amazon
        // .com/amazondynamodb/latest/APIReference/API_TimeToLiveDescription.html
        if (!"ENABLED".equals(timeToLiveStatus)) {
            System.out.println("TTL for table not enabled yet. Enabling…");
            TimeToLiveSpecification spec = new TimeToLiveSpecification();
            spec.withAttributeName(TTL_KEY).withEnabled(true);
            UpdateTimeToLiveRequest updateTtlRequest = new UpdateTimeToLiveRequest();
            updateTtlRequest.setTableName(tableName);
            updateTtlRequest.setTimeToLiveSpecification(spec);
            getDb().updateTimeToLive(updateTtlRequest);
            System.out.println("TTL for table updated");
        }
    }

    private DescribeTableResult waitForActiveTable(String tableName, AmazonDynamoDB db) {
        DescribeTableResult describeTableResult = db.describeTable(tableName);
        String tableStatus = describeTableResult.getTable().getTableStatus();
        int timesItWasActive = 0;
        // Even after the table is marked ACTIVE, it still takes a few more seconds before you can
        // put data into it. This is probably an eventual consistency bug with no external perfect
        // workaround, so we'll just wait and hope it works after 6 seconds. 🤷‍♀️
        while (timesItWasActive < 6) {
            System.out.print(tableStatus + " ");
            time.waitASec();
            tableStatus = describeTableResult.getTable().getTableStatus();
            if ("ACTIVE".equals(tableStatus)) {
                timesItWasActive++;
            }
            describeTableResult = db.describeTable(tableName);
        }
        System.out.println();
        return describeTableResult;
    }

    private AmazonDynamoDB getDb() {
        if (db == null) {
            String r = System.getenv("AWS_DEFAULT_REGION");
            if (r == null) {
                r = "us-west-2";
            }
            db = AmazonDynamoDBClientBuilder.standard().withRegion(r).withCredentials(cred).build();
        }
        return db;
    }

    private void ensureTableExists() {
        System.out.println("ensureTableExists method entered");
        DescribeTableResult result;
        String tableName = buildTableName();
        try {
            System.out.println("Attempting to describe table " + tableName);
            result = getDb().describeTable(tableName);
            System.out.println("Found table " + result.getTable().getTableName());
        } catch (ResourceNotFoundException e) {
            // TODO unit test this failure case
            System.out.println("Table not found " + tableName);
            result = createTable();
            System.out.println("Created table " + result.getTable().getTableName());
        }
        ensureTimeToLiveIsSet();

        TableDescription tableDesc = result.getTable();
        String gotTableName = tableDesc.getTableName();
        Long count = tableDesc.getItemCount();
        System.out.println("table name found: " + gotTableName + ", count: " + count);
    }

    /**
     * Fetches recent tweets from the database.
     *
     * @param days number of days back to look for tweets
     * @return twitch promo tweets stored in the database since the specified number of days ago
     */
    @Override
    public List<StoredTweet> retrieveRecentTweets(int days, String twitterHandle) {
        ensureTableExists();

        ZonedDateTime now = time.nowZonedDateTime();
        ZonedDateTime daysAgo = now.minusDays(days);
        String daysAgoString = daysAgo.toString();
        List<StoredTweet> retrievedStoredTweets = new ArrayList<>();

        // https://docs.aws.amazon
        // .com/amazondynamodb/latest/developerguide/QueryingJavaDocumentAPI.html

        AmazonDynamoDB db = getDb();
        Map<String, AttributeValue> attrNamesToAttrValues = new HashMap<>();
        Converter converter = new Converter();
        attrNamesToAttrValues.put(":v_twitter_handle", Converter.attr(twitterHandle));
        attrNamesToAttrValues.put(":v_time_stamp", Converter.attr(daysAgoString));

        QueryRequest request = new QueryRequest(buildTableName()).withIndexName(buildIndexName()).
                withKeyConditionExpression(TWITTER_HANDLE_KEY + " = :v_twitter_handle and " +
                        TIME_STAMP_KEY + " > :v_time_stamp").
                withExpressionAttributeValues(attrNamesToAttrValues);
        QueryResult query = db.query(request);
        for (Map<String, AttributeValue> item : query.getItems()) {
            retrievedStoredTweets.add(converter.convertItemMapToStoredTweet(item));
        }

        /*
spec.withMaxPageSize(10);

ItemCollection<QueryOutcome> items = table.query(spec);

// Process each page of results
int pageNum = 0;
for (Page<Item, QueryOutcome> page : items.pages()) {

    System.out.println("\nPage: " + ++pageNum);

    // Process each item on the current page
    Iterator<Item> item = page.iterator();
    while (item.hasNext()) {
        System.out.println(item.next().toJSONPretty());
    }
}
         */

        return retrievedStoredTweets;
    }

    @Override
    public void markTweetDeleted(long tweetId) {
        UpdateItemRequest request = new UpdateItemRequest().withTableName(buildTableName());
        request.addKeyEntry(TWEET_ID_KEY, Converter.attr(tweetId));
        AttributeValue value = new AttributeValue().withBOOL(true);
        request.addAttributeUpdatesEntry(DELETED_KEY, new AttributeValueUpdate(value, PUT));
        getDb().updateItem(request);
    }

    @Override public void removeExpiredItems(int days, String twitterHandle) {
        // Do nothing here. DynamoDB removes expired items automatically. Airtable does not.
    }
}
