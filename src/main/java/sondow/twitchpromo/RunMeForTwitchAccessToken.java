package sondow.twitchpromo;

import java.net.URI;
import java.net.URISyntaxException;
import ru.mirrobot.twitch.api.Twitch;
import ru.mirrobot.twitch.api.auth.Authenticator;
import sondow.common.Environment;

public class RunMeForTwitchAccessToken {

    public static void main(String[] args) {
        Environment environment = new Environment();
        //        String authUsername = environment.get("twitch_auth_username");
        String clientId = environment.get("twitch_client_id");
        String redirectUriString = environment.get("twitch_redirect_uri");
        //        String targetUsername = environment.get("twitch_target_username");
        //        new StreamFetcher().fetchStreamInfo(authUsername, clientId);

        Twitch twitch = new Twitch("https://id.twitch.tv");
        twitch.setClientId(clientId);

        Authenticator auth = twitch.auth();

        URI redirectUri;
        try {
            redirectUri = new URI(redirectUriString);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        String authenticationUrl = auth.getAuthenticationUrl(clientId, redirectUri);
        System.out.println(authenticationUrl);
        boolean b = auth.awaitAccessToken();
        System.out.println(b);

        // There's still something wrong with the server code, since the socket is closed before
        // the response gets sent to the client, but the redirect from twitch to local includes
        // the session token so that's enough for now. Later I can fix the server code or use a
        // different server stack such as Heroku or Glitch to re-authenticate myself or to
        // authenticate other users of the system.

        //        twitch.setClientId(clientId);
        //        twitch.auth().setAccessToken(accessToken);

    }
}
