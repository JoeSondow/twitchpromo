package sondow.twitchpromo;

import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import sondow.common.Environment;
import sondow.common.Time;
import twitter4j.AltTextTwitterImpl;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.UploadedMedia;
import twitter4j.auth.Authorization;
import twitter4j.auth.AuthorizationFactory;
import twitter4j.conf.Configuration;

public class Promoter {

    private Config config;

    private Decider decider;

    private Composer composer;

    private Random random;

    private StreamFetcher streamFetcher;

    private ImageDownloader imageDownloader;

    private TextFileProvider textFileProvider;

    private Database database;

    private AltTextTwitterImpl twitter;

    private Time time;

    public Promoter(Environment environment, String functionName, Random random,
            ImageDownloader imageDownloader,
            TextFileProvider textFileProvider, Database database, AltTextTwitterImpl twitter,
            StreamFetcher streamFetcher, Time time, Decider decider, Composer composer) {
        this.random = random;
        this.config = new ConfigFactory(environment, functionName).configure();
        this.imageDownloader = imageDownloader;
        this.textFileProvider = textFileProvider;
        this.database = database;
        this.twitter = twitter;
        this.streamFetcher = streamFetcher;
        this.time = time;
        this.decider = decider;
        this.composer = composer;
    }

    Promoter(String functionName) {
        this.random = new Random();
        Config config = new ConfigFactory(functionName).configure();
        this.config = config;
        this.imageDownloader = new ImageDownloader();
        this.textFileProvider = new TextFileProvider();
        Configuration twitterConfig = config.getTwitterConfig();
        // this.database = new AwsDynamoDatabase(config, twitterConfig.getUser());
        this.database = new AirtableDatabase(config);
        Authorization twitterAuth = AuthorizationFactory.getInstance(twitterConfig);
        this.twitter = new AltTextTwitterImpl(twitterConfig, twitterAuth);
        this.streamFetcher = new StreamFetcher();
        this.time = new Time();
        this.decider = new Decider(config, twitter);
        this.composer = new Composer(random, textFileProvider);
    }

    public static void main(String[] args) {
        Promoter promoter = new Promoter("Promoter.main");
        //        StatusUpdate statusUpdate = new StatusUpdate(
        //                "Just testing something here. " + new Random().nextInt());
        //        Status status = promoter.twitter.updateStatus(statusUpdate);
        //        System.out.println(status);
        //        String fullText = "It's been a while, but I'm back to streaming some Twitter
        //        bot " +
        //                "coding in Java. Come hang out! https://twitch.tv/joesondow";
        //        String coreMsg = "It's been a while, but I'm back to streaming some Twitter bot
        //        " +
        //                "coding in Java. Come hang out!";
        //
        //        Composition composition = new Composition(fullText, coreMsg);
        //        promoter.storeTweetInDatabase(composition, status);

        promoter.promoteIfLiveAndDueForPromotion();
    }

    public Status promoteIfLiveAndDueForPromotion() {
        Status result = null;

        // Perform database clean up occasionally.
        // I run this system about 400 times per day, but let's not assume the schedule is reliable.
        // Use randomness to make clean up happen approximately 1% of the times the system runs.
        if (random.nextInt(100) == 47 || config.isRemoveNow()) {
            // Do Airtable clean up. (DynamoDB has a self-cleaning feature Airtable lacks.)
            String handle = config.getTwitterConfig().getUser();
            database.removeExpiredItems(config.getDays(), handle);
        }

        // Is the Twitch account live?
        ImageDownloadResult downloadResult = tryToDownloadScreenshot();
        File imageFile = downloadResult.getFile();
        if (downloadResult.isOnline()) {
            result = promoteAndCleanIfDue(imageFile);
        } else {
            System.out.println("Twitch user " + config.getTargetUsername() + " is offline.");
        }
        if (imageFile != null) {
            boolean deleted = imageFile.delete();// Delete the file to clean up.
            if (!deleted) {
                System.out.println("File not deleted for some reason.");
            }
        }
        return result;
    }

    private ImageDownloadResult tryToDownloadScreenshot() {
        ImageUrls imageUrls = ImageDownloader.buildImageUrls(config);
        String saveDir = config.getSaveDir();
        ImageDownloadResult downloadResult;
        System.out.println("image urls to check: " + imageUrls);
        try {
            downloadResult = imageDownloader.checkAndDownload(imageUrls, saveDir);
        } catch (IOException e) {
            // TODO write a unit test for this failure case
            String msg = "Error saving " + imageUrls + " to " + saveDir + " - " + e.getMessage();
            System.out.println(msg);
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return downloadResult;
    }

    private Status promoteAndCleanIfDue(File imageFile) {

        Stream stream = fetchStreamInfo();

        // Check to see if there is a database of recent tweets. If no database yet, then make one.

        // Read tweets from database, not from Twitter.
        String handle = config.getTwitterConfig().getUser();
        List<StoredTweet> storedTweets = database.retrieveRecentTweets(config.getDays(), handle);

        // Decide if it's time to tweet or abort, considering when the last relevant tweet was.
        ZonedDateTime now = time.nowZonedDateTime();
        Decision decision = decider.decideWhatToDo(now, storedTweets, stream, imageFile);
        System.out.println("Decision for what to do: " + decision);

        Status result = null;
        if (decision.isTweetingTime()) {
            String stringInReplyTo = decision.getInReplyToStatusId();
            Long inReplyTo = null;
            if (!Strings.isNullOrEmpty(stringInReplyTo)) {
                inReplyTo = Long.parseLong(stringInReplyTo);
            }
            List<Long> toDelete = decision.getTweetIdsToDelete();
            deleteTweets(toDelete);
            List<Long> idsOfTweetsToMarkDeleted = decision.getIdsOfTweetsToMarkDeleted();
            for (Long id : idsOfTweetsToMarkDeleted) {
                database.markTweetDeleted(id);
            }
            // Exclude deleted tweets from list of recent relevant tweets.
            List<StoredTweet> remaining = removeDeletedTweetsFromList(toDelete, storedTweets);
            result = postTweet(stream, imageFile, remaining, inReplyTo);
        }
        return result;
    }

    private List<StoredTweet> removeDeletedTweetsFromList(List<Long> tweetIdsToDelete,
            List<StoredTweet> tweets) {
        List<StoredTweet> remaining = new ArrayList<>();
        for (StoredTweet tweet : tweets) {
            String tweetIdString = tweet.getTweetId();
            long tweetId = Long.parseLong(tweetIdString);
            if (!tweetIdsToDelete.contains(tweetId)) {
                remaining.add(tweet);
            }
        }
        return remaining;
    }

    private void deleteTweets(List<Long> tweetIdsToDelete) {

        // Sanity check. There should never be very many tweets getting deleted, because the program
        // runs again and deletes recent bad tweets before adding new ones. But just in case…
        if (tweetIdsToDelete.size() > 4) {
            throw new RuntimeException("Too many tweets marked for deletion: " + tweetIdsToDelete);
        }
        for (Long id : tweetIdsToDelete) {
            try {
                twitter.destroyStatus(id);
                database.markTweetDeleted(id);
            } catch (TwitterException e) {
                // TODO write a unit test for this failure case
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }

    private Status postTweet(Stream stream, File imageFile, List<StoredTweet> tweets,
            Long inReplyToStatusId) {

        Composition composition = composer.compose(config, tweets, stream);
        String fullMessage = composition.getFullMessage();
        String altText = composition.getImageAltText();
        StatusUpdate statusUpdate = new StatusUpdate(fullMessage);
        if (inReplyToStatusId != null) {
            statusUpdate.setInReplyToStatusId(inReplyToStatusId);
        }
        Status result;
        try {
            UploadedMedia uploadedMedia = twitter.uploadMedia(imageFile);
            twitter.createMediaMetadata(uploadedMedia.getMediaId(), altText);
            long[] mediaIds = new long[1];
            mediaIds[0] = uploadedMedia.getMediaId();
            statusUpdate.setMediaIds(mediaIds);
            result = twitter.updateStatus(statusUpdate);
        } catch (TwitterException e) {
            // TODO write a unit test for this failure case
            String msg = "Error tweeting " + e.getMessage() + " status code " + e.getStatusCode();
            System.out.println(msg);
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        StringBuilder successLogMsg = new StringBuilder("Tweeted: '").append(fullMessage);
        if (imageFile != null) {
            successLogMsg.append("' and file ").append(imageFile.getName()).
                    append(" with alt text '").append(altText).append("'");
        }
        successLogMsg.append(" with result ").append(result);
        System.out.println(successLogMsg);
        storeTweetInDatabase(composition, result);
        System.out.println("Finished storing tweet in database");
        retweetFromOtherAccounts(result.getId());
        return result;
    }

    private void storeTweetInDatabase(Composition composition, Status result) {
        System.out.println("Preparing to store tweet in database: " + composition.getFullMessage());
        String tweetId = "" + result.getId();
        Date timestamp = result.getCreatedAt();
        String handle = config.getTwitterConfig().getUser();
        String core = composition.getCoreMessage();
        String inReply;
        long inReplyToStatusId = result.getInReplyToStatusId();
        if (inReplyToStatusId == -1) {
            inReply = null;
        } else {
            inReply = "" + inReplyToStatusId;
        }
        StoredTweet tweet = new StoredTweet(tweetId, core, timestamp, handle, inReply);
        System.out.println("Preparing StoredTweet: " + tweet);
        database.storeTweet(tweet);
    }

    private Stream fetchStreamInfo() {
        String username = config.getTargetUsername();
        String clientId = config.getClientId();
        String accessToken = config.getAccessToken();
        return streamFetcher.fetchStreamInfo(username, clientId, accessToken);
    }

    private void retweetFromOtherAccounts(long gameTweetId) {
        List<Configuration> publicityConfigs = config.getPublicityConfigs();
        String twitterScreenNameToRetweet = config.getTwitterConfig().getUser();
        for (Configuration publicityConfig : publicityConfigs) {
            Retweeter retweeter = new Retweeter(publicityConfig);
            retweeter.unretweet(twitterScreenNameToRetweet);
            retweeter.retweet(gameTweetId);
        }
    }
}
