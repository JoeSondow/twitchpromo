package sondow.twitchpromo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Decision {

    private boolean tweetingTime = false;
    private String inReplyToStatusId = null;
    private List<Long> tweetIdsToDelete = new ArrayList<>();
    private List<Long> idsOfTweetsToMarkDeleted = new ArrayList<>();

    public boolean isTweetingTime() {
        return tweetingTime;
    }

    public void setItIsTimeToTweet(boolean itIsTimeToTweet) {
        this.tweetingTime = itIsTimeToTweet;
    }

    public String getInReplyToStatusId() {
        return inReplyToStatusId;
    }

    public void setInReplyToStatusId(String inReplyToStatusId) {
        this.inReplyToStatusId = inReplyToStatusId;
    }

    public List<Long> getTweetIdsToDelete() {
        return Collections.unmodifiableList(new ArrayList<>(tweetIdsToDelete));
    }

    public void deleteTweet(Long tweetId) {
        tweetIdsToDelete.add(tweetId);
    }

    List<Long> getIdsOfTweetsToMarkDeleted() {
        return Collections.unmodifiableList(new ArrayList<>(idsOfTweetsToMarkDeleted));
    }

    void markTweetDeleted(Long tweetId) {
        idsOfTweetsToMarkDeleted.add(tweetId);
    }

    @Override
    public String toString() {
        return "Decision{" +
                "tweetingTime=" + tweetingTime +
                ", inReplyToStatusId=" + inReplyToStatusId +
                ", tweetIdsToDelete=" + tweetIdsToDelete +
                ", idsOfTweetsToMarkDeleted=" + idsOfTweetsToMarkDeleted +
                '}';
    }
}
