package sondow.twitchpromo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Ruleset {

    private Map<String, Rule> rules;

    Ruleset(Map<String, Rule> rules) {
        this.rules = rules;
    }

    public List<String> getMessages(String game, String streamTitle) {
        return getMessages(game, game, streamTitle, false);
    }

    // TODO write lots of test for this
    private List<String> getMessages(String actualGame, String referencedGame, String streamTitle,
            boolean followingReference) {
        Rule gameRule = rules.get(referencedGame);
        if (gameRule == null) {
            // TODO enforce that a default game message list must be in messages file.
            gameRule = rules.get("default");
        }
        Rule chosenRule = null;
        if (gameRule.isToCheckTitle()) {
            chosenRule = chooseInnerRule(streamTitle, gameRule, chosenRule);
        } else {
            chosenRule = gameRule;
        }
        if (chosenRule == null) {
            chosenRule = rules.get("default");
        }
        List<String> messages = new ArrayList<>();
        if (chosenRule.isToPickMessageFromList()) {
            messages = chosenRule.getMessages();
        } else if (chosenRule.isToReferenceOtherGameRule()) {
            String gameToReference = chosenRule.getReferencedGame();
            if (followingReference) {
                String msg = "Multiple levels of indirection found in messages file for game '" +
                        actualGame + "' with stream title '" + streamTitle +
                        "' referencing game '" + referencedGame + "' which references '" +
                        gameToReference + "'";
                throw new RuntimeException(msg);
            }
            messages = getMessages(actualGame, gameToReference, streamTitle, true);
        }
        Function<String, String> replace = msg -> msg.replaceAll("#game#", actualGame);
        return messages.stream().map(replace).collect(Collectors.toList());
    }

    private Rule chooseInnerRule(String streamTitle, Rule gameRule, Rule chosenRule) {
        String title = streamTitle.toLowerCase();
        Map<String, Rule> titleFiltersToMessageLists = gameRule.getTitleFiltersToRules();
        Set<String> titleFilters = titleFiltersToMessageLists.keySet();
        // Loop through the title fragments until you find a match, or else go with "default".
        Iterator<String> titleFiltersIterator = titleFilters.iterator();
        String selectedTitleFilter = null;
        while (titleFiltersIterator.hasNext() && selectedTitleFilter == null) {
            String filter = titleFiltersIterator.next().trim().toLowerCase();
            // Check for plus symbols (+) meaning "AND" or pipe symbols (|) meaning "OR"
            if (filter.contains("+") && allMatch(filter, title)) {
                selectedTitleFilter = filter;
            } else if (filter.contains("|") && anyMatch(filter, title)) {
                selectedTitleFilter = filter;
            } else if (!filter.equals("default") && title.contains(filter)) {
                selectedTitleFilter = filter;
            }
            if (selectedTitleFilter != null) {
                chosenRule = titleFiltersToMessageLists.get(selectedTitleFilter);
            }
        }
        if (chosenRule == null) {
            chosenRule = titleFiltersToMessageLists.get("default");
        }
        return chosenRule;
    }

    private boolean anyMatch(String filter, String streamTitle) {
        String[] fragmentsAnyRequired = filter.split("\\|");
        boolean foundMatch = false;
        for (int i = 0; i < fragmentsAnyRequired.length && !foundMatch; i++) {
            String fragment = fragmentsAnyRequired[i].trim();
            if (streamTitle.contains(fragment.trim())) {
                foundMatch = true;
            }
        }
        return foundMatch;
    }

    private boolean allMatch(String titleFilter, String streamTitle) {
        String[] fragmentsAllRequired = titleFilter.split("\\+");
        boolean foundMismatch = false;
        for (int i = 0; i < fragmentsAllRequired.length && !foundMismatch; i++) {
            String fragment = fragmentsAllRequired[i].toLowerCase();
            if (!streamTitle.contains(fragment.trim())) {
                foundMismatch = true;
            }
        }
        return !foundMismatch;
    }

    @Override public String toString() {
        return "Ruleset{" + "rules=" + rules + '}';
    }
}
