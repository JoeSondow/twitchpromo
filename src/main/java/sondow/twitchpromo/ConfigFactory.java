package sondow.twitchpromo;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.util.StringUtils;
import java.util.ArrayList;
import java.util.List;
import sondow.common.Environment;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Gets configuration strings for authenticating with Twitch, Twitter, and Amazon Web Services (AWS)
 * from environment variables AWS Lambda can handle.
 *
 * @author joesondow
 */
public class ConfigFactory {

    private Environment environment;
    private String functionName;

    public ConfigFactory(Environment environment, String functionName) {
        this.environment = environment;
        this.functionName = functionName;
    }

    public ConfigFactory(String functionName) {
        this(new Environment(), functionName);
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, this custom code gets the configuration either
     * from Lambda-friendly environment variables or else allows Twitter4J to look in its default
     * locations like twitter4j.properties file at the project root, or on the classpath, or in
     * WEB-INF.
     *
     * @return configuration containing Twitch, Twitter, and AWS authentication strings and other
     * runtime variables
     */
    public Config configure() {

        String twitterAccount = environment.require("twitter_account");
        int days = environment.getInt("days", 15);
        String accessToken = environment.require("twitch_access_token");
        String clientId = environment.require("twitch_client_id");
        String targetUsername = environment.require("twitch_target_username");
        String scheduleBroadcasterId = environment.get("twitch_schedule_broadcaster_id");
        String saveDir = environment.get("image_save_dir", "/tmp");
        int width = environment.getInt("width", 1280);
        int height = environment.getInt("height", 720);
        Configuration twitterConfig = configureTwitter(twitterAccount, TrimUser.ENABLED);
        AWSCredentials awsCredentials = getAwsCredentials();
        String airtableAccessToken = environment.get("cred_airtable_personal_access_token");
        String airtableBase = environment.get("CRED_AIRTABLE_BASE");
        String messagesJsonUrl = environment.get("MESSAGES_JSON_URL");

        List<Configuration> publicityConfigs = new ArrayList<>();
        String publicityAccounts = environment.get("PUBLICITY_ACCOUNTS");
        if (publicityAccounts != null && publicityAccounts.length() >= 1) {
            String[] accounts = publicityAccounts.split(",");
            for (String account : accounts) {
                String key = buildTwitterCredEnvVarKey(account);
                String val = environment.get(key);
                if (val != null && val.length() >= 1) {
                    Configuration publicityConfig = configureTwitter(account, TrimUser.DISABLED);
                    publicityConfigs.add(publicityConfig);
                } else {
                    System.out.println("Can't find expected env var " + key);
                }
            }
        }

        int minIntervalMinutes = environment.getInt("min_interval_minutes", 39);
        int medIntervalMinutes = environment.getInt("med_interval_minutes", 59);
        int maxIntervalMinutes = environment.getInt("max_interval_minutes", 79);

        boolean removeNow = "true".equalsIgnoreCase(environment.get("remove_now"));

        return new Config(functionName, accessToken, days, clientId, targetUsername,
                scheduleBroadcasterId, saveDir, width,
                height, twitterConfig, awsCredentials, airtableAccessToken, airtableBase,
                messagesJsonUrl, publicityConfigs, minIntervalMinutes, medIntervalMinutes,
                maxIntervalMinutes, removeNow);
    }

    private String buildTwitterCredEnvVarKey(String twitterHandle) {
        return twitterHandle + "_twitter_credentials";
    }

    private Configuration configureTwitter(String twitterHandle, TrimUser trimUser) {
        ConfigurationBuilder configBuilder = new ConfigurationBuilder();

        String credentialsCsv = environment.require(buildTwitterCredEnvVarKey(twitterHandle));

        String[] tokens = credentialsCsv.split(",");
        String screenName = tokens[0];
        String consumerKey = tokens[1];
        String consumerSecret = tokens[2];
        String accessToken = tokens[3];
        String accessTokenSecret = tokens[4];

        configBuilder.setUser(screenName);
        configBuilder.setOAuthConsumerKey(consumerKey);
        configBuilder.setOAuthConsumerSecret(consumerSecret);
        configBuilder.setOAuthAccessToken(accessToken);
        configBuilder.setOAuthAccessTokenSecret(accessTokenSecret);

        boolean trimDecision = trimUser.getValue();
        return configBuilder.setTrimUserEnabled(trimDecision).setTweetModeExtended(true).build();
    }

    private AWSCredentials getAwsCredentials() {
        String accessKey = environment.get("CRED_AWS_ACCESS_KEY");
        String secretKey = environment.get("CRED_AWS_SECRET_KEY");
        accessKey = StringUtils.trim(accessKey);
        secretKey = StringUtils.trim(secretKey);
        return new BasicAWSCredentials(accessKey, secretKey);
    }
}
