package sondow.twitchpromo;

import com.google.common.base.Charsets;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.commons.io.IOUtils;

/**
 * This class downloads gets text files from places like web URLs.
 *
 * @author @JoeSondow
 */
public class TextFileProvider {

    public String download(String urlString) {

        URL url;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            // TODO rethink this for other people's accounts
            throw new RuntimeException(e);
        }
        String content;
        try {
            content = IOUtils.toString(url, Charsets.UTF_8);
        } catch (IOException e) {
            // TODO rethink this for other people's accounts
            throw new RuntimeException(e);
        }
        return content;
    }
}
