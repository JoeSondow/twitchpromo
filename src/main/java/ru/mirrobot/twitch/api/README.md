This package directory and and its subdirectories' packages are from
[https://github.com/Mirimas/Java-Twitch-Api-Helix](https://github.com/Mirimas/Java-Twitch-Api-Helix)
which is a useful but unpackaged library. There is no viable alternative Java library for the Twitch
API. The most pragmatic solution in this case is to copy the code from the Java-Twitch-Api-Helix
codebase and modify it as needed.

Both Java-Twitch-Api-Helix and twitchpromo use an MIT license.