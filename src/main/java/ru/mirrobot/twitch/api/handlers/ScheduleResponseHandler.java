package ru.mirrobot.twitch.api.handlers;

import ru.mirrobot.twitch.api.models.Schedule;

public interface ScheduleResponseHandler extends BaseFailureHandler {

    void onSuccess(Schedule schedule);
}
