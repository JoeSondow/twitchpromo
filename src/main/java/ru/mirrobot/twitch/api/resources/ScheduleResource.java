package ru.mirrobot.twitch.api.resources;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import ru.mirrobot.twitch.api.handlers.ScheduleResponseHandler;
import ru.mirrobot.twitch.api.models.Schedule;

/**
 * The {@link ScheduleResource} provides the functionality to access the <code>/schedule</code>
 * endpoints of the Twitch API.
 *
 * @author Anton Kildishev
 */
public class ScheduleResource extends AbstractResource {

    /**
     * Construct the resource using the Twitch API base URL and specified API version.
     *
     * @param baseUrl the base URL of the Twitch API
     */
    public ScheduleResource(String baseUrl) {
        super(baseUrl);
    }

    /**
     * Returns a schedule object of authenticated user.
     * <p>Authenticated, required scope: none</p>
     *
     * @param handler the response handler
     */
    public void get(final long userId, final ScheduleResponseHandler handler) {
        String url = String.format("%s/schedule?broadcaster_id=%s", getBaseUrl(), userId);

        http.get(url, new TwitchHttpResponseHandler(handler) {
            @Override
            public void onSuccess(int statusCode, Map<String, List<String>> headers,
                    String content) {
                try {
                    Schedule value = objectMapper.readValue(content, Schedule.class);
                    handler.onSuccess(value);
                } catch (IOException e) {
                    handler.onFailure(e);
                }
            }
        });
    }
}
