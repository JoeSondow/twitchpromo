package ru.mirrobot.twitch.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vacation {

    private Date startTime;
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Vacation vacation = (Vacation) o;
        return Objects.equals(startTime, vacation.startTime) &&
                Objects.equals(endTime, vacation.endTime);
    }

    @Override public int hashCode() {
        return Objects.hash(startTime, endTime);
    }

    @Override public String toString() {
        return "Vacation{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
