package ru.mirrobot.twitch.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.Objects;

/**
 * A segment is an upcoming stream on a schedule. A schedule contains zero or more segments.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Segment {

    private String id;
    private Date startTime;
    private Date endTime;
    private String title;
    private Date canceledUntil;
    private Game category;
    private boolean isRecurring;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCanceledUntil() {
        return canceledUntil;
    }

    public void setCanceledUntil(Date canceledUntil) {
        this.canceledUntil = canceledUntil;
    }

    public Game getCategory() {
        return category;
    }

    public void setCategory(Game category) {
        this.category = category;
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Segment segment = (Segment) o;
        return isRecurring == segment.isRecurring && Objects.equals(id, segment.id) &&
                Objects.equals(startTime, segment.startTime) &&
                Objects.equals(endTime, segment.endTime) &&
                Objects.equals(title, segment.title) &&
                Objects.equals(canceledUntil, segment.canceledUntil) &&
                Objects.equals(category, segment.category);
    }

    @Override public int hashCode() {
        return Objects.hash(id, startTime, endTime, title, canceledUntil, category, isRecurring);
    }

    @Override public String toString() {
        return "Segment{" +
                "id='" + id + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", title='" + title + '\'' +
                ", canceledUntil=" + canceledUntil +
                ", category=" + category +
                ", isRecurring=" + isRecurring +
                '}';
    }
}
