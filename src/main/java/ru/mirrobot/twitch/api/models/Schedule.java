package ru.mirrobot.twitch.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Schedule {

    private List<Segment> data;
    private long broadcasterId;
    private String broadcasterName;
    private String broadcasterLogin;
    private Vacation vacation;

    public List<Segment> getData() {
        return data;
    }

    public void setData(List<Segment> data) {
        this.data = data;
    }

    public long getBroadcasterId() {
        return broadcasterId;
    }

    public void setBroadcasterId(long broadcasterId) {
        this.broadcasterId = broadcasterId;
    }

    public String getBroadcasterName() {
        return broadcasterName;
    }

    public void setBroadcasterName(String broadcasterName) {
        this.broadcasterName = broadcasterName;
    }

    public String getBroadcasterLogin() {
        return broadcasterLogin;
    }

    public void setBroadcasterLogin(String broadcasterLogin) {
        this.broadcasterLogin = broadcasterLogin;
    }

    public Vacation getVacation() {
        return vacation;
    }

    public void setVacation(Vacation vacation) {
        this.vacation = vacation;
    }

    @Override public boolean equals(Object o) {
        if (this == o) {return true;}
        if (o == null || getClass() != o.getClass()) {return false;}
        Schedule schedule = (Schedule) o;
        return broadcasterId == schedule.broadcasterId &&
                Objects.equals(data, schedule.data) &&
                Objects.equals(broadcasterName, schedule.broadcasterName) &&
                Objects.equals(broadcasterLogin, schedule.broadcasterLogin) &&
                Objects.equals(vacation, schedule.vacation);
    }

    @Override public int hashCode() {
        return Objects.hash(data, broadcasterId, broadcasterName, broadcasterLogin, vacation);
    }

    @Override public String toString() {
        return "Schedule{" +
                "data=" + data +
                ", broadcasterId=" + broadcasterId +
                ", broadcasterName='" + broadcasterName + '\'' +
                ", broadcasterLogin='" + broadcasterLogin + '\'' +
                ", vacation=" + vacation +
                '}';
    }
}
